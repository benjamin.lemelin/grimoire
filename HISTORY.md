<div align="center">

# ![Grimoire](.docs/icon-large.svg)

# History

</div>

## Foreword

The Grimoire application has a pretty long history. What started as a simple school projet in 2013 has now been through
more than 8 versions, in 4 different programming languages and many, many databases implementations. Thankfully, I kept
almost all of those, and looking back at them now, I think it's pretty incredible how the application, and myself, have
grown since then. Here is the history of the Grimoire, and what I learned with it.

## What is the Grimoire ?

The Grimoire is a recipe management application that allows users to create and store their own unique recipes. It was
originally created as a way to digitize my mom recipe book, which was all written by hand. This book was so old that
the pages, already pretty yellowed, were starting to tear at the binding. If we wanted to preserve it, we had no choice
but to make a copy. This is where it all started.

## The PHP Era

### First version (2013)

<table>
<tr>
  <th colspan="2"><div align="center">Screenshots</div></th>
</tr>
<tr>
  <td>

![Grimoire V1 Home Page](.docs/history/v1-home.png)

  </td>
  <td>

![Grimoire V1 Categories Page](.docs/history/v1-categories.png)

  </td>
</tr>
<tr>
  <td>

![Grimoire V1 Category Page](.docs/history/v1-category.png)

  </td>
  <td>

![Grimoire V1 Recipe Page](.docs/history/v1-recipe.png)

  </td>
</tr>
<tr>
  <th width="5000"><div align="center">New features</div></th>
  <th width="5000"><div align="center">Used technologies</div></th>
</tr>
<tr>
  <td>
    <ul>
      <li>Create and read recipes.</li>
      <li>Browse by category and author.</li>
      <li>Search function (implemented using the <code>LIKE</code> keyword in SQL).</li>
    </ul>
  </td>
  <td>
<ul>
<li>

[PHP] - Programming langage

</li>
<li>

[MySQL] - Relational database server

<li>

[Bootstrap] - Css framework

</li>

</ul>
  </td>
</tr>
<tr>
  <th colspan="2"><div align="center">Notes</div></th>
</tr>
<tr>
  <td colspan="2">

I was studying computer sciences at the time. One day, our teacher said that the subject of our next project would be
free. That was the perfect opportunity for me to work on a website to write down recipes. At the time, [LAMP] servers
were still very popular, so I started working on [PHP] version backed by a [MySQL] database. It was also the time when
[Android] devices were becoming mainstream, so I created an [Android] application to browse the recipes.

The categories were hardcoded and could not be changed. That was intentional since I only had 2 weeks to program all of
this. To connect to the database, I was using the old [MySQL] extension for [PHP] (`mysql_connect()`,
`mysql_fetch_assoc()` and the like). That was still a thing back then, although the *PDO* extension would have been
better. The database schema had so many tables : `recipe`, `category`, `author`, `ingredient`, `instruction`, `image`
and even `measurement_unit`. We had learned to over-design our databases in college, so I just did what I was taught.

[Single page application]s weren't as prevalent in 2013, so everything was done using server side rendering in [PHP].
While it also had a REST API for the Android application, it was read-only and produced very large XML files. The
application was deployed on an old [Qnap TS-219], which had an ARM CPU and only 512MB of RAM. Fortunately, it had a 
PHP interpreter and a [MySQL] server preinstalled, so the only thing I needed to deploy the application was to copy 
the files.

  </td>
</tr>
</table>

### Second version (2013)

About a month or two later, the application went through a big refactor. I wanted my clients (Mom and Dad) to have
something more robust than a school project. This new version had the same features, but the code was WAY cleaner and 
much more maintainable. One more thing to note is I didn't used any framework whatsoever. I did try [Symfony], but
after a few hours, I decided that it wasn't worth it since there would have been more *framework code* than *application
code*.

## The Java Era

### Third version (2014)

<table>
<tr>
  <th colspan="2"><div align="center">Screenshots</div></th>
</tr>
<tr>
  <td>

![Grimoire V2 Categories Page](.docs/history/v2-categories.png)

  </td>
  <td>

![Grimoire V2 Recipe Page](.docs/history/v2-recipe.png)

  </td>
</tr>
<tr>
  <td>

![Grimoire V2 Recipe Edit Page](.docs/history/v2-edit.png)

  </td>
  <td>

![Grimoire V2 Admin Page](.docs/history/v2-admin.png)

  </td>
</tr>
<tr>
  <th colspan="2"><div align="center">Changes</div></th>
</tr>
<tr>
  <td colspan="2">
    <ul>
      <li>Applied general good practices (like database transactions, which I didn't knew existed when making the first version).</li>
      <li>Revamped web interface and branding (not bad even by today's standards)</li>
      <li>Simplified the database schema (removed tables <code>author</code>, <code>image</code> and <code>measurement_unit</code>).</li>
    </ul>
  </td>
</tr>
<tr>
  <th width="5000"><div align="center">New features</div></th>
  <th width="5000"><div align="center">Technologies</div></th>
</tr>
<tr>
  <td>
    <ul>
      <li>Database backups and restauration (zip file containing all the data serialized as JSON).</li>
    </ul>
  </td>
  <td>
<ul>
<li>

[Java] - Programming language

</li>
<li>

[Tomcat] - Servlet server

</li>
<li>

  Java Server Pages (<code>.jsp</code>)

</li>
<li>

[MySQL] - Relational database server

</li>
<li>

[Bootstrap] - Css framework

</li>
</ul>
  </td>
</tr>
<tr>
  <th colspan="2"><div align="center">Notes</div></th>
</tr>
<tr>
  <td colspan="2">

A year passed. Mom had written an impressive 206 recipes on the Grimoire, something I wasn't really expecting. In fact,
the whole family was using it, Dad included. All this data had to be handled carefully, and I feared that the current 
PHP version wasn't up to the task. Since we were learning about [Tomcat] and [Java servlet containers][Web Containers] 
at school, I took the opportunity to rewrite the application. The primary goal was to make the Grimoire more robust and
to add a way to backup the application data, in case something went terribly wrong.

  </td>
</tr>
</table>

> **Note**: Don't overthink your database structure unless you have space/time performance requirements. Nowadays, I
> tend to design the APIs first, then establish what my tables/collections should look like and finally choose a
> database implementation (SQL, NoSQL, etc). This reduces the amount of transformations your data has to go through for
> each layer of your application. Moreover, it tends to reduce the amount of requests you have to make to the database
> (and for SQL databases, the amount of joins).

### Fourth version (2015)

We used the third version for about a year. Then, I started to work on using an alternative database engine : [H2]. Like
[SQLite], [H2] is an embeddable database. It's implemented in [Java], pretty lightweight and simple to use. Being
embedded meant that the application could manage the database itself instead of relying on external configuration.

As a challenge, I decided to support both [H2] and [MySQL] databases. I also started to use [Hibernate], but it ended 
up to be pretty cumbersome. While I understand the appeal of [ORM]s, most of the time, it's a lot more work than if you 
had implemented the database layer yourself, especially if you are working with many relations between tables. I didn't 
used [ORM]s ever since.

## The Kotlin Era

### Fifth version (2018)

<table>
<tr>
  <th colspan="2"><div align="center">Screenshots</div></th>
</tr>
<tr>
  <td>

![Grimoire V5 Home Page](.docs/history/v5-home.png)

  </td>
  <td>

![Grimoire V5 Categories Page](.docs/history/v5-categories.png)

  </td>
</tr>
<tr>
  <td colspan="2">

![Grimoire V5 Ingredients Drag and Drop](.docs/history/v5-drag-and-drop.png)

  </td>
</tr>
<tr>
  <th colspan="3"><div align="center">Changes</div></th>
</tr>
<tr>
  <td colspan="3">
    <ul>
      <li>Small UI changes (colors, fonts, layout).</li>
      <li>Improved the recipe edit form (drag-an-drop to reorder ingredients and instructions).</li>
      <li>Improved search functionality (ignore stop words, removed diacritics, fuzzy search, etc.).</li>
    </ul>
  </td>
</tr>
<tr>
  <th width="5000"><div align="center">New features</div></th>
  <th width="5000"><div align="center">Technologies</div></th>
</tr>
<tr>
  <td>
    <ul>
      <li>Added duration and servings to recipes.</li>
      <li>Added the ability to create categories (they were hardcoded before).</li>
    </ul>
  </td>
  <td>
<ul>
<li>

[Kotlin] - Programming langage

</li>
<li>

[Spring Boot] - Inversion of control container

</li>
<li>

[Thymeleaf] - Template engine

</li>
<li>

[Tomcat] - Servlet server

</li>
<li>

[H2] - Database engine

</li>
<li>

[React] - JavaScript frontend library

</li>
<li>

[Apache Lucene] - Full-text indexing and searching library

</li>
</ul>
  </td>
</tr>
<tr>
  <th colspan="2"><div align="center">Notes</div></th>
</tr>
<tr>
  <td colspan="2">

In 2018, most of my skills came from the Grimoire in one way or another. While it's true that university and college was
a big help to get started, they tend to oversimplify things to make room for more subjects : programming, databases, 
operating systems, networking, web applications, mobile applications, games, agile software development, project 
management, cyber-security, infrastructure, mathematics, statistics, etc. You end up being average at everything and 
good at nothing.

Anyway, one of the many skills the Grimoire gave me was the ability to create [Android] applications. [Kotlin] was 
beginning to take over [Java] as the primary langage for the platform, so I started to take interest in it. To learn
[Kotlin], I decided to use it for the Grimoire. Since it targets the JVM, and can easily interop with [Java],
it meant that I could still deploy it on my old [Qnap TS-219].

I moved from server-side rendering to client-side rendering, learning [React] at the same time. Since I was changing 
the rendering paradigm, this meant that the backend had to be rewritten. I decided to use [Spring Boot] for my REST 
services, as it is very flexible, robust and still relevant to this day. I also wanted a more reliable search 
functionality, so I included the [Apache Lucene] library to the project. As it is not a search engine, it was not 
trivial to integrate with the rest of the database, but still was worth it at the end.

  </td>
</tr>
</table>

### Sixth version (2019)

More of a revision than a version. In 2019, I decided to migrate to a NoSQL database. Since I couldn't even dream of
installing [MongoDB] on such slow hardware, I tried using [Nitrite], an open source embedded document store written in 
Java. Unfortunately, this new version of the Grimoire wasn't very reliable, so I rolled back to the old version very 
quickly.

## The Rust Era

### Seventh version (2020)

<table>
<tr>
  <th colspan="2"><div align="center">Screenshots</div></th>
</tr>
<tr>
  <td>

![Grimoire V7 Home Page](.docs/history/v7-home.png)

  </td>
  <td>

![Grimoire V7 Recipe Edit Page](.docs/history/v7-edit.png)

  </td>
</tr>
<tr>
  <td>
  <div align="center">

![Grimoire V7 Home Page for mobile](.docs/history/v7-home-mobile.png)

  </div>
  </td>
  <td>
  <div align="center">

![Grimoire V7 Recipe for mobile](.docs/history/v7-recipe-mobile.png)

  </div>
  </td>
</tr>
<tr>
  <th colspan="3"><div align="center">Changes</div></th>
</tr>
<tr>
  <td colspan="3">
    <ul>
      <li>Major UI changes (removing Bootstrap in favor of raw Css and applying Google's Material Design langage).</li>
      <li>Categories are now treated like tags, making them easier to manage for the user.</li>
      <li>Switched to an homemade database.</li>
    </ul>
  </td>
</tr>
<tr>
  <th width="5000"><div align="center">New features</div></th>
  <th width="5000"><div align="center">Technologies</div></th>
</tr>
<tr>
  <td>
    <ul>
      <li>Added the ability to flag favorite recipes.</li>
      <li>Auto save feature (which ended up being a bad idea because of the lack of an undo feature).</li>
    </ul>
  </td>
  <td>
<ul>
<li>

[Rust] - Programming langage

</li>
<li>

[Rocket] - Web framework

</li>
<li>

[React] - JavaScript frontend library

</li>
</ul>
  </td>
</tr>
<tr>
  <th colspan="2"><div align="center">Notes</div></th>
</tr>
<tr>
  <td colspan="2">

The year is 2020. Our poor [TS-219][Qnap TS-219] wasn't showing any sign of fatigue, but the fact that most of the
available RAM was used by the Grimoire (310MB out of 512MB to be exact) bothered me. I must applaud this little
guy for withstanding such a load while performing very well with the other tasks. Nevertheless, I felt I could do more.
[Rust] doesn't have garbage collector, and is compiled directly to machine code. With that in mind, I hoped to reduce
the memory footprint of the Grimoire to about 100MB of RAM, so I began to work. I used [Rocket] as the backend web
framework, [serde] for serialization/deserialization and a [homemade database][Dodo]. The results didn't disappoint :
it used way less CPU and a mere 12MB of RAM.

> **Note** : The Java/Kotlin versions were always less memory hungry on low spec systems (like the TS-219). In fact, on
> my other server, the Grimoire was using up to 800MB of RAM, siting at around 600MB most of the time. I guess 
> giving memory back to the system is not a priority when you have plenty.

Being tired of [Bootstrap], I switched to raw Css files instead and started applying the [Material Design] langage. I 
also migrated to [React] functional components instead of using classes. Since the starter project came with 
[Progressive Web App] already configured, this new version was also installable on the user home screen. When opened, 
it acted just like any other [Android] application, even thought it is running inside a webview.

Deploying that version was a little bit harder since I had to cross-compile to ArmV5 and. I did manage to make it run 
though, and it worked like a charm for 3 years.

> **Note** : About that [homemade database][Dodo]. It was essentially leveraging the file system and uses [serde] to 
> serialize and deserialize the data. I don't recommend that you use it for your own projects. In fact, I don't use it 
> anymore unless it's something with very small requirements.

  </td>
</tr>
</table>

### Eighth version (2023)

<table>
<tr>
  <th colspan="2"><div align="center">Screenshots</div></th>
</tr>
<tr>
  <td>

![Grimoire V8 Home Page](.docs/history/v8-home.png)

  </td>
  <td>

![Grimoire V8 Home Page Dark MOde](.docs/history/v8-home-dark.png)

  </td>
</tr>
<tr>
  <td>
  <div align="center">

![Grimoire V8 Recipe Page](.docs/history/v8-recipe.png)

  </div>
  </td>
  <td>
  <div align="center">

![Grimoire V8 Edit Recipe Page](.docs/history/v8-edit.png)

  </div>
  </td>
</tr>
<tr>
  <th colspan="3"><div align="center">Changes</div></th>
</tr>
<tr>
  <td colspan="3">
    <ul>
      <li>Major revamp of the UI (close to Material Design 3, but not quite).</li>
      <li>Switch back to a relational database.</li>
    </ul>
  </td>
</tr>
<tr>
  <th width="5000"><div align="center">New features</div></th>
  <th width="5000"><div align="center">Technologies</div></th>
</tr>
<tr>
  <td>
    <ul>
      <li>Dark theme.</li>
      <li>Ability to mark ingredients and instructions as done.</li>
    </ul>
  </td>
  <td>
<ul>
<li>

[Rust] - Programming langage

</li>
<li>

[Axum] - Web framework

</li>
<li>

[SQLite] - Relational database

</li>
<li>

[tantivy] - Full-text search library

</li>
<li>

[Typescript] - Programming langage

</li>
<li>

[SolidJs] - JavaScript frontend library

</li>
</ul>
  </td>
</tr>
<tr>
  <th colspan="2"><div align="center">Notes</div></th>
</tr>
<tr>
  <td colspan="2">

I started to seriously develop this version back in september 2022. There were some attempts before that, but nothing 
came out of it. First thing I tried was some new web frameworks, like [Actix], [Warp] and [Poem]. I ended up using
[Axum] for its relative simplicity, its use of `async` and its close relation to [tokio] (a very robust async runtime 
for [Rust]).

For the frontend, I wanted to try one of the many new frameworks that are competing with [React]. I started with [Lit],
which I didn't like because of the scoped styling it enforced. As someone who isn't afraid of Css, it felt like a
downgrade. Then, I tried [Svelte], but encountered the exact same problem. While I must admit that it was a lot more 
fun, unless you are planing to use [Tailwind], I don't understand why you would go that route. Then, I came 
across [SolidJS]. This is another framework with no Virtual DOM, just like [Svelte], but without scoped Css. It is also
closer to React, albeit some key differences on how it manages reactivity. This is what I ended up using. Just like
[Rust], [Solid][SolidJS] has non-negligible learning curve, but it pays in the long run. I cannot recommend it enough.

As for the database, I wanted to go back to using a relational database, so I chose [SQLite]. It is lightweight and
embeddable. I also included [tantivy], a full-text search library written in [Rust]. Again, just like [Apache Lucene],
using it was a non-trivial task, but I ended up learning a lot about how real search engines are made along the way.

Like every new version, I'm always very proud of what I accomplished, but this time is very different. I know
[this sound dumb][Dunning-Kruger Effect], but if you look at the first version and compare it to this one, it isn't even
a contest : it looks and feel very clean and professional.

  </td>
</tr>
</table>

## The future

This is where this journey ends for now. I hope you enjoyed going through this little museum with me. I certainly did 
enjoy revisiting those old versions. I highly encourage anyone who want to improve to make new things. Experience is
everything, and theory is only a way to get you started.

[LAMP]: https://en.wikipedia.org/wiki/LAMP_(software_bundle)

[PHP]: https://www.php.net/

[MySQL]: https://en.wikipedia.org/wiki/MySQL

[Android]: https://en.wikipedia.org/wiki/Android_(operating_system)

[Single page application]: https://en.wikipedia.org/wiki/Single-page_application

[Bootstrap]: https://getbootstrap.com/

[Qnap TS-219]: https://www.qnap.com/en/product/ts-219p%20ii/specs/hardware

[Symfony]: https://fr.wikipedia.org/wiki/Symfony

[Java]: https://en.wikipedia.org/wiki/Java_(programming_language)

[Web Containers]: https://en.wikipedia.org/wiki/Web_container

[Tomcat]: https://tomcat.apache.org/

[H2]: https://www.h2database.com/html/main.html

[SQLite]: https://sqlite.org/index.html

[Hibernate]: https://hibernate.org/

[ORM]: https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping

[Kotlin]: https://kotlinlang.org/

[Thymeleaf]: https://www.thymeleaf.org/

[React]: https://react.dev/

[AngularJs]: https://angularjs.org/

[Spring Boot]: https://spring.io/

[Apache Lucene]: https://lucene.apache.org/

[MongoDB]: https://en.wikipedia.org/wiki/MongoDB

[Nitrite]: https://github.com/nitrite/nitrite-java

[Rust]: https://www.rust-lang.org/

[Rocket]: https://rocket.rs/

[serde]: https://serde.rs/

[Dodo]: https://crates.io/crates/dodo

[Material Design]: https://material.io

[Progressive Web App]: https://web.dev/progressive-web-apps/

[TypeScript]: https://www.typescriptlang.org/

[Actix]: https://actix.rs/

[Warp]: https://github.com/seanmonstar/warp

[Poem]: https://github.com/poem-web/poem

[Axum]: https://github.com/tokio-rs/axum

[Tokio]: https://tokio.rs/

[Lit]: https://lit.dev/

[Svelte]: https://svelte.dev/

[Tailwind]: https://tailwindcss.com/

[SolidJS]: https://www.solidjs.com/

[tantivy]: https://github.com/quickwit-oss/tantivy

[Dunning-Kruger Effect]: https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect