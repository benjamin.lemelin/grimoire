#/bin/sh
set -e

# Build QPKG
cp -R ${FRONTEND_ARTIFACTS} qpkg/shared/static
cp ${BACKEND_ARTIFACTS_X86_64_MUSL} qpkg/shared/app

cd qpkg
sed -i "s/^QPKG_VER=".*"$/QPKG_VER=\"${PACKAGE_VERSION}\"/g" qpkg.cfg
qbuild --build-version ${PACKAGE_VERSION}
cd ..

# Upload QPKG to package registry
curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file "${PACKAGE_QPKG_ARTIFACTS}" "${PACKAGE_QPKG_REGISTRY_URL}"