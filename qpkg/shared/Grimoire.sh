#!/bin/sh
CONF=/etc/config/qpkg.conf
QPKG_NAME="Grimoire"
QPKG_ROOT=`/sbin/getcfg $QPKG_NAME Install_Path -f ${CONF}`
PROCESS_FILE="GrimoireProcess.info"
export QNAP_QPKG=$QPKG_NAME

case "$1" in
  start)
    ENABLED=$(/sbin/getcfg $QPKG_NAME Enable -u -d FALSE -f $CONF)
    if [ "$ENABLED" != "TRUE" ]; then
        echo "$QPKG_NAME is disabled."
        exit 1
    fi
    
    cd "$QPKG_ROOT"
    if [ -f $PROCESS_FILE ]; then
        /bin/echo "Grimoire is started."
        exit 1
    else
        touch $PROCESS_FILE
    fi

    # In the past, the data folder was named ".app". If this folder exists, simply rename it to "data".
    if [ -d ./.app ]; then
      mv ./.app ./data
    fi

    # Start the application.
    chmod +x ./app
    ./app --data-dir ./data --port 8242 --secure-port 8243 > Grimoire.log 2> GrimoireErr.log &
    /bin/echo $! >> $PROCESS_FILE
    ;;

  stop)
    cd "$QPKG_ROOT"
    
    if [ -f $PROCESS_FILE ]; then
        value=`cat $PROCESS_FILE`
        kill $value
        rm $PROCESS_FILE
    else
        /bin/echo "Grimoire is not started."
        exit 1
    fi
    ;;

  restart)
    $0 stop
    $0 start
    ;;

  *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac

exit 0
