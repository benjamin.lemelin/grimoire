#/bin/sh
set -e

build() {
  backend_artifacts=$1
  suffix=$2
  platform=$(if [ -n "$3" ]; then echo "--platform $3"; fi)

  docker build \
         --build-arg backend_dir=$backend_artifacts \
         --build-arg frontend_dir=${FRONTEND_ARTIFACTS} \
         $platform \
         -t ${PACKAGE_DOCKER_IMAGE}:${PACKAGE_VERSION}-$suffix-dev \
         .
  docker push ${PACKAGE_DOCKER_IMAGE}:${PACKAGE_VERSION}-$suffix-dev
}

publish() {
  version=$1

  docker manifest create \
     ${PACKAGE_DOCKER_IMAGE}:$version-dev \
     --amend ${PACKAGE_DOCKER_IMAGE}:$version-amd64-dev \
     --amend ${PACKAGE_DOCKER_IMAGE}:$version-arm64-dev
  docker manifest push ${PACKAGE_DOCKER_IMAGE}:$version-dev
}

build ${BACKEND_ARTIFACTS_X86_64} amd64
build ${BACKEND_ARTIFACTS_AARCH64} arm64 linux/arm64/v8

publish ${PACKAGE_VERSION}