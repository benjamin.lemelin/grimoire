#/bin/sh
set -e

cd frontend
npm ci --cache ${NPM_HOME} --prefer-offline
npm version ${PACKAGE_VERSION}
npm run build