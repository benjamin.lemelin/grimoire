<div align="center">

![Grimoire](.docs/icon.svg)

# Changelog

</div>

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.2.3 - 2024-06-09

### Fixed

- Fixed issue on Chromium based browsers where the checkboxes would shrink too much if the related step text was very 
  long.

## 1.2.2 - 2023-10-03

### Added

- Added support for a demo mode. This mode can be used to try out the application in GitLab pages without installing 
  it (using fake data). Functionality of this mode is limited to read operations only.

### Fixed

- Fixed issue where the `--secure-cookies` flag wasn't really producing *secure* cookies. In fact, *secure* cookies
  were never produced, even in https.

### Changed

- Improved search results by using fuzzy queries. Users can now make typos (up to a certain point).
- Added a step when importing a database backup to ensure that all images are converted to JPEG files before writing
  them to the disk.

## 1.2.1 - 2023-09-24

### Changed

- Changed default data directory to `.grimoire`. The Docker image and the Qnap QPKG still use the `data` directory.
- Changed default ports to `8242` and `8243` for the Docker image and the Qnap QPKG. The standalone application still
  uses `8080` and `8443`.

## 1.2.0 - 2023-09-23

### Added

- Added an official Docker image install option.
- Added support for other languages in the user interface. Currently, the only available languages
  are english (`en`) and french (`fr`).

### Changed

- Account dropdown only closes when clicking on a hyperlink inside it.
- Changed some strings as part of the translation process.

## 1.1.3 - 2023-05-10

### Fixed

- Fixed overflow for large recipe images.
- Fixed search results layout, which had too much vertical space between the elements.
- Fixed issue where the profile dropdown was shown behind the search field.
- Fixed an issue preventing dropdowns from being closed by clicking on them.
- Fixed an issue where the "previous" button inside the header bar would lead to a dialog (which is an undesired
  behaviour).

## 1.1.2 - 2023-05-07

### Changed

- Multiple sessions are now allowed. Older sessions are automatically deleted when a new one is created.
- Sessions now contains the user agent and the last access time.

## 1.1.1 - 2023-05-05

### Added

- Images now fade into view after loading.

### Fixed

- Fixed layout of the user creation dialog on mobile.
- Fixed styling issues where some shadows would get cropped by other UI components.

### Changed

- Centered the settings page.
- Vertical scrollbar now take the full height of the viewport.
- Major refactoring of the dialog component.
- Removed dependency on `@solid-primitive/immutable`.

## 1.1.0 - 2023-04-30

### Added

- Added https support. Provide your SSL certificate through the command line options. When enabled, all http trafic is
  redirected to the https port. When behind a reverse proxy, please use the `--secure-cookies` flag.
- Added user accounts. Now, users must log in to access and modify the recipes. There are 3 types of accounts :
  - *Guests* : Can only view the recipes.
  - *Users* : Can view and modify the recipes.
  - *Admins* : Can view and modify the recipes, but also manage other accounts and make/restore backups.
- The database backup system is no longer hidden. Admins may access it in the application settings.

### Fixed

- Improvements to the accessibility. For example, icons are now ignored by screen readers.
- Fixed an issue where notifications would disappear even if the pending request wasn't finished.

### Changed

- Migrated to the `tracing` crate instead of `log` for logging. Very useful when using async code.
- All dialogs are now tied to a URL.
- The theme button has been moved to the settings dropdown on the top left of the screen.
- Other visual tweaks.

## 1.0.0 - 2023-04-03

### Added

- Initial version