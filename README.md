<div align="center">

# ![Grimoire](.docs/icon-large.svg)

</div>

Grimoire is a recipe management application that allows users to create and store their own unique recipes. Users can 
easily write out their favorite recipes, including ingredients and preparation steps, and view them all in one 
convenient location. Whether you're a seasoned chef or a beginner cook, Grimoire is the perfect tool for organizing and
keeping track of all your favorite recipes.

<div align="center">

![Grimoire Home Page](.docs/screenshot.png)

</div>

## Demo

Visit the [Live Demo][Demo] to try out the application without installing it. When asked to log in, you can use `admin`
for both the username and password. Additionally, there is also fake `user` and `guest` accounts for previewing the 
appearance and features associated with those account types.

## Installation

Follow those instructions to install the Grimoire on your server. The application will run by default on the `8242` 
port.

### Docker Compose

Create a `docker-compose.yml` file with the following contents. Edit this configuration to suit you needs, like the
user, port and volume path.

```yml
version: "3.5"
services:
  grimoire:
    image: blemelin/grimoire
    container_name: grimoire
    user: "uid:gid"
    ports:
      - 8242:8242
    volumes:
      - /path/to/data:/grimoire/data
    restart: "unless-stopped"
```

While in the same folder as the `docker-compose.yml`, use the following commands to start and stop the application.

```shell
docker compose up -d
docker compose down
```

If you plan to run this application behind a reverse proxy that provides *Https*, you might want to enable the
`--secure-cookies` flag. To do this, add the following to your `docker-compose.yml` file :

```yml
version: "3.5"
services:
  grimoire:
    command: "--secure-cookies"
```

### Qnap

Download the QPKG file from the [release page][Releases]. To install the application, open your NAS *App Center* and
install the QPKG manually. Please mote that you may need to allow the installation of unsigned applications in the 
settings, as this package is not signed.

## Building from sources

These instructions will guide you on how to build and run the application on your local machine for development and 
testing purposes. See [installation](#installation) for notes on how to deploy the project on a live system. 

This project is separated into two subprojects : the [backend](backend/README.md) and the [frontend](frontend/README.md).
Please refer to the individual `README.md` file for details on how to build each subproject. After that, you'll need to
start the backend first and then start the frontend.

## History

This application has a long history behind it. What started as a simple school projet in 2013 has now been through more
than 8 versions, in 4 different programming languages and many, many databases implementations. Read the
[HISTORY.md](HISTORY.md) file for more information.

## Changelog

See the [CHANGELOG.md](CHANGELOG.md) file for version details.

## License

This project is licensed under the GNU GPLv3. See the [LICENSE.md](LICENSE.md) file for details.

[Demo]: https://codebreak.gitlab.io/grimoire-demo
[Releases]: https://gitlab.com/benjamin.lemelin/grimoire/-/releases