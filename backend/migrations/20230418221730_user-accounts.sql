CREATE TABLE user
(
  id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  username   TEXT    NOT NULL UNIQUE,
  email      TEXT    NOT NULL,
  password   TEXT    NOT NULL, -- PHC string.
  first_name TEXT    NOT NULL,
  last_name  TEXT    NOT NULL,
  role       INTEGER NOT NULL
);

CREATE TABLE session
(
  token   TEXT    NOT NULL PRIMARY KEY, -- Hashed.
  user_id INTEGER NOT NULL UNIQUE REFERENCES user (id) ON DELETE CASCADE
);