CREATE TABLE recipe
(
  id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  name        TEXT    NOT NULL,
  author      TEXT    NOT NULL,
  description TEXT    NOT NULL,
  duration    INTEGER NOT NULL, -- In seconds.
  yield       TEXT    NOT NULL, -- Like "24 cookies" or "4 servings".
  difficulty  INTEGER NOT NULL, -- Starts at 1 (easiest).
  is_favorite INTEGER NOT NULL,
  has_image   INTEGER NOT NULL
);

CREATE TABLE category
(
  name TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE recipe_category_mapping
(
  recipe_id     INTEGER NOT NULL REFERENCES recipe (id) ON DELETE CASCADE,
  category_name TEXT    NOT NULL REFERENCES category (name) ON DELETE CASCADE,
  PRIMARY KEY (recipe_id, category_name)
);

CREATE TABLE ingredient
(
  id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  recipe_id INTEGER NOT NULL REFERENCES recipe (id) ON DELETE CASCADE,
  `index`   INTEGER NOT NULL,
  name      TEXT    NOT NULL,
  quantity  TEXT    NOT NULL, -- Like 200 gr. or 1 ml.
  comment   TEXT    NOT NULL
);

CREATE TABLE instruction
(
  id        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  recipe_id INTEGER NOT NULL REFERENCES recipe (id) ON DELETE CASCADE,
  `index`   INTEGER NOT NULL,
  text      TEXT    NOT NULL
);

CREATE TRIGGER create_new_categories
  BEFORE INSERT
  ON recipe_category_mapping
BEGIN
  INSERT OR IGNORE INTO category (name)
  VALUES (new.category_name);
END;

CREATE TRIGGER delete_unused_categories
  AFTER DELETE
  ON recipe_category_mapping
  WHEN old.category_name NOT IN (SELECT category_name
                                 FROM recipe_category_mapping)
BEGIN
  DELETE
  FROM category
  WHERE name = old.category_name;
END;