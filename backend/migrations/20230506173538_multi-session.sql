DROP TABLE session;
CREATE TABLE session
(
  token         TEXT     NOT NULL PRIMARY KEY, -- Hashed.
  user_id       INTEGER  NOT NULL REFERENCES user (id) ON DELETE CASCADE,
  user_agent    TEXT     NOT NULL, -- Json like { platform : "Linux", browser : "Firefox" }.
  last_accessed DATETIME NOT NULL
);