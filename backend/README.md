<div align="center">

![Grimoire](../.docs/icon-large.svg)

# Backend

</div>

## Building from sources

These instructions will guide you on how to build the backend of the application. First, you'll need to install the
[Rust] programming language toolchain. Simply follow the instructions for your operating system on their website.

Then, install the [sqlx] cli tools with *SQLite* and *rustls* support. On most systems, you can do this by running
the following command :

```shell
cargo install sqlx-cli --no-default-features --features sqlite,rustls
```

The [sqlx] crate provides compile-time syntactic and semantic verification of the SQL queries. To achieve this, there
must be a live database available at compile time. To create the database, simply run these commands :

```shell
mkdir -p .grimoire/db

sqlx database create
sqlx migrate run
```

Compile time verification can be quite time-consuming. To remedy this, [sqlx] places files inside the `.sqlx` folder
as a cache for the known working queries. This folder can be refreshed using this command :

```shell
cargo sqlx prepare
```

When this is all done, you'll then be able to compile the project :

```shell
cargo build --release
```

## Running in development mode

Use `cargo` to run the backend in development mode :

```shell
cargo run
```

The application will start listening for http connections on the `8080` port. If you want to use https, you'll need to
provide your own self-signed certificate inside the `.cert` folder. This might sound scary, but in reality, this is
only one command to run (although you'll need to have `openssl` installed for this to work). See the
[Appendix](#generating-self-signed-certificates) section for more information. After all that, start the backend in
https mode (port `8443`) using this command :

```shell
cargo run -- --cert .cert/cert.pem --key .cert/key.pem
```

## Routes

Here is a list of all routes of this application.

### API

Please refer to the [requests](requests) directory for more exemples on how to use the backend API.

| Route                             | Method             | Resource          | Read  | Write | Comment                               |
|-----------------------------------|--------------------|-------------------|-------|-------|---------------------------------------|
| **Recipes**                       |                    |                   |       |       |                                       |
| `/api/recipes`                    | GET / POST         | `RecipeSummary[]` | Guest | User  | Multipart POST (Json + Image).        |
| `/api/recipes/:id`                | GET / PUT / DELETE | `Recipe`          | Guest | User  | Multipart PUT (Json + Image).         |
| `/api/recipes/search?q=:keywords` | GET                | `RecipeSummary[]` | Guest | User  |                                       |
| `/api/recipes/favorites`          | GET                | `RecipeSummary[]` | Guest | User  |                                       |
| `/api/recipes/:id/image`          | GET                | `Image`           | Guest | User  |                                       |
| `/api/recipes/:id/favorite`       | PUT / DELETE       | *Empty*           | Guest | User  | PUT to add, DELETE to remove.         |
| **Categories**                    |                    |                   |       |       |                                       |
| `/api/categories`                 | GET                | `Category[]`      | Guest | User  |                                       |
| `/api/categories/:name`           | GET                | `RecipeSummary[]` | Guest | User  |                                       |
| `/api/categories/:name/image`     | GET                | `Image`           | Guest | User  |                                       |
| **Users**                         |                    |                   |       |       |                                       |
| `/api/users`                      | GET / POST         | `User[]`          | Admin | Admin | Admins can edit other accounts.       |
| `/api/users/:username`            | GET / DELETE       | `User`            | Guest | User  | Admins can delete other accounts.     |
| `/api/users/:username/username`   | PUT                | Username          | Guest | User  |                                       |
| `/api/users/:username/password`   | PUT                | Password          | Guest | User  | Requires old and new passwords.       |
| `/api/users/:username/profile`    | PUT                | User profile      | Guest | User  |                                       |
| `/api/users/:username/role`       | PUT                | `Role`            | Admin | Admin |                                       |
| `/api/users/login`                | POST               | `User`            | None  | All   | Also produces session cookie.         |
| `/api/users/logout`               | POST               | *Empty*           | None  | Guest | Produces expired session cookie.      |
| **Administration**                |                    |                   |       |       |                                       |
| `/api/admin/setup`                | POST               | Login credentials | All   | All   | First account creation. Usable  once. |
| `/api/admin/database`             | GET / PUT / DELETE | Database Backup   | Admin | Admin | Zip file.                             |

### Static

The purpose of those routes is to serve the frontend files. Theses are not used in development mode. See the frontend
[README](../frontend/README.md) for more information.

| Route    | Result                                                                 |
|----------|------------------------------------------------------------------------|
| `/:file` | Static file inside the "static" directory, if any.                     |
| `/`      | Fallback to the `index.html` file, if none of the above routes worked. |

## Appendix

### Generating self-signed certificates

You'll need a signed certificate to use https in development mode. Generate one using `openssl` with the following
command :

```shell
mkdir .cert
openssl req -nodes -new -x509 -keyout .cert/key.pem -out .cert/cert.pem -subj '/CN=localhost'
```

Simply copy the resulting `.cert` folder alongside the `Cargo.toml` or `package.json` and you are good to go.

[Rust]: https://www.rust-lang.org/

[sqlx]: https://github.com/launchbadge/sqlx