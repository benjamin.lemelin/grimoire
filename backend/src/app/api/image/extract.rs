use std::io;

use axum::{async_trait, body::Bytes, http::{header, Request, StatusCode}};
use axum::extract::{FromRequest, rejection::BytesRejection};
use axum::response::{IntoResponse, Response};
use tracing::{error, info};

use crate::app::api::image::repository::ImageFile;

const MAX_IMAGE_WIDTH: u32 = 1920;
const MAX_IMAGE_HEIGHT: u32 = 1080;

/// Recipe image DTO.
///
/// Images are always converted to a jpeg when received.
#[derive(Debug)]
pub struct ImageDTO(pub Vec<u8>);

/// Recipe image DTO rejection.
#[derive(Debug, thiserror::Error)]
pub enum ImageDTORejection {
    /// Invalid or corrupted image (produces `400 Bad request`).
    #[error("invalid or corrupted image")]
    InvalidImage(#[source] image::ImageError),
    /// Unsupported image format (produces `415 Unsupported Media Type`).
    #[error("image format not jpeg, png or webp")]
    UnsupportedFormat(#[source] image::ImageError),
    /// Rejected bytes (produces various codes).
    #[error("{0}")]
    RejectedBytes(#[from] BytesRejection),
    /// Error while reading/writing the image (produces `500 Internal Server Error`).
    #[error("{0}")]
    Io(#[from] io::Error),
}

impl ImageDTO {
    /// Create from database entity.
    ///
    /// Entities are considered safe, so there is no conversion to JPEG happening. If you read data from
    /// an unknown source, use [from_raw][ImageDTO::from_raw] instead.
    pub fn from_entity(image_file: ImageFile) -> Self {
        Self(image_file)
    }

    /// Convert into entity.
    pub fn into_entity(self) -> ImageFile {
        self.0
    }

    /// Create from raw bytes.
    ///
    /// This could fail if the bytes do no represent a valid image file (like a JPEG or a PNG).
    pub fn from_raw(bytes: &[u8]) -> Result<Self, image::ImageError> {
        Ok(Self(Self::convert_to_jpeg(bytes)?))
    }

    fn into_raw(self) -> Vec<u8> {
        self.0
    }

    /// Get as raw bytes.
    pub fn as_raw(&self) -> &[u8] {
        &self.0
    }

    fn convert_to_jpeg(
        bytes: &[u8]
    ) -> Result<Vec<u8>, image::ImageError> {
        use std::io::Cursor;

        // Inspect image.
        let mut image = image::load_from_memory(bytes)?;
        let mut has_changed = false;

        // Convert to jpeg, if needed.
        if image::ImageFormat::Jpeg != image::guess_format(bytes)? {
            has_changed = true;
        }

        // Resize if too large.
        {
            let image_width = image.width();
            let image_height = image.height();
            if image_width > MAX_IMAGE_WIDTH || image_height > MAX_IMAGE_HEIGHT {
                has_changed = true;
                image = image.resize(MAX_IMAGE_WIDTH, MAX_IMAGE_HEIGHT, image::imageops::CatmullRom);
            }
        }

        // Write if changes were made.
        if has_changed {
            let mut bytes = Cursor::new(Vec::with_capacity(bytes.len()));
            image.write_to(&mut bytes, image::ImageFormat::Jpeg)?;
            Ok(bytes.into_inner())
        } else {
            Ok(bytes.into())
        }
    }
}

#[async_trait]
impl<S, B> FromRequest<S, B> for ImageDTO
    where
        Bytes: FromRequest<S, B, Rejection=BytesRejection>,
        S: Send + Sync,
        B: Send + 'static,
{
    type Rejection = ImageDTORejection;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        Ok(Self::from_raw(&Bytes::from_request(req, state).await?)?)
    }
}

impl IntoResponse for ImageDTO {
    fn into_response(self) -> Response {
        let headers = [(header::CONTENT_TYPE, "image/jpeg")];
        let content = self.into_raw();

        (headers, content).into_response()
    }
}

impl IntoResponse for ImageDTORejection {
    fn into_response(self) -> Response {
        match &self {
            ImageDTORejection::Io(_) => error!("{self}"),
            _ => info!("{self}"),
        }

        match self {
            Self::InvalidImage(_) => {
                (StatusCode::BAD_REQUEST, self.to_string()).into_response()
            }
            Self::UnsupportedFormat(_) => {
                (StatusCode::UNSUPPORTED_MEDIA_TYPE, self.to_string()).into_response()
            }
            Self::RejectedBytes(err) => {
                err.into_response()
            }
            Self::Io(_) => {
                StatusCode::INTERNAL_SERVER_ERROR.into_response()
            }
        }
    }
}

impl From<image::ImageError> for ImageDTORejection {
    fn from(err: image::ImageError) -> Self {
        match err {
            image::ImageError::Unsupported(_) => Self::UnsupportedFormat(err),
            image::ImageError::IoError(err) => Self::Io(err),
            _ => Self::InvalidImage(err),
        }
    }
}