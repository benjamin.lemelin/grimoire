use axum::{debug_handler, Router, routing::get};
use axum::extract::{Path, State};
use axum::response::IntoResponse;

use crate::app::api::{
    self,
    image::{extract::ImageDTO, repository as image_repository},
    session::Session,
};

/// Category Api routes.
pub(in crate::app::api) fn routes() -> Router<api::State> {
    Router::new()
        .route("/recipes/:id/image", get(find_by_recipe))
        .route("/categories/:name/image", get(find_by_category))
}

/// Find recipe image.
#[debug_handler]
async fn find_by_recipe(
    _session: Session,
    State(ctx): State<api::State>,
    Path(id): Path<i64>,
) -> api::Result<impl IntoResponse> {
    let storage = &ctx.images;
    let image = image_repository::find_by_recipe(id, storage).await?;
    Ok(ImageDTO::from_entity(image))
}

/// Find category image.
#[debug_handler]
async fn find_by_category(
    _session: Session,
    State(ctx): State<api::State>,
    Path(name): Path<String>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let storage = &ctx.images;
    let image = image_repository::find_by_category(&name, &mut db, storage).await?;
    Ok(ImageDTO::from_entity(image))
}