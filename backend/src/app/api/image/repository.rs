use std::io;

use crate::{database, repository, storage};

/// Image file, as represented on disk.
pub type ImageFile = Vec<u8>;

/// Find image for a given recipe.
///
/// Returns an error when image is not found.
pub async fn find_by_recipe(
    recipe_id: i64,
    storage: &storage::Bucket,
) -> repository::Result<ImageFile> {
    let image = storage.read(&file_name(recipe_id)).await?;
    Ok(image)
}

/// Find a random image for a given category.
///
/// Returns an error when category is not found or no recipe in this category has an image.
pub async fn find_by_category<'c, C>(
    category_name: &str,
    db: &'c mut C,
    storage: &storage::Bucket,
) -> repository::Result<ImageFile>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_scalar!(
        "SELECT \
             recipe.id as `id?` \
         FROM \
             recipe_category_mapping \
         JOIN \
             recipe on recipe.id = recipe_category_mapping.recipe_id \
         WHERE \
             recipe_category_mapping.category_name = ? AND recipe.has_image = 1 \
         ORDER BY \
             RANDOM() \
         LIMIT 1",
        category_name
    );

    let recipe_id = query
        .fetch_one(db)
        .await?
        .ok_or(repository::Error::NotFound)?;

    let image = storage.read(&file_name(recipe_id)).await?;
    Ok(image)
}

/// Set the image of a recipe.
///
/// Does not check if the recipe exist.
pub async fn set_for_recipe(
    recipe_id: i64,
    image: Option<ImageFile>,
    storage: &storage::Bucket,
) -> repository::Result<Option<ImageFile>> {
    match image {
        Some(image) => {
            storage.write(&file_name(recipe_id), &image).await?;
            Ok(Some(image))
        }
        None => match storage.delete(&file_name(recipe_id)).await {
            Ok(_) => Ok(None),
            Err(e) if e.kind() == io::ErrorKind::NotFound => Ok(None),
            Err(e) => Err(e)?
        },
    }
}

/// Delete the image of a recipe.
///
/// Does not check if the recipe exist.
pub async fn delete(
    recipe_id: i64,
    storage: &storage::Bucket,
) -> repository::Result<()> {
    match storage.delete(&file_name(recipe_id)).await {
        Ok(_) => Ok(()),
        Err(e) if e.kind() == io::ErrorKind::NotFound => Ok(()),
        Err(e) => Err(e)?
    }
}

fn file_name(recipe_id: i64) -> String {
    format!("{recipe_id}.jpg")
}