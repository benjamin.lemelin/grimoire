use std::io;

use argon2::password_hash;
use axum::{http::StatusCode, Router};
use axum::response::{IntoResponse, Response};
use tracing::{error, info};

use session::store::SessionStore;

use crate::config::Config;
use crate::database::Database;
use crate::index::Index;
use crate::repository;
use crate::storage::Bucket;

mod admin;
mod category;
mod image;
mod ingredient;
mod instruction;
mod recipe;
mod session;
mod user;
mod extract;

/// Api routes.
pub async fn routes(config: &Config) -> anyhow::Result<Router> {
    // Dependencies.
    let database = Database::open(config.database_path()).await?;
    let index = Index::open(config.index_path()).await?;
    let images = Bucket::open(config.images_path()).await?;
    let sessions = SessionStore::new(&database, config.max_sessions());
    let state = State {
        database,
        index,
        images,
        sessions,
        secure_cookies: config.https() || config.secure_cookies(),
    };

    Ok(Router::new()
        .merge(admin::api::routes())
        .merge(category::api::routes())
        .merge(image::api::routes())
        .merge(recipe::api::routes())
        .merge(user::api::routes())
        .with_state(state))
}

/// Api state.
///
/// Holds api state and utilities.
#[derive(Debug, Clone)]
struct State {
    database: Database,
    index: Index,
    images: Bucket,
    sessions: SessionStore,
    secure_cookies: bool,
}

/// Api result.
type Result<T> = std::result::Result<T, Error>;

/// Api errors.
#[derive(Debug, thiserror::Error)]
enum Error {
    /// Resource not found (produces `404 Not Found`).
    ///
    /// This means that the requested resource is missing, or that the user lacks authorization to
    /// request the resource.
    #[error("not found")]
    NotFound,
    /// Session cookie not provided (produces `401 Unauthorized`).
    ///
    /// Occurs when a session cookie is not provided in the request, but was required.
    #[error("session cookie not provided")]
    CookieNotProvided,
    /// Session not found (produces `401 Unauthorized`).
    ///
    /// Occurs when a session cookie is provided, but said session doesn't exist.
    #[error("session not found")]
    SessionNotFound,
    /// Access to resource is forbidden (produces `403 Forbidden`).
    ///
    /// Occurs when user has insufficient rights to perform an operation. Also occurs if the
    /// operation is impossible due to the state of the application (like initial setup).
    #[error("forbidden")]
    Forbidden,
    /// Authentication failed due to wrong credentials (produces `400 Bad Request`).
    ///
    /// No more information is given to the user about this error. The user may not exist or the
    /// password is wrong.
    #[error("wrong credentials")]
    WrongCredentials,
    /// Resource already exists (produces `409 Conflict`).
    ///
    /// Happens when a duplicated ressource would have been created.
    #[error("already exists")]
    AlreadyExists,
    /// Error while using the database (produces `500 Internal Server Error`).
    ///
    /// Those are unmanaged errors coming from the database, like failed constraints or IO errors.
    #[error("error using the database : {0}")]
    Database(#[from] sqlx::Error),
    /// Error while using the index (produces `500 Internal Server Error`).
    ///
    /// Those are unmanaged errors coming from the index, most likely IO errors.
    #[error("error using the index : {0}")]
    Index(#[from] tantivy::TantivyError),
    /// Error while using the storage (produces `500 Internal Server Error`).
    ///
    /// Those are unmanaged IO errors coming from the storage.
    #[error("error using the storage : {0}")]
    Storage(#[from] io::Error),
    /// Error while hashing passwords (produces `500 Internal Server Error`).
    ///
    /// Very unlikely errors, like an invalid PHC string coming from the database or other hashing
    /// related errors.
    #[error("error while hashing passwords : {0}")]
    Hashing(#[from] password_hash::Error),
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        match self {
            Self::NotFound => {
                info!("{self}");
                (StatusCode::NOT_FOUND).into_response()
            }
            Self::CookieNotProvided | Self::SessionNotFound => {
                info!("{self}");
                (StatusCode::UNAUTHORIZED).into_response()
            }
            Self::Forbidden => {
                info!("{self}");
                (StatusCode::FORBIDDEN).into_response()
            }
            Self::WrongCredentials => {
                info!("{self}");
                (StatusCode::BAD_REQUEST, format!("{self}")).into_response()
            }
            Self::AlreadyExists => {
                info!("{self}");
                (StatusCode::CONFLICT).into_response()
            }
            _ => {
                error!("{self}");
                (StatusCode::INTERNAL_SERVER_ERROR).into_response()
            }
        }
    }
}

impl From<repository::Error> for Error {
    fn from(err: repository::Error) -> Self {
        match err {
            repository::Error::NotFound => Self::NotFound,
            repository::Error::AlreadyExists => Self::AlreadyExists,
            repository::Error::Database(e) => e.into(),
            repository::Error::Index(e) => e.into(),
            repository::Error::Storage(e) => e.into(),
        }
    }
}