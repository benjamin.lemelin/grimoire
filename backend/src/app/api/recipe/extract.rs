use axum::{async_trait, body::Full, http::{self, Request, StatusCode}};
use axum::extract::{FromRequest, Json, rejection::JsonRejection};
use axum::extract::multipart::{Multipart, MultipartError, MultipartRejection};
use axum::response::{IntoResponse, Response};
use serde::{Deserialize, Serialize};
use tracing::{info, error, warn};
use validator::{Validate, ValidationErrors};

use crate::app::api::{
    recipe::repository::RecipeEntity,
    image::extract::{ImageDTO, ImageDTORejection},
    category::{extract::CategoryDTO, repository::CategoryEntity},
    ingredient::{extract::IngredientDTO, repository::IngredientEntity},
    instruction::{extract::InstructionDTO, repository::InstructionEntity},
};

/// Recipe DTO.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RecipeDTO {
    pub id: i64,
    pub name: String,
    pub author: String,
    pub description: String,
    pub duration: DurationDTO,
    pub r#yield: String,
    pub difficulty: DifficultyDTO,
    pub is_favorite: bool,
    pub has_image: bool,
    pub categories: Vec<CategoryDTO>,
    pub ingredients: Vec<IngredientDTO>,
    pub instructions: Vec<InstructionDTO>,
}

impl RecipeDTO {
    /// Create from database entity.
    pub fn from_entity(
        recipe: RecipeEntity,
        categories: Vec<CategoryEntity>,
        ingredients: Vec<IngredientEntity>,
        instructions: Vec<InstructionEntity>,
    ) -> Self {
        Self {
            id: recipe.id,
            name: recipe.name,
            author: recipe.author,
            description: recipe.description,
            duration: DurationDTO::from_seconds(recipe.duration),
            r#yield: recipe.r#yield,
            difficulty: DifficultyDTO::from_integer(recipe.difficulty),
            is_favorite: recipe.is_favorite == 1,
            has_image: recipe.has_image == 1,
            categories,
            ingredients: ingredients.into_iter().map(IngredientDTO::from_entity).collect(),
            instructions: instructions.into_iter().map(InstructionDTO::from_entity).collect(),
        }
    }

    /// Convert to database entity.
    pub fn into_entity(self) -> (
        RecipeEntity,
        Vec<CategoryEntity>,
        Vec<IngredientEntity>,
        Vec<InstructionEntity>
    ) {
        (
            RecipeEntity {
                id: self.id,
                name: self.name,
                author: self.author,
                description: self.description,
                duration: self.duration.into_seconds(),
                r#yield: self.r#yield,
                difficulty: self.difficulty.into_integer(),
                is_favorite: if self.is_favorite { 1 } else { 0 },
                has_image: if self.has_image { 1 } else { 0 },
            },
            self.categories,
            self.ingredients.into_iter().map(IngredientDTO::into_entity).collect(),
            self.instructions.into_iter().map(InstructionDTO::into_entity).collect()
        )
    }
}

/// Recipe summary DTO.
#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RecipeSummaryDTO {
    pub id: i64,
    pub name: String,
    pub author: String,
    pub description: String,
    pub duration: DurationDTO,
    pub r#yield: String,
    pub difficulty: DifficultyDTO,
    pub is_favorite: bool,
    pub has_image: bool,
}

impl RecipeSummaryDTO {
    /// Create from database entity.
    pub fn from_entity(recipe: RecipeEntity) -> Self {
        Self {
            id: recipe.id,
            name: recipe.name,
            author: recipe.author,
            description: recipe.description,
            duration: DurationDTO::from_seconds(recipe.duration),
            r#yield: recipe.r#yield,
            difficulty: DifficultyDTO::from_integer(recipe.difficulty),
            is_favorite: recipe.is_favorite == 1,
            has_image: recipe.has_image == 1,
        }
    }

    /// Create from database entities.
    pub fn from_entities(recipes: Vec<RecipeEntity>) -> Vec<Self> {
        recipes.into_iter().map(Self::from_entity).collect()
    }
}

/// Duration DTO.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct DurationDTO {
    pub seconds: i64,
}

impl DurationDTO {
    /// Create from seconds.
    pub fn from_seconds(seconds: i64) -> Self {
        Self {
            seconds
        }
    }

    /// Convert to seconds.
    pub fn into_seconds(self) -> i64 {
        self.seconds
    }
}

/// Difficulty DTO.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum DifficultyDTO {
    Easy,
    Normal,
    Hard,
}

impl DifficultyDTO {
    /// Create from integer.
    pub fn from_integer(difficulty: i64) -> Self {
        match difficulty {
            1 => Self::Easy,
            2 => Self::Normal,
            3 => Self::Hard,
            other => {
                warn!("recipe difficulty not in range (value of {other}, expected between 1 and 3)");
                Self::Normal
            }
        }
    }

    /// Convert to integer.
    pub fn into_integer(self) -> i64 {
        match self {
            Self::Easy => 1,
            Self::Normal => 2,
            Self::Hard => 3,
        }
    }
}

/// Recipe by keywords query DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct RecipeByKeywords {
    #[validate(length(min = 1, message = "must be at least one character"))]
    #[serde(rename = "q", deserialize_with = "serde_trim::string_trim")]
    pub keywords: String,
}

/// Recipe to add DTO.
///
/// This is a multipart request : a recipe (JSON) and an image (file).
#[derive(Debug)]
pub struct NewRecipeDTO(pub NewRecipeDTOInner, pub Option<ImageDTO>);

/// New recipe DTO (recipe part of the multipart request).
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct NewRecipeDTOInner {
    #[validate(length(min = 1, message = "must be at least one character"))]
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub name: String,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub author: String,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub description: String,
    pub duration: DurationDTO,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub r#yield: String,
    pub difficulty: DifficultyDTO,
    pub is_favorite: bool,
    pub has_image: bool,
    pub categories: Vec<CategoryDTO>,
    pub ingredients: Vec<IngredientDTO>,
    pub instructions: Vec<InstructionDTO>,
}

/// New recipe DTO rejection.
#[derive(Debug, thiserror::Error)]
pub enum NewRecipeDTORejection {
    /// Missing recipe inside request (produces `400 Bad request`).
    #[error("missing recipe from request body")]
    MissingRecipe,
    /// Invalid recipe (produces `400 Bad request`).
    #[error("invalid fields:\n{0}")]
    InvalidRecipe(#[from] ValidationErrors),
    /// Invalid multipart (produces `400 Bad request`).
    #[error("{0}")]
    InvalidMultipart(#[from] MultipartError),
    /// Rejected multipart (produces various codes).
    #[error("{0}")]
    RejectedMultipart(#[from] MultipartRejection),
    /// Rejected recipe (produces various codes).
    #[error("{0}")]
    RejectedRecipe(#[from] JsonRejection),
    /// Rejected image (produces various codes).
    #[error("{0}")]
    RejectedImage(#[from] ImageDTORejection),
    /// Rejected http (produces `500 Internal Server Error`).
    #[error("{0}")]
    RejectedHttp(#[from] http::Error),
}

impl NewRecipeDTOInner {
    /// Convert to database entity.
    pub fn into_entity(self) -> (
        RecipeEntity,
        Vec<CategoryEntity>,
        Vec<IngredientEntity>,
        Vec<InstructionEntity>
    ) {
        (
            RecipeEntity {
                id: 0,
                name: self.name,
                author: self.author,
                description: self.description,
                duration: self.duration.into_seconds(),
                r#yield: self.r#yield,
                difficulty: self.difficulty.into_integer(),
                is_favorite: if self.is_favorite { 1 } else { 0 },
                has_image: if self.has_image { 1 } else { 0 },
            },
            self.categories,
            self.ingredients.into_iter().map(IngredientDTO::into_entity).collect(),
            self.instructions.into_iter().map(InstructionDTO::into_entity).collect()
        )
    }
}

#[async_trait]
impl<S, B> FromRequest<S, B> for NewRecipeDTO
    where
        Multipart: FromRequest<S, B, Rejection=MultipartRejection>,
        S: Send + Sync,
        B: Send + 'static,
{
    type Rejection = NewRecipeDTORejection;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        let mut data = Multipart::from_request(req, state).await?;
        let mut recipe = None;
        let mut image = None;

        while let Some(field) = data.next_field().await? {
            match field.name() {
                Some("recipe") => {
                    let headers = field.headers().clone();
                    let bytes = field.bytes().await?;

                    // Creating a request for the Json extractor.
                    let mut request = Request::builder().body(Full::new(bytes))?;
                    *request.headers_mut() = headers;

                    let Json(value): Json<NewRecipeDTOInner> = Json::from_request(request, state).await?;
                    value.validate()?;
                    recipe = Some(value);
                }
                Some("image") => {
                    let headers = field.headers().clone();
                    let bytes = field.bytes().await?;

                    // Creating a request for the Image extractor.
                    let mut request = Request::builder().body(Full::new(bytes))?;
                    *request.headers_mut() = headers;

                    image = Some(ImageDTO::from_request(request, state).await?);
                }
                _ => { continue; }
            }
        }

        Ok(Self(
            recipe.ok_or_else(|| NewRecipeDTORejection::MissingRecipe)?,
            image,
        ))
    }
}

impl IntoResponse for NewRecipeDTORejection {
    fn into_response(self) -> Response {
        match &self {
            NewRecipeDTORejection::RejectedHttp(_) => error!("{self}"),
            _ => info!("{self}"),
        }

        match self {
            Self::MissingRecipe | Self::InvalidRecipe(_) | Self::InvalidMultipart(_) => {
                (StatusCode::BAD_REQUEST, self.to_string()).into_response()
            }
            Self::RejectedMultipart(e) => {
                e.into_response()
            }
            Self::RejectedRecipe(e) => {
                e.into_response()
            }
            Self::RejectedImage(e) => {
                e.into_response()
            }
            Self::RejectedHttp(_) => {
                StatusCode::INTERNAL_SERVER_ERROR.into_response()
            }
        }
    }
}

/// Recipe to update DTO.
pub type UpdateRecipeDTO = NewRecipeDTO;