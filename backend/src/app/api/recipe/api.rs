use axum::{debug_handler, http::StatusCode, Router, routing::{get, put}};
use axum::extract::{Json, Path, State};
use axum::response::IntoResponse;

use crate::app::api::{
    self,
    category::repository as category_repository,
    extract::ValidatedQuery,
    image::{extract::ImageDTO, repository as image_repository},
    ingredient::repository as ingredient_repository,
    instruction::repository as instruction_repository,
    session::{Session, UserSession},
    recipe::{
        extract::{NewRecipeDTO, RecipeByKeywords, RecipeDTO, RecipeSummaryDTO, UpdateRecipeDTO},
        repository as recipe_repository,
    },
};

// TODO : Favorites should be per user.

/// Recipe Api routes.
pub(in crate::app::api) fn routes() -> Router<api::State> {
    Router::new()
        .route("/recipes", get(find_all).post(add))
        .route("/recipes/:id", get(find_by_id).put(update).delete(delete))
        .route("/recipes/favorites", get(find_favorites))
        .route("/recipes/search", get(find_by_keywords))
        .route("/categories/:name", get(find_by_category))
        .route("/recipes/:id/favorite", put(set_favorite).delete(unset_favorite))
}

/// Find all recipes.
#[debug_handler]
async fn find_all(
    _session: Session,
    State(ctx): State<api::State>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let recipes = recipe_repository::find_all(&mut db).await?;
    let dto = RecipeSummaryDTO::from_entities(recipes);
    Ok(Json(dto))
}

/// Find favorite recipes.
#[debug_handler]
async fn find_favorites(
    _session: Session,
    State(ctx): State<api::State>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let recipes = recipe_repository::find_favorites(&mut db).await?;
    let dto = RecipeSummaryDTO::from_entities(recipes);
    Ok(Json(dto))
}

/// Find recipes by category.
#[debug_handler]
async fn find_by_category(
    _session: Session,
    State(ctx): State<api::State>,
    Path(category): Path<String>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let recipes = recipe_repository::find_by_category(&category, &mut db).await?;
    let dto = RecipeSummaryDTO::from_entities(recipes);
    Ok(Json(dto))
}

/// Find recipes by keywords.
#[debug_handler]
async fn find_by_keywords(
    _session: Session,
    State(ctx): State<api::State>,
    ValidatedQuery(RecipeByKeywords { keywords }): ValidatedQuery<RecipeByKeywords>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let index = ctx.index.read().await?;
    let recipes = recipe_repository::find_by_keywords(&keywords, 15, &mut db, &index).await?;
    let dto = RecipeSummaryDTO::from_entities(recipes);
    Ok(Json(dto))
}

/// Find recipe by id.
#[debug_handler]
async fn find_by_id(
    _session: Session,
    State(ctx): State<api::State>,
    Path(id): Path<i64>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let recipe = recipe_repository::find_by_id(id, &mut db).await?;
    let categories = category_repository::find_by_recipe_id(id, &mut db).await?;
    let ingredients = ingredient_repository::find_by_recipe_id(id, &mut db).await?;
    let instructions = instruction_repository::find_by_recipe_id(id, &mut db).await?;
    let dto = RecipeDTO::from_entity(recipe, categories, ingredients, instructions);
    Ok(Json(dto))
}

/// Add a new recipe.
#[debug_handler]
async fn add(
    _session: UserSession,
    State(ctx): State<api::State>,
    recipe: NewRecipeDTO,
) -> api::Result<impl IntoResponse> {
    let dto = save(ctx, None, recipe).await?;
    Ok((StatusCode::CREATED, Json(dto)))
}

/// Update a recipe.
#[debug_handler]
async fn update(
    _session: UserSession,
    State(ctx): State<api::State>,
    Path(id): Path<i64>,
    recipe: UpdateRecipeDTO,
) -> api::Result<impl IntoResponse> {
    let dto = save(ctx, Some(id), recipe).await?;
    Ok(Json(dto))
}

// Add or update recipe.
async fn save(
    ctx: api::State,
    id: Option<i64>,
    NewRecipeDTO(recipe, image): NewRecipeDTO,
) -> api::Result<RecipeDTO> {
    let mut db = ctx.database.write().await?;
    let index = ctx.index.write().await?;
    let image_storage = &ctx.images;

    // Unpack DTO.
    let recipe_has_image = recipe.has_image;
    let (mut recipe, categories, ingredients, instructions) = recipe.into_entity();
    let image = image.map(ImageDTO::into_entity);

    // Insert or update recipe.
    let recipe = match id {
        None => {
            recipe_repository::insert(recipe, &mut db, &index).await?
        }
        Some(id) => {
            recipe.id = id;
            recipe_repository::update(recipe, &mut db, &index).await?
        }
    };

    // Update categories, ingredients, instructions.
    let categories = category_repository::set_for_recipe(recipe.id, categories, &mut db).await?;
    let ingredients = ingredient_repository::set_for_recipe(recipe.id, ingredients, &mut db).await?;
    let instructions = instruction_repository::set_for_recipe(recipe.id, instructions, &mut db).await?;

    // Update image. Images are deleted only when the recipe is marked as having no image.
    // Otherwise, we assume the image is unchanged.
    if recipe_has_image {
        if image.is_some() {
            image_repository::set_for_recipe(recipe.id, image, image_storage).await?;
        }
    } else {
        image_repository::set_for_recipe(recipe.id, None, image_storage).await?;
    }

    // Produce dto with saved recipe.
    let dto = RecipeDTO::from_entity(recipe, categories, ingredients, instructions);
    db.commit().await?;
    index.commit().await?;
    Ok(dto)
}

/// Add a recipe to favorites.
#[debug_handler]
async fn set_favorite(
    _session: UserSession,
    State(ctx): State<api::State>,
    Path(id): Path<i64>,
) -> api::Result<impl IntoResponse> {
    save_favorite(ctx, id, true).await
}

/// Remove a recipe from favorites.
#[debug_handler]
async fn unset_favorite(
    _session: UserSession,
    State(ctx): State<api::State>,
    Path(id): Path<i64>,
) -> api::Result<impl IntoResponse> {
    save_favorite(ctx, id, false).await
}

/// Add or remove a recipe from the favorites.
async fn save_favorite(
    ctx: api::State,
    id: i64,
    is_favorite: bool,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    recipe_repository::update_favorite(id, is_favorite, &mut db).await?;
    db.commit().await?;
    Ok(())
}

/// Delete a recipe.
#[debug_handler]
async fn delete(
    _session: UserSession,
    State(ctx): State<api::State>,
    Path(id): Path<i64>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    let index = ctx.index.write().await?;
    let image_storage = &ctx.images;

    recipe_repository::delete(id, &mut db, &index).await?;
    image_repository::delete(id, image_storage).await?;

    db.commit().await?;
    index.commit().await?;
    Ok(())
}