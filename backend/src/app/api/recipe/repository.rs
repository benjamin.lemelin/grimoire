use database::extension::SqliteQueryResultExt;

use crate::{database, index, repository};

/// Recipe entity, as represented in database.
#[derive(Debug)]
pub struct RecipeEntity {
    pub id: i64,
    pub name: String,
    pub author: String,
    pub description: String,
    pub duration: i64,
    pub r#yield: String,
    pub difficulty: i64,
    pub is_favorite: i64,
    pub has_image: i64,
}

/// Find all recipes.
pub async fn find_all<'c, C>(
    db: &'c mut C,
) -> repository::Result<Vec<RecipeEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        RecipeEntity,
        "SELECT \
             id, \
             name, \
             author, \
             description, \
             duration, \
             yield, \
             difficulty, \
             is_favorite, \
             has_image \
         FROM \
             recipe \
         ORDER BY \
             name"
    );
    let recipes = query
        .fetch_all(db)
        .await?;
    Ok(recipes)
}

/// Find all favorite recipes.
pub async fn find_favorites<'c, C>(
    db: &'c mut C,
) -> repository::Result<Vec<RecipeEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        RecipeEntity,
        "SELECT \
             id, \
             name, \
             author, \
             description, \
             duration, \
             yield, \
             difficulty, \
             is_favorite, \
             has_image \
         FROM \
             recipe \
         WHERE \
             is_favorite = 1 \
         ORDER BY \
             name"
    );
    let recipes = query
        .fetch_all(db)
        .await?;
    Ok(recipes)
}

/// Find recipes by category.
///
/// Returns an error if the category could not be found.
pub async fn find_by_category<'c, C>(
    category_name: &str,
    db: &'c mut C,
) -> repository::Result<Vec<RecipeEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        RecipeEntity,
        "SELECT \
            recipe.id as `id`, \
            recipe.name as `name`, \
            recipe.author as `author`, \
            recipe.description as `description`, \
            recipe.duration as `duration`, \
            recipe.yield as `yield`, \
            recipe.difficulty as `difficulty`, \
            recipe.is_favorite as `is_favorite`, \
            recipe.has_image as `has_image` \
         FROM \
             recipe_category_mapping \
         JOIN \
             recipe on recipe.id = recipe_category_mapping.recipe_id \
         WHERE \
             recipe_category_mapping.category_name = ? \
         ORDER BY \
             recipe.name",
        category_name
    );

    let recipes = query
        .fetch_all(db)
        .await?;

    // Categories are guaranteed to have at least one recipe. Thus, empty results means that the
    // category doesn't exists.
    if !recipes.is_empty() {
        Ok(recipes)
    } else {
        Err(repository::Error::NotFound)
    }
}

/// Find recipes by keywords.
pub async fn find_by_keywords(
    keywords: &str,
    max_results: usize,
    db: &mut database::Reader,
    index: &index::Reader,
) -> repository::Result<Vec<RecipeEntity>> {
    let ids = index.find(keywords, max_results).await?;
    let mut recipes = Vec::with_capacity(ids.len());
    for id in ids {
        recipes.push(find_by_id(id, db).await?);
    }
    Ok(recipes)
}

/// Find recipe by id.
///
/// Returns an error if the recipe could not be found.
pub async fn find_by_id(
    id: i64,
    db: &mut database::Reader,
) -> repository::Result<RecipeEntity> {
    let query = sqlx::query_as!(
        RecipeEntity,
        "SELECT \
             id, \
             name, \
             author, \
             description, \
             duration, \
             yield, \
             difficulty, \
             is_favorite, \
             has_image \
         FROM \
             recipe \
         WHERE \
             id = ?",
        id
    );
    let recipe = query
        .fetch_one(db)
        .await?;
    Ok(recipe)
}

/// Add a new recipe.
pub async fn insert(
    mut recipe: RecipeEntity,
    db: &mut database::Writer,
    index: &index::Writer,
) -> repository::Result<RecipeEntity> {
    // Add to database.
    let query = sqlx::query_scalar!(
        "INSERT INTO recipe (\
             name, \
             author, \
             description, \
             duration, \
             yield, \
             difficulty, \
             is_favorite, \
             has_image \
         ) VALUES (\
             ?,?,?,?,?,?,?,?\
         ) RETURNING id as `id:_`",
        recipe.name,
        recipe.author,
        recipe.description,
        recipe.duration,
        recipe.r#yield,
        recipe.difficulty,
        recipe.is_favorite,
        recipe.has_image
    );
    recipe.id = query
        .fetch_one(db)
        .await?;

    // Add to index.
    index.insert(recipe.id, &recipe.name)?;

    Ok(recipe)
}

/// Update a recipe.
///
/// Returns an error if the recipe could not be found.
pub async fn update(
    recipe: RecipeEntity,
    db: &mut database::Writer,
    index: &index::Writer,
) -> repository::Result<RecipeEntity> {
    // Update in database.
    let query = sqlx::query!(
        "UPDATE recipe \
         SET \
             name = ?, \
             author = ?, \
             description = ?, \
             duration = ?, \
             yield = ?, \
             difficulty = ?, \
             is_favorite = ?, \
             has_image = ? \
         WHERE \
             id = ?",
        recipe.name,
        recipe.author,
        recipe.description,
        recipe.duration,
        recipe.r#yield,
        recipe.difficulty,
        recipe.is_favorite,
        recipe.has_image,
        recipe.id
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    // Update in index.
    index.update(recipe.id, &recipe.name)?;

    Ok(recipe)
}

/// Update a recipe favorite status.
///
/// Returns an error if the recipe could not be found.
pub async fn update_favorite(
    id: i64,
    is_favorite: bool,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let is_favorite = if is_favorite { 1 } else { 0 };
    let query = sqlx::query!(
        "UPDATE recipe \
         SET \
             is_favorite = ? \
         WHERE \
             id = ?",
        is_favorite,
        id
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;
    Ok(())
}

/// Delete a recipe.
///
/// This is a cascading operation. All ingredients, instructions and category mappings will be
/// deleted too. If a category goes unused after this operation, it is also deleted (by a database
/// trigger).
///
/// Returns an error if the recipe could not be found.
pub async fn delete(
    id: i64,
    db: &mut database::Writer,
    index: &index::Writer,
) -> repository::Result<()> {
    // Delete in database.
    let query = sqlx::query!(
        "DELETE FROM recipe \
         WHERE id = ?",
        id
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    // Delete in index.
    index.delete(id)?;

    Ok(())
}

/// Delete all recipes.
///
/// This is a cascading operation. All ingredients, instructions and categories will be deleted.
pub async fn clear(
    db: &mut database::Writer,
    index: &index::Writer,
) -> repository::Result<()> {
    // Delete in database.
    let query = sqlx::query!(
        "DELETE FROM recipe"
    );
    query
        .execute(db)
        .await?;

    // Delete in index.
    index.clear()?;

    Ok(())
}