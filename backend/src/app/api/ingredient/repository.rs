use database::extension::{foreign_key_to_not_found, SqliteResultExt};

use crate::{database, repository};

/// Ingredient entity, as represented in database.
#[derive(Debug)]
pub struct IngredientEntity {
    pub name: String,
    pub quantity: String,
    pub comment: String,
}

/// Find all ingredients of a recipe.
///
/// Does not check if the recipe exist.
pub async fn find_by_recipe_id<'c, C>(
    recipe_id: i64,
    db: &'c mut C,
) -> repository::Result<Vec<IngredientEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        IngredientEntity,
        "SELECT \
             name, \
             quantity, \
             comment \
         FROM \
             ingredient \
         WHERE \
             recipe_id = ? \
         ORDER BY \
             `index`",
        recipe_id
    );
    let ingredients = query
        .fetch_all(db)
        .await?;
    Ok(ingredients)
}

/// Set the ingredients of a recipe.
///
/// Returns an error if the recipe could not be found.
pub async fn set_for_recipe(
    recipe_id: i64,
    ingredients: Vec<IngredientEntity>,
    db: &mut database::Writer,
) -> repository::Result<Vec<IngredientEntity>> {
    delete_for_recipe(recipe_id, db).await?;
    insert_for_recipe(recipe_id, &ingredients, db).await?;
    Ok(ingredients)
}

/// Add ingredients to a recipe.
/// Assumes that there are no other ingredients currently assigned to the recipe.
///
/// Returns an error if the recipe could not be found.
async fn insert_for_recipe(
    recipe_id: i64,
    ingredients: &[IngredientEntity],
    db: &mut database::Writer,
) -> repository::Result<()> {
    for (i, ingredient) in ingredients.iter().enumerate() {
        let index = i as i64;
        let query = sqlx::query!(
            "INSERT INTO ingredient (\
                 recipe_id, \
                 `index`, \
                 name, \
                 quantity, \
                 comment\
             ) VALUES (\
                 ?,?,?,?,?\
             )",
            recipe_id,
            index,
            ingredient.name,
            ingredient.quantity,
            ingredient.comment,
        );
        query
            .execute(&mut *db)
            .await
            .map_error_code(foreign_key_to_not_found)?;
    }
    Ok(())
}

/// Delete all ingredients of a recipe.
///
/// Does not check if the recipe exist.
async fn delete_for_recipe(
    recipe_id: i64,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "DELETE FROM ingredient \
         WHERE recipe_id = ?",
        recipe_id
    );
    query
        .execute(&mut *db)
        .await?;
    Ok(())
}