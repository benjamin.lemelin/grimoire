use serde::{Deserialize, Serialize};

use crate::app::api::ingredient::repository::IngredientEntity;

/// Recipe ingredient DTO.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct IngredientDTO {
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub name: String,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub quantity: String,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub comment: String,
}

impl IngredientDTO {
    /// Create from database entity.
    pub fn from_entity(ingredient: IngredientEntity) -> Self {
        Self {
            name: ingredient.name,
            quantity: ingredient.quantity,
            comment: ingredient.comment,
        }
    }

    /// Convert to database entity.
    pub fn into_entity(self) -> IngredientEntity {
        IngredientEntity {
            name: self.name,
            quantity: self.quantity,
            comment: self.comment,
        }
    }
}