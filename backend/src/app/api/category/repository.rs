use database::extension::{foreign_key_to_not_found, SqliteResultExt};

use crate::{database, repository};

/// Category entity, as represented in database.
pub type CategoryEntity = String;

/// Find all categories.
pub async fn find_all<'c, C>(
    db: &'c mut C,
) -> repository::Result<Vec<CategoryEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_scalar!(
        "SELECT \
             name \
         FROM \
             category \
         ORDER BY \
             name",
    );
    let categories = query
        .fetch_all(db)
        .await?;
    Ok(categories)
}

/// Find all categories of a recipe.
///
/// Does not check if the recipe exist.
pub async fn find_by_recipe_id<'c, C>(
    recipe_id: i64,
    db: &'c mut C,
) -> repository::Result<Vec<CategoryEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_scalar!(
        "SELECT \
             category_name \
         FROM \
             recipe_category_mapping \
         WHERE \
             recipe_id = ? \
         ORDER BY \
             category_name",
        recipe_id
    );
    let categories = query
        .fetch_all(db)
        .await?;
    Ok(categories)
}

/// Set the categories of a recipe.
///
/// Returns an error if the recipe could not be found.
pub async fn set_for_recipe(
    recipe_id: i64,
    categories: Vec<CategoryEntity>,
    db: &mut database::Writer,
) -> repository::Result<Vec<CategoryEntity>> {
    delete_for_recipe(recipe_id, db).await?;
    insert_for_recipe(recipe_id, &categories, db).await?;
    Ok(categories)
}

/// Add categories to a recipe, creating them if they do not exist.
/// Assumes the categories are not already assigned to the recipe.
///
/// Returns an error if the recipe could not be found.
async fn insert_for_recipe(
    recipe_id: i64,
    categories: &Vec<CategoryEntity>,
    db: &mut database::Writer,
) -> repository::Result<()> {
    for category in categories {
        // There is a trigger that inserts missing categories inside the "category" table
        // just before it is inserted into the "recipe_category_mapping" table.

        let query = sqlx::query!(
            "INSERT INTO recipe_category_mapping (\
                 recipe_id, \
                 category_name\
             ) VALUES (\
                 ?,?\
             )",
            recipe_id,
            category
        );
        query
            .execute(&mut *db)
            .await
            .map_error_code(foreign_key_to_not_found)?;
    }
    Ok(())
}

/// Delete all categories of a recipe, deleting them if they go unused.
///
/// Does not check if the recipe exist.
async fn delete_for_recipe(
    recipe_id: i64,
    db: &mut database::Writer,
) -> repository::Result<()> {
    // There is a trigger that deletes unused categories inside the "category" table
    // just after it is deleted from the "recipe_category_mapping" table.

    let query = sqlx::query!(
        "DELETE FROM recipe_category_mapping \
         WHERE recipe_id = ?",
        recipe_id
    );
    query
        .execute(&mut *db)
        .await?;
    Ok(())
}