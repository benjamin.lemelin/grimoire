use axum::{debug_handler, Router, routing::get};
use axum::extract::{Json, State};
use axum::response::IntoResponse;

use crate::app::api::{
    self,
    category::{extract::CategoryDTO, repository as category_repository},
    session::Session,
};

/// Category Api routes.
pub(in crate::app::api) fn routes() -> Router<api::State> {
    Router::new()
        .route("/categories", get(find_all))
}

/// Find all recipe categories.
#[debug_handler]
async fn find_all(
    _session: Session,
    State(ctx): State<api::State>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let categories: Vec<CategoryDTO> = category_repository::find_all(&mut db).await?;
    Ok(Json(categories))
}