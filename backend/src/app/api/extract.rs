use axum::{async_trait, http::{Request, request::Parts as RequestParts, StatusCode}};
use axum::extract::{FromRequest, FromRequestParts, Json, Query, rejection::{JsonRejection, QueryRejection}};
use axum::response::{IntoResponse, Response};
use serde::de::DeserializeOwned;
use tracing::{error, info};
use validator::{Validate, ValidationErrors};

/// Validated url params.
#[derive(Debug)]
pub struct ValidatedQuery<T>(pub T);

/// Validation url params rejection.
#[derive(Debug, thiserror::Error)]
pub enum ValidatedQueryRejection {
    /// Validation error (produces `400 Bad request`).
    #[error("invalid fields:\n{0}")]
    InvalidData(#[from] ValidationErrors),
    /// Invalid url params (produces various codes).
    #[error("{0}")]
    RejectedQuery(#[from] QueryRejection),
}

#[async_trait]
impl<T, S> FromRequestParts<S> for ValidatedQuery<T>
    where
        Query<T>: FromRequestParts<S, Rejection=QueryRejection>,
        T: DeserializeOwned + Validate,
        S: Send + Sync
{
    type Rejection = ValidatedQueryRejection;

    async fn from_request_parts(parts: &mut RequestParts, state: &S) -> Result<Self, Self::Rejection> {
        let Query(value): Query<T> = Query::from_request_parts(parts, state).await?;
        value.validate()?;
        Ok(Self(value))
    }
}

impl IntoResponse for ValidatedQueryRejection {
    fn into_response(self) -> Response {
        info!("{self}");

        match self {
            Self::InvalidData(_) => {
                (StatusCode::BAD_REQUEST, self.to_string()).into_response()
            }
            Self::RejectedQuery(err) => {
                err.into_response()
            }
        }
    }
}

/// Validated json body.
#[derive(Debug)]
pub struct ValidatedJson<T>(pub T);

/// Validation json rejection.
#[derive(Debug, thiserror::Error)]
pub enum ValidatedJsonRejection {
    /// Validation error (produces `400 Bad request`).
    #[error("invalid fields:\n{0}")]
    InvalidData(#[from] ValidationErrors),
    /// Invalid json (produces various codes).
    #[error("{0}")]
    RejectedJson(#[from] JsonRejection),
}

#[async_trait]
impl<T, S, B> FromRequest<S, B> for ValidatedJson<T>
    where
        Json<T>: FromRequest<S, B, Rejection=JsonRejection>,
        T: DeserializeOwned + Validate,
        S: Send + Sync,
        B: Send + 'static
{
    type Rejection = ValidatedJsonRejection;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        let Json(value): Json<T> = Json::from_request(req, state).await?;
        value.validate()?;
        Ok(Self(value))
    }
}

impl IntoResponse for ValidatedJsonRejection {
    fn into_response(self) -> Response {
        info!("{self}");

        match self {
            Self::InvalidData(_) => {
                (StatusCode::BAD_REQUEST, self.to_string()).into_response()
            }
            Self::RejectedJson(err) => {
                err.into_response()
            }
        }
    }
}