use database::extension::{foreign_key_to_not_found, SqliteResultExt};

use crate::{database, repository};

/// Instruction entity, as represented in database.
#[derive(Debug)]
pub struct InstructionEntity {
    pub text: String,
}

/// Find all instructions of a recipe.
///
/// Does not check if the recipe exist.
pub async fn find_by_recipe_id<'c, C>(
    recipe_id: i64,
    db: &'c mut C,
) -> repository::Result<Vec<InstructionEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        InstructionEntity,
        "SELECT \
             text \
         FROM \
             instruction \
         WHERE \
             recipe_id = ? \
         ORDER BY \
             `index`",
        recipe_id
    );
    let instructions = query
        .fetch_all(db)
        .await?;
    Ok(instructions)
}

/// Set the instructions of a recipe.
///
/// Returns an error if the recipe could not be found.
pub async fn set_for_recipe(
    recipe_id: i64,
    instructions: Vec<InstructionEntity>,
    db: &mut database::Writer,
) -> repository::Result<Vec<InstructionEntity>> {
    delete_for_recipe(recipe_id, db).await?;
    insert_for_recipe(recipe_id, &instructions, db).await?;
    Ok(instructions)
}

/// Add instructions to a recipe.
/// Assumes that there are no other instructions currently assigned to the recipe.
///
/// Returns an error if the recipe could not be found.
async fn insert_for_recipe(
    recipe_id: i64,
    instructions: &[InstructionEntity],
    db: &mut database::Writer,
) -> repository::Result<()> {
    for (i, instruction) in instructions.iter().enumerate() {
        let index = i as i64;
        let query = sqlx::query!(
            "INSERT INTO instruction (\
                 recipe_id, \
                 `index`, \
                 text\
             ) VALUES (\
                 ?,?,?\
             )",
            recipe_id,
            index,
            instruction.text
        );
        query
            .execute(&mut *db)
            .await
            .map_error_code(foreign_key_to_not_found)?;
    }
    Ok(())
}

/// Delete all instructions of a recipe.
///
/// Does not check if the recipe exist.
async fn delete_for_recipe(
    recipe_id: i64,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "DELETE FROM instruction \
         WHERE recipe_id = ?",
        recipe_id
    );
    query
        .execute(&mut *db)
        .await?;
    Ok(())
}