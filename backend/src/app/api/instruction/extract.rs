use serde::{Deserialize, Serialize};

use crate::app::api::instruction::repository::InstructionEntity;

/// Recipe instruction DTO.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InstructionDTO {
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub text: String,
}

impl InstructionDTO {
    /// Create from database entity.
    pub fn from_entity(instruction: InstructionEntity) -> Self {
        Self {
            text: instruction.text,
        }
    }

    /// Convert to database entity.
    pub fn into_entity(self) -> InstructionEntity {
        InstructionEntity {
            text: self.text,
        }
    }
}