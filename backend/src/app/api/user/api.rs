use axum::{debug_handler, http::StatusCode, Router, routing::{get, post, put}};
use axum::extract::{Json, Path, State};
use axum::response::IntoResponse;

use crate::app::api::{
    self,
    extract::ValidatedJson,
    session::{AdminSession, AnySession, ExpiredSession, OpenedSession, Session, UserAgent},
    user::{
        extract::{
            NewUserDTO,
            UpdateUsernameDTO,
            UpdateUserPasswordDTO,
            UpdateUserProfileDTO,
            UpdateUserRoleDTO,
            UserDTO,
            UserLoginDTO,
        },
        Role,
        service as user_service,
        User,
    },
};

/// User Api routes.
pub(in crate::app::api) fn routes() -> Router<api::State> {
    Router::new()
        .route("/users", get(find_all).post(add))
        .route("/users/:username", get(find_by_username).delete(delete))
        .route("/users/:username/username", put(update_username))
        .route("/users/:username/password", put(update_password))
        .route("/users/:username/profile", put(update_profile))
        .route("/users/:username/role", put(update_role))
        .route("/users/login", post(login))
        .route("/users/logout", post(logout))
}

/// Find all users.
#[debug_handler]
async fn find_all(
    _session: AdminSession,
    State(ctx): State<api::State>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let users = user_service::find_all(&mut db).await?;
    let dto = UserDTO::from_users(users);
    Ok(Json(dto))
}

/// Fetch current user details.
#[debug_handler]
async fn find_by_username(
    AnySession(_, user): AnySession,
    State(ctx): State<api::State>,
    Path(username): Path<String>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;

    let user = if user.username() == username {
        user
    } else if user.has_role(Role::Admin) {
        user_service::find_by_username(&username, &mut db).await?
    } else {
        return Err(api::Error::Forbidden);
    };

    let dto = UserDTO::from_user(user);
    Ok(Json(dto))
}

/// Add a new user.
#[debug_handler]
async fn add(
    _session: AdminSession,
    State(ctx): State<api::State>,
    ValidatedJson(user): ValidatedJson<NewUserDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    user_service::create(user, &mut db).await?;
    db.commit().await?;
    Ok(StatusCode::CREATED)
}

/// Delete a user.
#[debug_handler]
async fn delete(
    AnySession(_, user): AnySession,
    State(ctx): State<api::State>,
    Path(username): Path<String>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;

    if !is_admin_or_owner(&user, &username) {
        return Err(api::Error::Forbidden);
    }

    user_service::delete(&username, &mut db).await?;
    db.commit().await?;
    Ok(())
}

/// Update an username.
#[debug_handler]
async fn update_username(
    AnySession(_, user): AnySession,
    State(ctx): State<api::State>,
    Path(username): Path<String>,
    ValidatedJson(update): ValidatedJson<UpdateUsernameDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;

    if !is_admin_or_owner(&user, &username) {
        return Err(api::Error::Forbidden);
    }

    user_service::update_username(&username, &update.username, &mut db).await?;
    db.commit().await?;
    Ok(())
}

/// Update user password.
#[debug_handler]
async fn update_password(
    AnySession(_, user): AnySession,
    State(ctx): State<api::State>,
    Path(username): Path<String>,
    ValidatedJson(update): ValidatedJson<UpdateUserPasswordDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;

    if !is_admin_or_owner(&user, &username) {
        return Err(api::Error::Forbidden);
    }

    user_service::update_password(&username, update, &mut db).await?;
    db.commit().await?;
    Ok(())
}

/// Update user profile.
#[debug_handler]
async fn update_profile(
    AnySession(_, user): AnySession,
    State(ctx): State<api::State>,
    Path(username): Path<String>,
    ValidatedJson(update): ValidatedJson<UpdateUserProfileDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;

    if !is_admin_or_owner(&user, &username) {
        return Err(api::Error::Forbidden);
    }

    user_service::update_profile(&username, update, &mut db).await?;
    db.commit().await?;
    Ok(())
}

/// Update user role.
#[debug_handler]
async fn update_role(
    _session: AdminSession,
    State(ctx): State<api::State>,
    Path(username): Path<String>,
    ValidatedJson(update): ValidatedJson<UpdateUserRoleDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    user_service::update_role(&username, update, &mut db).await?;
    db.commit().await?;
    Ok(())
}

/// Authenticate user and create session.
#[debug_handler]
async fn login(
    State(ctx): State<api::State>,
    user_agent: UserAgent,
    ValidatedJson(login): ValidatedJson<UserLoginDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    let sessions = &ctx.sessions;

    match user_service::find_by_credentials(&login.username, &login.password, &mut db).await {
        Ok(user) => {
            let session = sessions.create(&user, &user_agent, &mut db).await?;
            let dto = UserDTO::from_user(user);
            db.commit().await?;
            Ok((OpenedSession(session, ctx.secure_cookies), Json(dto)))
        }
        Err(e) => {
            Err(e)
        }
    }
}

/// Invalidate user session.
#[debug_handler]
async fn logout(
    session: Session,
    State(ctx): State<api::State>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    let sessions = &ctx.sessions;

    sessions.invalidate(session, &mut db).await?;
    db.commit().await?;
    Ok(ExpiredSession)
}

/// Checks if a user has rights over a certain account, by checking if it is their own account
/// or if they are an admin.
fn is_admin_or_owner(user: &User, username: &str) -> bool {
    user.has_role(Role::Admin) || user.username() == username
}