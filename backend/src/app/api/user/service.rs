use argon2::{Argon2, password_hash::{self, SaltString}, PasswordHash, PasswordHasher, PasswordVerifier};

use crate::{database, repository};
use crate::app::api::{
    self,
    user::{
        extract::{
            NewUserDTO,
            UpdateUserPasswordDTO,
            UpdateUserProfileDTO,
            UpdateUserRoleDTO,
        },
        repository as user_repository,
        Role,
        User,
    },
};

/// Find all users.
pub(in crate::app::api) async fn find_all<'c, C>(
    db: &'c mut C,
) -> api::Result<Vec<User>>
    where &'c mut C: database::Connection<'c> {
    let users = user_repository::find_all(db).await?;
    Ok(User::from_entities(users))
}

/// Find user by id.
///
/// Returns an error if the user doesn't exist.
pub(in crate::app::api) async fn find_by_id<'c, C>(
    id: i64,
    db: &'c mut C,
) -> api::Result<User>
    where &'c mut C: database::Connection<'c> {
    let user = user_repository::find_by_id(id, db).await?;
    Ok(User::from_entity(user))
}

/// Find user by username.
///
/// Returns an error if the user doesn't exist.
pub(in crate::app::api) async fn find_by_username<'c, C>(
    username: &str,
    db: &'c mut C,
) -> api::Result<User>
    where &'c mut C: database::Connection<'c> {
    let user = user_repository::find_by_username(username, db).await?;
    Ok(User::from_entity(user))
}

/// Find user using provided credentials.
///
/// This implements all the expected security features, such as password hashing.
///
/// Returns an error if the user doesn't exists or the credentials are wrong.
pub(in crate::app::api) async fn find_by_credentials<'c, C>(
    username: &str,
    password: &str,
    db: &'c mut C,
) -> api::Result<User>
    where &'c mut C: database::Connection<'c> {
    let argon2 = Argon2::default();

    match user_repository::find_by_username(username, db).await {
        Ok(user) => match argon2.verify_password(password.as_bytes(), &PasswordHash::new(&user.password)?) {
            Ok(_) => Ok(User::from_entity(user)),
            Err(password_hash::Error::Password) => Err(api::Error::WrongCredentials),
            Err(e) => Err(e.into())
        },
        Err(repository::Error::NotFound) => Err(api::Error::WrongCredentials),
        Err(e) => Err(e.into())
    }
}

/// Create user.
///
/// This implements all the expected security features, such as password hashing.
///
/// Returns an error if the user already exists.
pub(in crate::app::api) async fn create(
    user: NewUserDTO,
    db: &mut database::Writer,
) -> api::Result<User> {
    let mut user = user.into_entity();

    let argon2 = Argon2::default();
    let salt = SaltString::generate(&mut rand::thread_rng());
    user.password = argon2.hash_password(user.password.as_bytes(), &salt)?.to_string();
    user = user_repository::insert(user, db).await?;

    Ok(User::from_entity(user))
}

/// Update username.
///
/// Returns an error if the user doesn't exists.
pub(in crate::app::api) async fn update_username(
    old: &str,
    new: &str,
    db: &mut database::Writer,
) -> api::Result<()> {
    user_repository::update_username(old, new, db).await?;

    Ok(())
}

/// Update user password.
///
/// This implements all the expected security features, such as password hashing.
/// This also verifies the current user password.
///
/// Returns an error if the user doesn't exists.
pub(in crate::app::api) async fn update_password(
    username: &str,
    UpdateUserPasswordDTO { old, new }: UpdateUserPasswordDTO,
    db: &mut database::Writer,
) -> api::Result<()> {
    let argon2 = Argon2::default();

    match user_repository::find_by_username(username, db).await {
        Ok(user) => match argon2.verify_password(old.as_bytes(), &PasswordHash::new(&user.password)?) {
            Ok(_) => {
                let salt = SaltString::generate(&mut rand::thread_rng());
                let password = argon2.hash_password(new.as_bytes(), &salt)?.to_string();

                user_repository::update_password(user.id, &password, db).await?;
                Ok(())
            }
            Err(password_hash::Error::Password) => Err(api::Error::WrongCredentials),
            Err(e) => Err(e.into())
        },
        Err(repository::Error::NotFound) => Err(api::Error::WrongCredentials),
        Err(e) => Err(e.into())
    }
}

/// Update user profile.
///
/// Returns an error if the user doesn't exists.
pub(in crate::app::api) async fn update_profile(
    username: &str,
    UpdateUserProfileDTO { email, first_name, last_name }: UpdateUserProfileDTO,
    db: &mut database::Writer,
) -> api::Result<()> {
    user_repository::update_profile(username, &email, &first_name, &last_name, db).await?;

    Ok(())
}

/// Update user role.
///
/// Returns an error if the user doesn't exists.
pub(in crate::app::api) async fn update_role(
    username: &str,
    UpdateUserRoleDTO { role }: UpdateUserRoleDTO,
    db: &mut database::Writer,
) -> api::Result<()> {
    // Be careful not to remove the only remaining admin account.
    let user = find_by_username(username, db).await?;
    if user.has_role(Role::Admin) && count_admins(db).await? == 1 {
        return Err(api::Error::Forbidden);
    }

    // Safe to update.
    user_repository::update_role(username, role.into_integer(), db).await?;

    Ok(())
}

/// Delete user by username.
///
/// Returns an error if the user doesn't exists.
pub(in crate::app::api) async fn delete(
    username: &str,
    db: &mut database::Writer,
) -> api::Result<()> {
    // Be careful not to delete the only remaining admin account.
    let user = find_by_username(username, db).await?;
    if user.has_role(Role::Admin) && count_admins(db).await? == 1 {
        return Err(api::Error::Forbidden);
    }

    // Safe to delete.
    user_repository::delete_by_id(user.id, db).await?;

    Ok(())
}

/// Count how many admin users there is.
pub(in crate::app::api) async fn count_admins<'c, C>(
    db: &'c mut C,
) -> api::Result<i64>
    where &'c mut C: database::Connection<'c> {
    Ok(user_repository::count_admin(db).await?)
}