use database::extension::{SqliteQueryResultExt, SqliteResultExt, unique_to_already_exists};

use crate::{database, repository};
use crate::app::api::user::Role;

/// User entity, as represented in database.
#[derive(Debug)]
pub struct UserEntity {
    pub id: i64,
    pub username: String,
    pub email: String,
    pub password: String,
    pub first_name: String,
    pub last_name: String,
    pub role: i64,
}

/// Find all users.
pub(super) async fn find_all<'c, C>(
    db: &'c mut C,
) -> repository::Result<Vec<UserEntity>>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        UserEntity,
        "SELECT \
             id, \
             username, \
             email, \
             password, \
             first_name, \
             last_name, \
             role \
         FROM \
             user \
         ORDER BY \
             username"
    );
    let users = query
        .fetch_all(db)
        .await?;
    Ok(users)
}

/// Find user by id.
///
/// Returns an error if the user could not be found.
pub(super) async fn find_by_id<'c, C>(
    id: i64,
    db: &'c mut C,
) -> repository::Result<UserEntity>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        UserEntity,
        "SELECT \
             id, \
             username, \
             email, \
             password, \
             first_name, \
             last_name, \
             role \
         FROM \
             user \
         WHERE \
             id = ?",
        id
    );
    let user = query
        .fetch_one(db)
        .await?;
    Ok(user)
}

/// Find user by username.
///
/// Returns an error if the user could not be found.
pub(super) async fn find_by_username<'c, C>(
    username: &str,
    db: &'c mut C,
) -> repository::Result<UserEntity>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        UserEntity,
        "SELECT \
             id, \
             username, \
             email, \
             password, \
             first_name, \
             last_name, \
             role \
         FROM \
             user \
         WHERE \
             username = ?",
        username
    );
    let user = query
        .fetch_one(db)
        .await?;
    Ok(user)
}

/// Add a new user.
///
/// Returns an error in case the user already exists.
pub(super) async fn insert(
    mut user: UserEntity,
    db: &mut database::Writer,
) -> repository::Result<UserEntity> {
    let query = sqlx::query_scalar!(
        "INSERT INTO user (\
             username, \
             email, \
             password, \
             first_name, \
             last_name, \
             role \
         ) VALUES (\
             ?,?,?,?,?,?\
         ) RETURNING id as `id:_`",
        user.username,
        user.email,
        user.password,
        user.first_name,
        user.last_name,
        user.role
    );
    user.id = query
        .fetch_one(db)
        .await
        .map_error_code(unique_to_already_exists)?;

    Ok(user)
}

/// Update an username.
///
/// Returns an error if the user could not be found.
pub(super) async fn update_username(
    old: &str,
    new: &str,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "UPDATE user \
         SET \
             username = ? \
         WHERE \
             username = ?",
        new,
        old
    );
    query
        .execute(db)
        .await
        .map_error_code(unique_to_already_exists)?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Update user's password.
///
/// Returns an error if the user could not be found.
pub(super) async fn update_password(
    id: i64,
    password: &str,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "UPDATE user \
         SET \
             password = ? \
         WHERE \
             id = ?",
        password,
        id
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Update user's profile.
///
/// Returns an error if the user could not be found.
pub(super) async fn update_profile(
    username: &str,
    email: &str,
    first_name: &str,
    last_name: &str,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "UPDATE user \
         SET \
             email = ?, \
             first_name = ?, \
             last_name = ? \
         WHERE \
             username = ?",
        email,
        first_name,
        last_name,
        username
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Update user role.
///
/// Returns an error if the user could not be found.
pub(super) async fn update_role(
    username: &str,
    role: i64,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "UPDATE user \
         SET \
             role = ? \
         WHERE \
             username = ?",
        role,
        username
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Delete a user.
///
/// This is a cascading operation. Sessions will also be deleted.
///
/// Returns an error if the user could not be found.
pub(super) async fn delete_by_id(
    id: i64,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "DELETE FROM user \
         WHERE id = ?",
        id
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Admin user count.
pub(super) async fn count_admin<'c, C>(
    db: &'c mut C,
) -> repository::Result<i64>
    where &'c mut C: database::Connection<'c> {
    let admin_role = Role::Admin.into_integer();

    let query = sqlx::query_scalar!(
        "SELECT \
             COUNT(id) as `count:_` \
         FROM \
             user \
         WHERE \
             role = ?",
        admin_role
    );
    let count = query
        .fetch_one(db)
        .await?;
    Ok(count)
}