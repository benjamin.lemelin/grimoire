use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use validator::Validate;
use regex::Regex;

use crate::app::api::user::{
    repository::UserEntity,
    Role,
    User,
};

lazy_static! {
    // Accepts empty strings or complete emails.
    static ref EMAIL_REGEX: Regex = Regex::new(r"(.{0}|(.+@.+\..+))").expect("email regex should be valid");
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct UserDTO {
    pub username: String,
    pub email: String,
    pub first_name: String,
    pub last_name: String,
    pub role: RoleDTO,
}

impl UserDTO {
    /// Create from user.
    pub fn from_user(user: User) -> Self {
        Self {
            username: user.username,
            email: user.email,
            first_name: user.first_name,
            last_name: user.last_name,
            role: RoleDTO::from_role(user.role),
        }
    }

    /// Create from users.
    pub fn from_users(users: Vec<User>) -> Vec<Self> {
        users.into_iter().map(Self::from_user).collect()
    }
}

/// Role DTO.
#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum RoleDTO {
    Guest,
    User,
    Admin,
}

impl RoleDTO {
    /// Create from role.
    pub fn from_role(role: Role) -> Self {
        match role {
            Role::Guest => Self::Guest,
            Role::User => Self::User,
            Role::Admin => Self::Admin,
        }
    }

    /// Convert to role.
    pub fn into_role(self) -> Role {
        match self {
            Self::Guest => Role::Guest,
            Self::User => Role::User,
            Self::Admin => Role::Admin,
        }
    }

    /// Convert to integer.
    pub fn into_integer(self) -> i64 {
        self.into_role().into_integer()
    }
}

/// User Login DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UserLoginDTO {
    #[validate(length(min = 1, message = "must be at least one character"))]
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub username: String,
    #[validate(length(min = 1, message = "must be at least one character"))]
    pub password: String,
}

/// New User DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct NewUserDTO {
    #[validate(length(min = 1, message = "must be at least one character"))]
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub username: String,
    #[validate(length(min = 8, message = "must be at least 8 characters long"))]
    #[validate(length(max = 256, message = "must be at most 256 characters long"))]
    pub password: String,
    pub role: RoleDTO,
}

impl NewUserDTO {
    /// Convert to database entity.
    pub fn into_entity(self) -> UserEntity {
        UserEntity {
            id: 0,
            username: self.username,
            email: "".to_string(),
            password: self.password,
            first_name: "".to_string(),
            last_name: "".to_string(),
            role: self.role.into_integer(),
        }
    }
}

/// Username update DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UpdateUsernameDTO {
    #[validate(length(min = 1, message = "must be at least one character"))]
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub username: String,
}

/// Password update DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UpdateUserPasswordDTO {
    #[validate(length(min = 1, message = "must be at least one character"))]
    pub old: String,
    #[validate(length(min = 8, message = "must be at least 8 characters long"))]
    #[validate(length(max = 256, message = "must be at most 256 characters long"))]
    pub new: String,
}

/// User profile update DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UpdateUserProfileDTO {
    #[validate(regex(path = "EMAIL_REGEX", message = "must be a well formed email"))]
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub email: String,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub first_name: String,
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub last_name: String,
}

/// User role update DTO.
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct UpdateUserRoleDTO {
    pub role: RoleDTO,
}