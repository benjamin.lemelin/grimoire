use tracing::log::warn;

use repository::UserEntity;

pub mod api;
pub mod extract;
pub mod repository;
pub mod service;

/// User.
#[derive(Debug)]
pub struct User {
    id: i64,
    username: String,
    email: String,
    first_name: String,
    last_name: String,
    role: Role,
}

impl User {
    /// Create from database entity.
    fn from_entity(user: UserEntity) -> Self {
        Self {
            id: user.id,
            username: user.username,
            email: user.email,
            first_name: user.first_name,
            last_name: user.last_name,
            role: Role::from_integer(user.role),
        }
    }

    /// Create from database entities.
    fn from_entities(users: Vec<UserEntity>) -> Vec<Self> {
        users.into_iter().map(Self::from_entity).collect()
    }

    /// Id.
    pub fn id(&self) -> i64 {
        self.id
    }

    // Username.
    pub fn username(&self) -> &str {
        &self.username
    }

    /// Verify if user has at least the specified role.
    pub fn has_role(&self, role: Role) -> bool {
        self.role >= role
    }
}

/// Role.
#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq, PartialEq)]
pub enum Role {
    Guest,
    User,
    Admin,
}

impl Role {
    /// Create from integer.
    fn from_integer(role: i64) -> Self {
        match role {
            1 => Self::Guest,
            2 => Self::User,
            3 => Self::Admin,
            other => {
                warn!("user role not in range (value of {other}, expected between 1 and 3)");
                Self::Guest
            }
        }
    }

    /// Convert to integer.
    fn into_integer(self) -> i64 {
        match self {
            Self::Guest => 1,
            Self::User => 2,
            Self::Admin => 3,
        }
    }
}