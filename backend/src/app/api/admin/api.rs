use std::collections::HashMap;

use axum::{debug_handler, Router, routing::{get}};
use axum::extract::State;
use axum::response::{IntoResponse, Json};

use crate::app::api::{
    self,
    admin::extract::{BackupDTO, SetupDTO},
    category::repository as category_repository,
    extract::ValidatedJson,
    image::{extract::ImageDTO, repository as image_repository},
    ingredient::repository as ingredient_repository,
    instruction::repository as instruction_repository,
    recipe::{extract::RecipeDTO, repository as recipe_repository},
    session::AdminSession,
    user::service as user_service,
};

/// Admin Api routes.
pub(in crate::app::api) fn routes() -> Router<api::State> {
    use axum::extract::DefaultBodyLimit;

    Router::new()
        .route("/admin/setup", get(setup_required).post(create_setup_user))
        .route("/admin/database", get(export).put(import).delete(clear).layer(DefaultBodyLimit::disable()))
}

/// First user creation required.
#[debug_handler]
async fn setup_required(
    State(ctx): State<api::State>,
) -> api::Result<Json<bool>> {
    let mut db = ctx.database.write().await?;
    Ok(Json(user_service::count_admins(&mut db).await? == 0))
}

/// Create first user (only once).
#[debug_handler]
async fn create_setup_user(
    State(ctx): State<api::State>,
    ValidatedJson(setup): ValidatedJson<SetupDTO>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;

    // Only allowed when no admin user exists.
    if user_service::count_admins(&mut db).await? > 0 {
        return Err(api::Error::Forbidden);
    }

    // Creation of the first admin user.
    let user = setup.into_new_user_dto();
    user_service::create(user, &mut db).await?;
    db.commit().await?;

    Ok(())
}

/// Export database to a zip.
#[debug_handler]
async fn export(
    _session: AdminSession,
    State(ctx): State<api::State>,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.read().await?;
    let image_storage = &ctx.images;

    let mut recipes = Vec::new();
    let mut images = HashMap::new();

    for recipe in recipe_repository::find_all(&mut db).await? {
        // Recipe.
        let id = recipe.id;
        let categories = category_repository::find_by_recipe_id(id, &mut db).await?;
        let ingredients = ingredient_repository::find_by_recipe_id(id, &mut db).await?;
        let instructions = instruction_repository::find_by_recipe_id(id, &mut db).await?;
        let recipe = RecipeDTO::from_entity(recipe, categories, ingredients, instructions);
        let has_image = recipe.has_image;
        recipes.push(recipe);

        // Recipe image.
        if has_image {
            let image = image_repository::find_by_recipe(id, image_storage).await?;
            images.insert(id, ImageDTO::from_entity(image));
        }
    }

    Ok(BackupDTO { recipes, images })
}

/// Import database from a zip.
#[debug_handler]
async fn import(
    _session: AdminSession,
    State(ctx): State<api::State>,
    BackupDTO { recipes, mut images }: BackupDTO,
) -> api::Result<impl IntoResponse> {
    let mut db = ctx.database.write().await?;
    let index = ctx.index.write().await?;
    let image_storage = &ctx.images;

    recipe_repository::clear(&mut db, &index).await?;
    image_storage.clear().await?;

    for recipe in recipes {
        let (recipe, categories, ingredients, instructions) = recipe.into_entity();
        let image = images.remove(&recipe.id).map(ImageDTO::into_entity);

        let recipe = recipe_repository::insert(recipe, &mut db, &index).await?;
        category_repository::set_for_recipe(recipe.id, categories, &mut db).await?;
        ingredient_repository::set_for_recipe(recipe.id, ingredients, &mut db).await?;
        instruction_repository::set_for_recipe(recipe.id, instructions, &mut db).await?;
        image_repository::set_for_recipe(recipe.id, image, image_storage).await?;
    }

    db.commit().await?;
    index.commit().await?;

    Ok(())
}

/// Clear everything from database.
#[debug_handler]
async fn clear(
    _session: AdminSession,
    State(ctx): State<api::State>,
) -> api::Result<()> {
    let mut db = ctx.database.write().await?;
    let index = ctx.index.write().await?;
    let images = &ctx.images;

    // Delete all recipes. Since this is a cascading operation, we do not need
    // to delete ingredients, instructions and categories.
    recipe_repository::clear(&mut db, &index).await?;

    // Delete all images.
    images.clear().await?;

    db.commit().await?;
    index.commit().await?;

    Ok(())
}