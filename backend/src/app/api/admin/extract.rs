use std::collections::HashMap;
use std::io::{self, Read, Seek, Write};

use axum::{async_trait, body::Bytes, http::{header, Request, StatusCode}};
use axum::extract::{FromRequest, rejection::BytesRejection};
use axum::response::{IntoResponse, Response};
use serde::Deserialize;
use tracing::{error, info, warn};
use validator::Validate;
use zip::{read::ZipFile, result::ZipError, ZipArchive, ZipWriter};

use crate::app::api::{
    image::extract::ImageDTO,
    ingredient::extract::IngredientDTO,
    instruction::extract::InstructionDTO,
    recipe::extract::{DifficultyDTO, DurationDTO, RecipeDTO},
    user::extract::{RoleDTO, NewUserDTO},
};

/// Setup DTO (first user account).
#[derive(Debug, Deserialize, Validate)]
#[serde(rename_all = "camelCase")]
pub struct SetupDTO {
    #[validate(length(min = 1, message = "must be at least one character"))]
    #[serde(deserialize_with = "serde_trim::string_trim")]
    pub username: String,
    #[validate(length(min = 8, message = "must be at least 8 characters long"))]
    #[validate(length(max = 256, message = "must be at most 256 characters long"))]
    pub password: String,
}

impl SetupDTO {
    /// Convert to `NewUserDTO`.
    pub fn into_new_user_dto(self) -> NewUserDTO {
        NewUserDTO {
            username: self.username,
            password: self.password,
            role: RoleDTO::Admin,
        }
    }
}

/// Database backup DTO.
#[derive(Debug)]
pub struct BackupDTO {
    pub recipes: Vec<RecipeDTO>,
    pub images: HashMap<i64, ImageDTO>,
}

/// Database backup DTO rejection.
#[derive(Debug, thiserror::Error)]
pub enum BackupDTORejection {
    /// Unsupported backup version (produces `400 Bad request`).
    #[error("unsupported backup version ({0})")]
    UnsupportedVersion(String),
    /// Missing required file inside backup (produces `400 Bad request`).
    #[error("required file \"{0}\" not found inside the backup")]
    FileNotFound(String),
    /// Invalid or corrupted JSON files inside backup (produces `400 Bad request`).
    #[error("invalid or corrupted JSON files found in backup")]
    InvalidJson(#[from] serde_json::Error),
    /// Invalid or corrupted image inside backup (produces `400 Bad request`).
    #[error("invalid or corrupted image")]
    InvalidImage(#[from] image::ImageError),
    /// Backup is not a valid zip file (produces `400 Bad request`).
    #[error("backup is not a zip file or is corrupted")]
    InvalidFormat(#[source] ZipError),
    /// Rejected bytes (produces various codes).
    #[error("{0}")]
    RejectedBytes(#[from] BytesRejection),
    /// Error while reading/writing the backup file (produces `500 Internal Server Error`).
    #[error("{0}")]
    Io(#[from] io::Error),
}

const METADATA_FILE_NAME: &str = "METADATA.txt";
const METADATA_V7: &str = "version:7;";
const METADATA_V8: &str = "version:8;";
const RECIPES_FILE_NAME_V7: &str = "RECIPES.json";
const RECIPE_IMAGES_FILE_NAME_V7: &str = "RECIPE_IMAGES.zip";
const RECIPES_FILE_NAME_V8: &str = "recipes.json";
const RECIPE_IMAGES_DIR_V8: &str = "images";

impl BackupDTO {
    fn from_bytes(bytes: &[u8]) -> Result<Self, BackupDTORejection> {
        let mut zip = ZipArchive::new(io::Cursor::new(bytes))?;
        let version = Self::read_version(&mut zip)?;

        match version.as_str() {
            METADATA_V7 => Self::read_recipes_v7(zip),
            METADATA_V8 => Self::read_recipes_v8(zip),
            _ => Err(BackupDTORejection::UnsupportedVersion(version)),
        }
    }

    fn to_bytes(&self) -> Result<Vec<u8>, BackupDTORejection> {
        let mut zip = ZipWriter::new(io::Cursor::new(Vec::new()));
        Self::write_recipes_v8(&self.recipes, &self.images, &mut zip)?;
        Ok(zip.finish()?.into_inner())
    }

    fn read_version<R: Read + Seek>(zip: &mut ZipArchive<R>) -> Result<String, BackupDTORejection> {
        let mut version = String::with_capacity(METADATA_V7.len());
        zip.by_name_ext(METADATA_FILE_NAME)?.read_to_string(&mut version)?;
        Ok(version)
    }

    fn read_recipes_v7<R: Read + Seek>(mut zip: ZipArchive<R>) -> Result<Self, BackupDTORejection> {
        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct RecipeV7 {
            id: String,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            name: String,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            author: String,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            description: String,
            duration_in_seconds: i64,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            r#yield: String,
            difficulty: i64,
            is_favorite: bool,
            categories: Vec<String>,
            ingredients: Vec<IngredientV7>,
            instructions: Vec<InstructionV7>,
        }

        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct IngredientV7 {
            #[serde(deserialize_with = "serde_trim::string_trim")]
            name: String,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            quantity: String,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            unit: String,
            #[serde(deserialize_with = "serde_trim::string_trim")]
            comment: String,
        }

        #[derive(Debug, Deserialize)]
        #[serde(rename_all = "camelCase")]
        struct InstructionV7 {
            #[serde(deserialize_with = "serde_trim::string_trim")]
            text: String,
        }

        let mut buffer = Vec::new();
        let recipes_v7 = {
            zip.by_name_ext(RECIPES_FILE_NAME_V7)?.read_to_end(&mut buffer)?;
            serde_json::from_slice::<Vec<RecipeV7>>(&buffer)?
        };
        let mut images_v7 = {
            buffer.clear();
            zip.by_name_ext(RECIPE_IMAGES_FILE_NAME_V7)?.read_to_end(&mut buffer)?;
            let mut images_zip = ZipArchive::new(io::Cursor::new(buffer))?;

            let mut images_v7 = HashMap::new();
            for file_name in images_zip.file_names().map(From::from).collect::<Vec<String>>() {
                let mut file = images_zip.by_name_ext(&file_name)?;
                let mut buffer = Vec::with_capacity(file.size() as usize);
                file.read_to_end(&mut buffer)?;
                images_v7.insert(file_name, buffer);
            }
            images_v7
        };

        let mut recipes = Vec::with_capacity(recipes_v7.len());
        let mut images = HashMap::with_capacity(images_v7.len());

        for (i, recipe_v7) in recipes_v7.into_iter().enumerate() {
            let id = i as i64;
            let image_v7 = images_v7.remove(&recipe_v7.id);

            recipes.push(RecipeDTO {
                id,
                name: recipe_v7.name,
                author: recipe_v7.author,
                description: recipe_v7.description,
                duration: DurationDTO { seconds: recipe_v7.duration_in_seconds },
                r#yield: recipe_v7.r#yield,
                difficulty: match recipe_v7.difficulty {
                    1 => DifficultyDTO::Easy,
                    3 => DifficultyDTO::Hard,
                    _ => DifficultyDTO::Normal,
                },
                is_favorite: recipe_v7.is_favorite,
                has_image: image_v7.is_some(),
                categories: recipe_v7.categories,
                ingredients: recipe_v7.ingredients.into_iter().map(|ingredient_v7| {
                    IngredientDTO {
                        name: ingredient_v7.name,
                        quantity: format!(
                            "{quantity} {unit}",
                            quantity = ingredient_v7.quantity,
                            unit = ingredient_v7.unit
                        ).trim().to_string(),
                        comment: ingredient_v7.comment,
                    }
                }).collect(),
                instructions: recipe_v7.instructions.into_iter().map(|instruction_v7| {
                    InstructionDTO {
                        text: instruction_v7.text
                    }
                }).collect(),
            });

            if let Some(image_v7) = image_v7 {
                images.insert(id, ImageDTO::from_raw(&image_v7)?);
            }
        }

        Ok(Self {
            recipes,
            images,
        })
    }

    fn read_recipes_v8<R: Read + Seek>(mut zip: ZipArchive<R>) -> Result<Self, BackupDTORejection> {
        let recipes_v8 = {
            let mut file = zip.by_name_ext(RECIPES_FILE_NAME_V8)?;
            serde_json::from_reader::<_, Vec<RecipeDTO>>(&mut file)?
        };

        let images_v8 = {
            let mut images_v8 = HashMap::new();
            for file_name in zip.file_names().filter(|it| it.starts_with(RECIPE_IMAGES_DIR_V8)).map(String::from).collect::<Vec<String>>() {
                let mut file = zip.by_name_ext(&file_name)?;
                match file_name.split('/').nth(1).and_then(|it: &str| it.parse::<i64>().ok()) {
                    Some(id) => {
                        let mut image = Vec::with_capacity(file.size() as usize);
                        file.read_to_end(&mut image)?;
                        images_v8.insert(id, ImageDTO::from_raw(&image)?);
                    }
                    None => {
                        warn!("image \"{file_name}\" inside backup has an invalid id or is completely missing : ignoring")
                    }
                }
            }
            images_v8
        };

        Ok(Self {
            recipes: recipes_v8,
            images: images_v8,
        })
    }

    fn write_recipes_v8<W: Write + Seek>(recipes: &Vec<RecipeDTO>, images: &HashMap<i64, ImageDTO>, zip: &mut ZipWriter<W>) -> Result<(), BackupDTORejection> {
        // Version
        zip.start_file(METADATA_FILE_NAME, Default::default())?;
        write!(&mut *zip, "{}", METADATA_V8)?;

        // Recipes
        zip.start_file(RECIPES_FILE_NAME_V8, Default::default())?;
        serde_json::to_writer(&mut *zip, &recipes)?;

        // Images
        for (id, image) in images {
            zip.start_file(&format!("{RECIPE_IMAGES_DIR_V8}/{id}"), Default::default())?;
            zip.write_all(image.as_raw())?;
        }

        Ok(())
    }
}

#[async_trait]
impl<S, B> FromRequest<S, B> for BackupDTO
    where
        Bytes: FromRequest<S, B, Rejection=BytesRejection>,
        S: Send + Sync,
        B: Send + 'static,
{
    type Rejection = BackupDTORejection;

    async fn from_request(req: Request<B>, state: &S) -> Result<Self, Self::Rejection> {
        Self::from_bytes(&Bytes::from_request(req, state).await?)
    }
}

impl IntoResponse for BackupDTO {
    fn into_response(self) -> Response {
        let headers = [
            (header::CONTENT_TYPE, "application/zip"),
            (header::CONTENT_DISPOSITION, "attachment;filename=\"Database.zip\"")
        ];
        let content = self.to_bytes();

        (headers, content).into_response()
    }
}

impl IntoResponse for BackupDTORejection {
    fn into_response(self) -> Response {
        match &self {
            BackupDTORejection::Io(_) => error!("{self}"),
            _ => info!("{self}"),
        }

        match self {
            Self::UnsupportedVersion(_) | Self::FileNotFound(_) | Self::InvalidImage(_) | Self::InvalidJson(_) | Self::InvalidFormat(_) => {
                (StatusCode::BAD_REQUEST, self.to_string()).into_response()
            }
            Self::RejectedBytes(err) => {
                err.into_response()
            }
            Self::Io(_) => {
                (StatusCode::INTERNAL_SERVER_ERROR).into_response()
            }
        }
    }
}

impl From<ZipError> for BackupDTORejection {
    fn from(err: ZipError) -> Self {
        match err {
            ZipError::InvalidArchive(_) | ZipError::UnsupportedArchive(_) => Self::InvalidFormat(err),
            ZipError::Io(err) => Self::Io(err),
            ZipError::FileNotFound => {
                error!("uncaught \"file not found\" error when reading database backup");
                Self::FileNotFound("<unknown>".into())
            }
        }
    }
}

trait ZipArchiveExt {
    fn by_name_ext(&mut self, name: &str) -> Result<ZipFile, BackupDTORejection>;
}

impl<R: Read + Seek> ZipArchiveExt for ZipArchive<R> {
    fn by_name_ext(&mut self, name: &str) -> Result<ZipFile, BackupDTORejection> {
        match self.by_name(name) {
            Err(ZipError::FileNotFound) => Err(BackupDTORejection::FileNotFound(name.into())),
            Err(ZipError::Io(e)) => Err(BackupDTORejection::Io(e)),
            Err(e) => Err(BackupDTORejection::InvalidFormat(e)),
            Ok(value) => Ok(value)
        }
    }
}