use std::collections::HashMap;
use std::sync::Arc;
use std::time::{Duration, Instant};
use tokio::sync::{mpsc, RwLock};
use tracing::error;

use crate::{database::{self, Database}, repository};
use crate::app::api::{
    self,
    session::{repository as session_repository, Session, SessionToken, Timestamp, UserAgent},
    user::User,
};

/// Session store.
#[derive(Debug, Clone)]
pub(in crate::app::api)  struct SessionStore {
    max_sessions: i64,
    last_accessed: Arc<RwLock<HashMap<SessionToken, Timestamp>>>,
    last_accessed_sender: mpsc::Sender<()>,
}

impl SessionStore {
    /// Create nwe session store.
    pub fn new(database: &Database, max_sessions: i64) -> Self {
        // Last access thread work queue. The channel is used to notify that work is available.
        // Thus, a buffer size of 1 is enough in this case.
        let last_accessed = Arc::new(RwLock::new(HashMap::new()));
        let (last_accessed_sender, last_accessed_receiver) = mpsc::channel(1);

        // Debounce the last accessed writes to the database, using a worker thread.
        Self::spawn_last_access_thread(database, last_accessed.clone(), last_accessed_receiver);

        Self {
            max_sessions,
            last_accessed,
            last_accessed_sender,
        }
    }

    /// Spawns the last access update thread.
    ///
    /// This is to debounce the last access updates to the database, as they can be very frequent.
    fn spawn_last_access_thread(
        database: &Database,
        last_accessed: Arc<RwLock<HashMap<SessionToken, Timestamp>>>,
        mut last_accessed_receiver: mpsc::Receiver<()>,
    ) {
        let database = database.clone();
        let debounce_min = Duration::from_secs(1);
        let debounce_max = Duration::from_secs(5);
        tokio::spawn(async move {
            while last_accessed_receiver.recv().await.is_some() {
                // Wait before doing any work. This is the debounce.
                // To ensure we are not locked in a forever waiting loop, there is a maximum amount
                // of time before we starting working.
                let start = Instant::now();
                while start.elapsed() < debounce_max {
                    // Sleep for the minimum amount of time.
                    tokio::time::sleep(debounce_min).await;
                    match last_accessed_receiver.try_recv() {
                        Err(mpsc::error::TryRecvError::Empty) |
                        Err(mpsc::error::TryRecvError::Disconnected) => {
                            // Nothing received or channel closed. Do the work now.
                            break;
                        }
                        Ok(_) => {
                            // We received something. Wait again.
                            continue
                        },
                    }
                }

                // Since we consumed what notified this thread, there is a good chance that
                // there will be a second run of this loop, but without any work to do.
                // Check this before opening an exclusive "write" connection to the database.
                let mut last_accessed = last_accessed.write().await;
                if !last_accessed.is_empty() {
                    // We can't send any error back, so we need to manage them. Here, we log any
                    // error that we encounter.
                    match database.write().await {
                        Ok(mut db) => {
                            for (token, timestamp) in last_accessed.drain() {
                                if let Err(e) = session_repository::update_last_accessed(&token, timestamp, &mut db).await {
                                    error!("Failed to update last access timestamp : {e}.")
                                }
                            }
                            if let Err(e) = db.commit().await {
                                error!("Failed to commit last access timestamps : {e}.")
                            }
                        },
                        Err(e) => error!("Failed to connect to database fo write last access timestamp : {e}.")
                    }
                }
            }
        });
    }

    /// Find session by id.
    ///
    /// This triggers an update the last access timestamp.
    /// Returns an error if the session doesn't exist.
    pub async fn find_by_id(
        &self,
        id: &str,
        db: &mut database::Reader,
    ) -> api::Result<Session> {
        let token = Session::derive_token(id);

        match session_repository::find(&token, db).await {
            Ok(session) => {
                // Make sure to provide a session with a fresh last access timestamp.
                let mut session = Session::from_entity(id.to_owned(), session);
                session.update_last_accessed();

                // Add a task for the last access update thread.
                // We use "try_send", because the thread might already have been notified
                // and the channel have a size of 1.
                self.last_accessed.write().await.insert(token, session.last_accessed);
                self.last_accessed_sender.try_send(()).ok();

                Ok(session)
            }
            Err(repository::Error::NotFound) => Err(api::Error::SessionNotFound),
            Err(e) => Err(e.into()),
        }
    }

    /// Create session for user.
    ///
    /// This invalidates older sessions, keeping only the maximum number of sessions allowed.
    /// Caller must ensure that the user has been fully authenticated.
    pub async fn create(
        &self,
        user: &User,
        user_agent: &UserAgent,
        db: &mut database::Writer,
    ) -> api::Result<Session> {
        // In the very unlikely possibility that the generated session id is already in use by
        // someone else, this will loop and create a new one. In reality, this should never happen,
        // but you never know.
        loop {
            let session = Session::new(user.id(), user_agent.clone());
            let session_entity = session.as_entity();

            match session_repository::insert(session_entity, db).await {
                Ok(_) => {
                    session_repository::delete_older(user.id(), self.max_sessions, db).await?;
                    return Ok(session);
                }
                Err(repository::Error::AlreadyExists) => continue,
                Err(e) => return Err(e.into()),
            }
        }
    }

    /// Invalidate session.
    ///
    /// Caller must ensure that the user has been fully authenticated.
    /// Does not return an error if the session doesn't exist.
    pub async fn invalidate(
        &self,
        session: Session,
        db: &mut database::Writer,
    ) -> api::Result<()> {
        match session_repository::delete(&session.token, db).await {
            Ok(()) | Err(repository::Error::NotFound) => Ok(()),
            Err(e) => Err(e.into()),
        }
    }
}