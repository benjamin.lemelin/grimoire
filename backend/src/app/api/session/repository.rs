use database::extension::{primary_key_to_already_exists, SqliteQueryResultExt, SqliteResultExt};

use crate::{database, repository};

/// Session entity, as represented in database.
#[derive(Debug)]
pub(super) struct SessionEntity {
    pub token: String,
    pub user_id: i64,
    pub user_agent: String,
    pub last_accessed: time::OffsetDateTime,
}

/// Find session by token.
///
/// Returns an error if the session doesn't exist.
pub(super) async fn find<'c, C>(
    token: &str,
    db: &'c mut C,
) -> repository::Result<SessionEntity>
    where &'c mut C: database::Connection<'c> {
    let query = sqlx::query_as!(
        SessionEntity,
        "SELECT \
             token, \
             user_id,\
             user_agent,
             last_accessed as 'last_accessed:_' \
         FROM \
             session \
         WHERE \
             token = ?",
        token
    );
    let session = query
        .fetch_one(db)
        .await?;

    Ok(session)
}

/// Add a new session user.
///
/// Returns an error in case the session already exists.
pub(super) async fn insert(
    session: SessionEntity,
    db: &mut database::Writer,
) -> repository::Result<SessionEntity> {
    let query = sqlx::query!(
        "INSERT INTO session (\
             token, \
             user_id,\
             user_agent,\
             last_accessed \
         ) VALUES (\
             ?,?,?,?\
         )",
        session.token,
        session.user_id,
        session.user_agent,
        session.last_accessed
    );
    query
        .execute(db)
        .await
        .map_error_code(primary_key_to_already_exists)?;

    Ok(session)
}

/// Update session last accessed timestamp.
///
/// Returns an error if the session could not be found.
pub(super) async fn update_last_accessed(
    token: &str,
    last_accessed: time::OffsetDateTime,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "UPDATE session \
         SET \
             last_accessed = ? \
         WHERE \
             token = ?",
        last_accessed,
        token
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Delete session by token.
///
/// Returns an error if the session could not be found.
pub(super) async fn delete(
    token: &str,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "DELETE FROM session \
         WHERE token = ?",
        token
    );
    query
        .execute(db)
        .await?
        .map_empty(|| repository::Error::NotFound)?;

    Ok(())
}

/// Delete user's old sessions until only `max` sessions remains.
///
/// Does not check if the user exists.
pub(super) async fn delete_older(
    user_id: i64,
    max_sessions: i64,
    db: &mut database::Writer,
) -> repository::Result<()> {
    let query = sqlx::query!(
        "DELETE FROM session \
         WHERE token IN (SELECT token \
                         FROM session \
                         WHERE user_id = ? \
                         ORDER BY DATETIME(last_accessed) DESC \
                         LIMIT ?, 1)",
        user_id,
        max_sessions
    );
    query
        .execute(db)
        .await?;

    Ok(())
}