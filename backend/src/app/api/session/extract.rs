use std::convert::Infallible;

use axum::{async_trait, http::{header::USER_AGENT, request::Parts as RequestParts}};
use axum::extract::FromRequestParts;
use axum::response::{IntoResponse, IntoResponseParts, Response, ResponseParts};
use axum_extra::extract::{cookie::{Cookie, SameSite}, CookieJar};
use lazy_static::lazy_static;
use regex::Regex;

use crate::{
    database,
    app::api::{
        self,
        session::{
            store::SessionStore,
            AdminSession,
            AnySession,
            Browser,
            ExpiredSession,
            OpenedSession,
            Platform,
            Session,
            UserAgent,
            UserSession,
        },
        user::{
            User,
            Role,
            service as user_service,
        },
    },
};

/// Session cookie name.
pub const SESSION_COOKIE_NAME: &str = "grimoire_session";

#[async_trait]
impl FromRequestParts<api::State> for Session {
    type Rejection = api::Error;

    async fn from_request_parts(parts: &mut RequestParts, ctx: &api::State) -> Result<Self, Self::Rejection> {
        let mut db = ctx.database.read().await?;
        let sessions = &ctx.sessions;

        session_from_request_parts(parts, ctx, &mut db, sessions).await
    }
}

#[async_trait]
impl FromRequestParts<api::State> for AnySession {
    type Rejection = api::Error;

    async fn from_request_parts(parts: &mut RequestParts, ctx: &api::State) -> Result<Self, Self::Rejection> {
        let mut db = ctx.database.read().await?;
        let sessions = &ctx.sessions;

        let (session, user) = user_from_request_parts(parts, ctx, &mut db, sessions).await?;
        Ok(Self(session, user))
    }
}

#[async_trait]
impl FromRequestParts<api::State> for UserSession {
    type Rejection = api::Error;

    async fn from_request_parts(parts: &mut RequestParts, ctx: &api::State) -> Result<Self, Self::Rejection> {
        let mut db = ctx.database.read().await?;
        let sessions = &ctx.sessions;

        let (session, user) = user_with_role_from_request_parts(parts, ctx, &mut db, sessions, Role::User).await?;
        Ok(Self(session, user))
    }
}

#[async_trait]
impl FromRequestParts<api::State> for AdminSession {
    type Rejection = api::Error;

    async fn from_request_parts(parts: &mut RequestParts, ctx: &api::State) -> Result<Self, Self::Rejection> {
        let mut db = ctx.database.read().await?;
        let sessions = &ctx.sessions;

        let (session, user) = user_with_role_from_request_parts(parts, ctx, &mut db, sessions, Role::Admin).await?;
        Ok(Self(session, user))
    }
}

async fn session_from_request_parts(
    parts: &mut RequestParts,
    ctx: &api::State,
    db: &mut database::Reader,
    sessions: &SessionStore,
) -> Result<Session, api::Error> {
    let cookies = CookieJar::from_request_parts(parts, ctx).await.expect("extracting cookies should be infallible");

    match cookies.get(SESSION_COOKIE_NAME).map(Cookie::value) {
        Some(id) => Ok(sessions.find_by_id(id, &mut *db).await?),
        None => Err(api::Error::CookieNotProvided),
    }
}

async fn user_from_request_parts(
    parts: &mut RequestParts,
    ctx: &api::State,
    db: &mut database::Reader,
    sessions: &SessionStore,
) -> Result<(Session, User), api::Error> {
    let session = session_from_request_parts(parts, ctx, &mut *db, sessions).await?;
    let user = user_service::find_by_id(session.user_id, &mut *db).await?;

    Ok((session, user))
}

async fn user_with_role_from_request_parts(
    parts: &mut RequestParts,
    ctx: &api::State,
    db: &mut database::Reader,
    sessions: &SessionStore,
    role: Role,
) -> Result<(Session, User), api::Error> {
    let (session, user) = user_from_request_parts(parts, ctx, &mut *db, sessions).await?;

    match user.has_role(role) {
        true => Ok((session, user)),
        false => Err(api::Error::Forbidden)
    }
}

impl IntoResponse for OpenedSession {
    fn into_response(self) -> Response {
        (self, ()).into_response()
    }
}

impl IntoResponseParts for OpenedSession {
    type Error = Infallible;

    fn into_response_parts(self, res: ResponseParts) -> Result<ResponseParts, Self::Error> {
        CookieJar::new().add(
            Cookie::build(SESSION_COOKIE_NAME, self.0.id)
                .path("/")
                .same_site(SameSite::Strict)
                .http_only(true)
                .secure(self.1)
                .finish()
        ).into_response_parts(res)
    }
}

impl IntoResponse for ExpiredSession {
    fn into_response(self) -> Response {
        (self, ()).into_response()
    }
}

impl IntoResponseParts for ExpiredSession {
    type Error = Infallible;

    fn into_response_parts(self, res: ResponseParts) -> Result<ResponseParts, Self::Error> {
        CookieJar::new().add(
            Cookie::build(SESSION_COOKIE_NAME, "")
                .path("/")
                .same_site(SameSite::Strict)
                .max_age(time::Duration::seconds(0))
                .finish()
        ).into_response_parts(res)
    }
}

#[async_trait]
impl FromRequestParts<api::State> for UserAgent {
    type Rejection = Infallible;

    async fn from_request_parts(parts: &mut RequestParts, _ctx: &api::State) -> Result<Self, Self::Rejection> {
        // We do not need advanced parsing of the user agents. A basic regex will do.

        lazy_static! {
            static ref USER_AGENT_REGEX: Regex = Regex::new(r"Mozilla\/5.0 \(.*(Windows|Macintosh|Linux|Android|iPhone|iPad|iPod).*\).*(Firefox|Chrome|Edg|Safari)").expect("user agent regex should be valid");
        }

        match parts.headers
            .get(USER_AGENT)
            .and_then(|it| it.to_str().ok())
            .and_then(|it| USER_AGENT_REGEX.captures(it)) {
            Some(captures) => {
                match (captures.get(1), captures.get(2)) {
                    (Some(platform), Some(browser)) => {
                        let platform = match platform.as_str() {
                            "Windows" => Platform::Windows,
                            "Macintosh" => Platform::Mac,
                            "Linux" => Platform::Linux,
                            "Android" => Platform::Android,
                            "iPhone" | "iPad" | "iPod" => Platform::Ios,
                            _ => Platform::Unknown
                        };

                        let browser = match browser.as_str() {
                            "Firefox" => Browser::Firefox,
                            "Chrome" => Browser::Chrome,
                            "Edg" => Browser::Edge,
                            "Safari" => Browser::Safari,
                            _ => Browser::Unknown
                        };
                        Ok(UserAgent::new(platform, browser))
                    }
                    _ => Ok(UserAgent::default())
                }
            }
            None => Ok(UserAgent::default()),
        }
    }
}