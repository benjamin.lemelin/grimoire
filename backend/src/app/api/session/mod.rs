use serde::{Deserialize, Serialize};
use tracing::error;

use repository::SessionEntity;

use crate::app::api::user::User;
use crate::util;

pub mod extract;
pub mod repository;
pub mod store;

/// Session.
///
/// When used as an extractor, this will ensure that the user is authenticated and has at least the
/// guest role. Not usable as a response.
#[derive(Debug)]
pub struct Session {
    id: SessionId,
    token: SessionToken,
    user_id: i64,
    user_agent: UserAgent,
    last_accessed: Timestamp,
}

/// Any session.
///
/// This is useful to fetch the user informations at the same time as the session. Can be used as an
/// extractor. Not usable as a response.
#[derive(Debug)]
pub struct AnySession(pub Session, pub User);

/// User session.
///
/// When used as an extractor, this will ensure that the authenticated user has at least the user
/// role. Not usable as a response.
#[derive(Debug)]
pub struct UserSession(pub Session, pub User);

/// Admin session.
///
/// When used as an extractor, this will ensure that the authenticated user has at least the admin
/// role. Not usable as a response.
#[derive(Debug)]
pub struct AdminSession(pub Session, pub User);

/// Newly opened session.
///
/// When used as a response, this will send a new session cookie. The second parameter determines
/// if the sent cookie is secure or not. Not usable as an extractor.
#[derive(Debug)]
pub struct OpenedSession(pub Session, pub bool);

/// Expired Session.
///
/// When used as a response, this will destroy the session cookie. Not usable as an extractor.
#[derive(Debug)]
pub struct ExpiredSession;

/// Session id (cookie content).
pub type SessionId = String;

/// Session token (as stored in database).
pub type SessionToken = String;

/// Timestamp.
pub type Timestamp = time::OffsetDateTime;

/// User agent.
///
/// Used for disambiguation purposes when the user want to know which session is which.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct UserAgent {
    platform: Platform,
    browser: Browser,
}

/// Platform (family of operating systems).
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub enum Platform {
    Windows,
    Mac,
    Linux,
    Android,
    Ios,
    #[default]
    Unknown,
}

/// Web browser.
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub enum Browser {
    Firefox,
    Chrome,
    Edge,
    Safari,
    #[default]
    Unknown,
}

impl Session {
    /// Create new session for user.
    fn new(user_id: i64, user_agent: UserAgent) -> Self {
        let id = Self::create_id();
        let token = Self::derive_token(&id);
        let last_accessed = Self::create_timestamp();

        Self {
            id,
            token,
            user_id,
            user_agent,
            last_accessed,
        }
    }

    /// Generate random session id using a cryptographically secure random.
    fn create_id() -> String {
        use rand::RngCore;

        let mut cookie = [0u8; 128];
        rand::thread_rng().fill_bytes(&mut cookie);
        util::base64_encode(cookie)
    }

    /// Derive token from session id using sha3.
    fn derive_token(id: &str) -> String {
        util::base64_sha3_hash(id)
    }

    /// Create Unix timestamp from system time.
    fn create_timestamp() -> Timestamp {
        time::OffsetDateTime::now_utc()
    }

    /// Create from entity.
    fn from_entity(id: String, session: SessionEntity) -> Self {
        Self {
            id,
            token: session.token,
            user_id: session.user_id,
            user_agent: serde_json::from_str(&session.user_agent).unwrap_or_else(|e| {
                error!("Failed to deserialize User Agent from session in database : {e}.");
                UserAgent::default()
            }),
            last_accessed: session.last_accessed,
        }
    }

    /// Convert to database entity.
    fn as_entity(&self) -> SessionEntity {
        SessionEntity {
            token: self.token.clone(),
            user_id: self.user_id,
            user_agent: serde_json::to_string(&self.user_agent).unwrap_or_else(|e| {
                error!("Failed to serialize User Agent from session for database : {e}.");
                String::new()
            }),
            last_accessed: self.last_accessed,
        }
    }

    /// Update last access timestamp to current system time.
    pub fn update_last_accessed(&mut self) {
        self.last_accessed = Self::create_timestamp()
    }
}

impl UserAgent {
    /// Create new user agent.
    fn new(platform: Platform, browser: Browser) -> Self {
        Self {
            platform,
            browser,
        }
    }
}