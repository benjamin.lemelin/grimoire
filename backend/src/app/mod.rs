use std::net::SocketAddr;

use axum::Router;
use tracing::error;

use crate::app::extension::UriExt;
use crate::config::Config;

mod api;
mod frontend;
mod extension;

/// Start Grimoire server.
pub async fn run(config: Config) -> anyhow::Result<()> {
    if config.https() {
        serve_https(&config).await
    } else {
        serve_http(&config).await
    }
}

/// Start server in https mode, redirecting any http trafic to the https port.
async fn serve_https(config: &Config) -> anyhow::Result<()> {
    use axum_server::tls_rustls::RustlsConfig;
    use axum::{extract::Host, handler::HandlerWithoutStateExt, http::{StatusCode, Uri}, response::Redirect};

    // TLS config.
    let tls_config = RustlsConfig::from_pem_file(
        config.cert_path().expect("cert path should exist when configured for https"),
        config.key_path().expect("key path should exist when configured for https"),
    ).await?;

    // Https redirect handler.
    let http_port = config.port();
    let https_port = config.secure_port();
    let https_redirect = move |Host(host): Host, uri: Uri| async move {
        match uri.into_https(host, http_port, https_port) {
            Ok(uri) => Ok(Redirect::permanent(&uri.to_string())),
            Err(e) => {
                error!("failed to redirect http request to https : {e}");
                Err(StatusCode::INTERNAL_SERVER_ERROR)
            }
        }
    };

    // Http trafic (redirects to https).
    let http_trafic = axum::Server::bind(&SocketAddr::from(([0, 0, 0, 0], config.port())))
        .serve(https_redirect.into_make_service());

    // Https trafic.
    let https_trafic = axum_server::bind_rustls(SocketAddr::from(([0, 0, 0, 0], config.secure_port())), tls_config)
        .serve(routes(config).await?.into_make_service());

    // Run servers.
    let http_trafic = tokio::spawn(http_trafic);
    let https_trafic = tokio::spawn(https_trafic);
    http_trafic.await??;
    https_trafic.await??;
    Ok(())
}

/// Start the server in http mode.
async fn serve_http(config: &Config) -> anyhow::Result<()> {
    axum::Server::bind(&SocketAddr::from(([0, 0, 0, 0], config.port())))
        .serve(routes(config).await?.into_make_service())
        .await?;

    Ok(())
}

/// Server routes.
async fn routes(config: &Config) -> anyhow::Result<Router> {
    use axum::{extract::MatchedPath, http::Request};
    use tower_http::trace::TraceLayer;
    use tracing::info_span;

    // Layers.
    let trace_layer = TraceLayer::new_for_http().make_span_with(|request: &Request<_>| {
        let matched_path = request
            .extensions()
            .get::<MatchedPath>()
            .map(MatchedPath::as_str);

        info_span!(
            "api",
            uri = matched_path,
            method = ?request.method(),
        )
    });

    Ok(Router::new()
        .merge(frontend::routes())
        .nest("/api", api::routes(config).await?)
        .layer(trace_layer)
    )
}