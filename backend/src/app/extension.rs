use axum::{BoxError, http::{Uri, uri::Scheme}};

/// Extensions for `URI`s.
pub trait UriExt {
    /// Transform an URI from http to https.
    fn into_https(self, host: String, http_port: u16, https_port: u16) -> Result<Uri, BoxError>;
}

impl UriExt for Uri {
    fn into_https(self, host: String, http_port: u16, https_port: u16) -> Result<Uri, BoxError> {
        let mut parts = self.into_parts();

        parts.scheme = Some(Scheme::HTTPS);
        parts.authority = Some(host.replace(&format!(":{http_port}"), &format!(":{https_port}")).parse()?);

        Ok(Uri::from_parts(parts)?)
    }
}