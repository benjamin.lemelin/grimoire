use axum::Router;
use tower_http::services::{ServeDir, ServeFile};

/// Frontend routes.
pub fn routes() -> Router {
    Router::new()
        .fallback_service(ServeDir::new("static").not_found_service(ServeFile::new("static/index.html")))
}