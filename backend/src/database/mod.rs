use std::io;
use std::path::Path;
use std::sync::Arc;

use futures::{future::BoxFuture, stream::BoxStream};
use sqlx::{ConnectOptions, database::HasStatement, Describe, Either, Execute, sqlite::SqliteConnectOptions};
use tokio::{fs, sync::{OwnedRwLockReadGuard, OwnedRwLockWriteGuard, RwLock}};

pub mod extension;

/// Application database.
///
/// Stores application data, excluding large files like images. For such files,
/// use a [Bucket].
///
/// [Bucket]: crate::storage::Bucket
#[derive(Debug, Clone)]
pub struct Database {
    connection_pool: Arc<RwLock<sqlx::SqlitePool>>,
}

/// Database reader.
///
/// SQLite should allow multiple readers and writers, but tests have shown this to not be the case.
/// This ensures only one user at a time can read/write to the database.
#[derive(Debug)]
pub struct Reader {
    _lock: OwnedRwLockReadGuard<sqlx::SqlitePool>,
    reader: sqlx::pool::PoolConnection<sqlx::Sqlite>,
}

/// Database writer.
///
/// SQLite should allow multiple readers and writers, but tests have shown this to not be the case.
/// This ensures only one user at a time can read/write to the database.
#[derive(Debug)]
pub struct Writer {
    _lock: OwnedRwLockWriteGuard<sqlx::SqlitePool>,
    writer: sqlx::Transaction<'static, sqlx::Sqlite>,
}

/// Database connection.
///
/// Useful when abstracting over a reader or a writer connection.
pub trait Connection<'c>: sqlx::Executor<'c, Database=sqlx::Sqlite> {}

impl Database {
    /// Open database from file.
    ///
    /// This creates the database if it doesn't exists (including parent directories).
    /// Any required migration is also applied.
    pub async fn open<P: AsRef<Path>>(path: P) -> sqlx::Result<Self> {
        let path = path.as_ref();

        // Create required parent directories.
        fs::create_dir_all(path.parent().ok_or_else(|| io::Error::new(
            io::ErrorKind::Other,
            "database path is not inside a directory",
        ))?).await?;

        // Create connection to Sqlite database.
        let connection_pool = sqlx::SqlitePool::connect_with(
            SqliteConnectOptions::new().filename(path).create_if_missing(true).disable_statement_logging()
        ).await?;

        // Run required migrations.
        sqlx::migrate!().run(&connection_pool).await?;

        Ok(Self {
            connection_pool: Arc::new(RwLock::new(connection_pool))
        })
    }

    /// Provide a read-only access to the database.
    pub async fn read(&self) -> sqlx::Result<Reader> {
        let lock = self.connection_pool.clone().read_owned().await;
        let reader = lock.acquire().await?;

        Ok(Reader {
            _lock: lock,
            reader,
        })
    }

    /// Provide a read-write access to the database.
    ///
    /// This immediately start a transaction.
    pub async fn write(&self) -> sqlx::Result<Writer> {
        let lock = self.connection_pool.clone().write_owned().await;
        let writer = lock.begin().await?;

        Ok(Writer {
            _lock: lock,
            writer,
        })
    }
}

impl<'c> Connection<'c> for &'c mut Reader {}

impl<'c> sqlx::Executor<'c> for &'c mut Reader {
    type Database = sqlx::Sqlite;

    fn fetch_many<'e, 'q: 'e, E: 'q>(
        self,
        query: E,
    ) -> BoxStream<'e, Result<Either<<Self::Database as sqlx::Database>::QueryResult, <Self::Database as sqlx::Database>::Row>, sqlx::Error>>
        where 'c: 'e, E: Execute<'q, Self::Database> {
        self.reader.fetch_many(query)
    }

    fn fetch_optional<'e, 'q: 'e, E: 'q>(
        self,
        query: E,
    ) -> BoxFuture<'e, Result<Option<<Self::Database as sqlx::Database>::Row>, sqlx::Error>>
        where 'c: 'e, E: Execute<'q, Self::Database> {
        self.reader.fetch_optional(query)
    }

    fn prepare_with<'e, 'q: 'e>(
        self,
        sql: &'q str,
        parameters: &'e [<Self::Database as sqlx::Database>::TypeInfo],
    ) -> BoxFuture<'e, Result<<Self::Database as HasStatement<'q>>::Statement, sqlx::Error>>
        where 'c: 'e {
        self.reader.prepare_with(sql, parameters)
    }

    fn describe<'e, 'q: 'e>(
        self,
        sql: &'q str,
    ) -> BoxFuture<'e, Result<Describe<Self::Database>, sqlx::Error>>
        where 'c: 'e {
        self.reader.describe(sql)
    }
}

impl Writer {
    pub async fn commit(self) -> sqlx::Result<()> {
        self.writer.commit().await
    }
}

impl<'c> Connection<'c> for &'c mut Writer {}

impl<'c> sqlx::Executor<'c> for &'c mut Writer {
    type Database = sqlx::Sqlite;

    fn fetch_many<'e, 'q: 'e, E: 'q>(
        self,
        query: E,
    ) -> BoxStream<'e, Result<Either<<Self::Database as sqlx::Database>::QueryResult, <Self::Database as sqlx::Database>::Row>, sqlx::Error>>
        where 'c: 'e, E: Execute<'q, Self::Database> {
        self.writer.fetch_many(query)
    }

    fn fetch_optional<'e, 'q: 'e, E: 'q>(
        self,
        query: E,
    ) -> BoxFuture<'e, Result<Option<<Self::Database as sqlx::Database>::Row>, sqlx::Error>>
        where 'c: 'e, E: Execute<'q, Self::Database> {
        self.writer.fetch_optional(query)
    }

    fn prepare_with<'e, 'q: 'e>(
        self,
        sql: &'q str,
        parameters: &'e [<Self::Database as sqlx::Database>::TypeInfo],
    ) -> BoxFuture<'e, Result<<Self::Database as HasStatement<'q>>::Statement, sqlx::Error>>
        where 'c: 'e {
        self.writer.prepare_with(sql, parameters)
    }

    fn describe<'e, 'q: 'e>(
        self,
        sql: &'q str,
    ) -> BoxFuture<'e, Result<Describe<Self::Database>, sqlx::Error>>
        where 'c: 'e {
        self.writer.describe(sql)
    }
}