use sqlx::error::DatabaseError;
use sqlx::sqlite::{SqliteError, SqliteQueryResult};

use crate::repository;

/// Sqlite error codes.
///
/// This is not meant to be exhaustive.
/// See https://www.sqlite.org/rescode.html#primary_result_code_list for all error codes.
#[derive(Debug, Eq, PartialEq)]
pub enum SqliteErrorCode {
    /// Indicates that a primary key constraint failed. Code : 1555.
    PrimaryKey,
    /// Indicates that a foreign key constraint failed. Code : 787.
    ForeignKey,
    /// Indicates that a unique constraint failed. Code : 2067.
    Unique,
}

/// Extensions for the `SqliteQueryResultExt` type.
pub trait SqliteQueryResultExt {
    /// Transforms rows affected by the query into a `Result<u64,E>`, mapping `0` to
    /// `Err(e)` and everything else to `Ok(rows)`.
    fn map_empty<E, F: FnOnce() -> E>(self, op: F) -> Result<u64, E>;
}

impl SqliteQueryResultExt for SqliteQueryResult {
    fn map_empty<E, F: FnOnce() -> E>(self, op: F) -> Result<u64, E> {
        match self.rows_affected() {
            0 => Err(op()),
            rows => Ok(rows),
        }
    }
}

/// Extensions for the `sqlx::Result` when using a Sqlite database.
pub trait SqliteResultExt<T> {
    /// Maps error code to an other error type by applying a function to a contained
    /// `SqliteErrorCode`. The others errors are converted using `From`.
    ///
    /// The provided function may return `None` in case the received error code should be
    /// mapped using `From`.
    fn map_error_code<E, F>(self, op: F) -> Result<T, E>
        where Self: Sized,
              E: From<sqlx::Error>,
              F: FnOnce(SqliteErrorCode) -> Option<E>;
}

impl<T> SqliteResultExt<T> for sqlx::Result<T> {
    fn map_error_code<E, F>(self, op: F) -> Result<T, E>
        where E: From<sqlx::Error>,
              F: FnOnce(SqliteErrorCode) -> Option<E> {
        match self {
            Ok(t) => Ok(t),
            Err(sqlx::Error::Database(e)) => {
                match match e.downcast_ref::<SqliteError>().code() {
                    Some(code) if code == "1555" => op(SqliteErrorCode::PrimaryKey),
                    Some(code) if code == "787" => op(SqliteErrorCode::ForeignKey),
                    Some(code) if code == "2067" => op(SqliteErrorCode::Unique),
                    _ => None,
                } {
                    Some(e) => Err(e),
                    None => Err(sqlx::Error::Database(e).into())
                }
            }
            Err(e) => Err(e.into())
        }
    }
}

/// Maps primary key constraint error to the `AlreadyExists` repository error.
pub fn primary_key_to_already_exists(code: SqliteErrorCode) -> Option<repository::Error> {
    match code {
        SqliteErrorCode::PrimaryKey => Some(repository::Error::AlreadyExists),
        _ => None
    }
}

/// Maps foreign key constraint error to the `NotFound` repository error.
pub fn foreign_key_to_not_found(code: SqliteErrorCode) -> Option<repository::Error> {
    match code {
        SqliteErrorCode::ForeignKey => Some(repository::Error::NotFound),
        _ => None
    }
}

/// Maps unique constraint error to the `AlreadyExists` repository error.
pub fn unique_to_already_exists(code: SqliteErrorCode) -> Option<repository::Error> {
    match code {
        SqliteErrorCode::Unique => Some(repository::Error::AlreadyExists),
        _ => None
    }
}
