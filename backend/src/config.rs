use std::path::{Path, PathBuf};

use clap::Parser;

/// The Grimoire is a web application to store your recipes.
#[derive(Debug, Clone, Parser)]
#[command(author, version, about)]
pub struct Config {
    /// Application port (for http).
    ///
    /// You may want to change this port if the default one is already used.
    #[arg(long, default_value = "8080")]
    port: u16,
    /// Application port (for https).
    ///
    /// You may want to change this port if the default one is already used. Please note setting
    /// this options does not enables https. See the --cert and --key parameters.
    #[arg(long, default_value = "8443")]
    secure_port: u16,
    /// Path to the SSL certificate for https.
    ///
    /// Also requires the --key parameter to be set.
    #[arg(long, requires = "key")]
    cert: Option<PathBuf>,
    /// Path to the SSL certificate private key for https.
    ///
    /// Must be an unencrypted private key. Also requires the --cert parameter to be set.
    #[arg(long, requires = "cert")]
    key: Option<PathBuf>,
    /// Force secure cookies for session cookies, even when not using https.
    ///
    /// Use this if you plan to run the application behind a reverse proxy that provides https.
    #[arg(long)]
    secure_cookies: bool,
    /// Sets the maximum number of sessions a user is allowed to have.
    ///
    /// Must be set between 1 and 255.
    #[arg(long, value_parser = 1..255, default_value = "3")]
    max_sessions: i64,
    /// Path to the application data.
    ///
    /// The application will assume total control over this folder.
    #[arg(long, default_value = ".grimoire")]
    data_dir: PathBuf,
}

impl Config {
    /// Fetch and parse configuration.
    ///
    /// If configuration is incomplete or cannot be fetched, this shutdown the programs
    /// and prints an help message.
    pub fn parse() -> Self {
        Parser::parse()
    }

    /// Application port (for http).
    pub fn port(&self) -> u16 {
        self.port
    }

    /// Application port (for https).
    pub fn secure_port(&self) -> u16 {
        self.secure_port
    }

    /// Returns whether https is configured.
    pub fn https(&self) -> bool {
        self.cert.is_some()
    }

    /// SSL certificate file path, if any.
    pub fn cert_path(&self) -> Option<&Path> {
        self.cert.as_deref()
    }

    /// SSL certificate private key file path, if any.
    pub fn key_path(&self) -> Option<&Path> {
        self.key.as_deref()
    }

    /// Returns whether sensitive cookies should be flagged as secure.
    pub fn secure_cookies(&self) -> bool {
        self.secure_cookies
    }

    /// Maximum number of sessions a user is allowed to have.
    pub fn max_sessions(&self) -> i64 {
        self.max_sessions
    }

    /// Path to the database file.
    pub fn database_path(&self) -> PathBuf {
        self.data_dir.join("db").join("app.db")
    }

    /// Path to the index directory.
    pub fn index_path(&self) -> PathBuf {
        self.data_dir.join("index")
    }

    /// Path to the images bucket directory.
    pub fn images_path(&self) -> PathBuf {
        self.data_dir.join("images")
    }
}