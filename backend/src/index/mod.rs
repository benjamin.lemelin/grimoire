use std::fmt::{self, Debug};
use std::path::Path;
use std::sync::Arc;

use tantivy::{Index as TantivyIndex, IndexReader as TantivyReader, IndexWriter as TantivyWriter};
use tantivy::schema::{Field as TantivyField, Term as TantivyTerm};
use tantivy::tokenizer::TextAnalyzer as TantivyTokenizer;
use tokio::fs;
use tokio::sync::{OwnedRwLockWriteGuard, RwLock};
use tracing::{error, warn};

/// Application index.
///
/// Stores a searchable index. This index cannot be used to retrieve data : it returns ids that
/// can be used to lookup real data on the [Database].
///
/// [Database]: crate::database::Database
#[derive(Clone)]
pub struct Index {
    schema: Schema,
    index: TantivyIndex,
    tokenizer: TantivyTokenizer,
    reader: TantivyReader,
    writer: Arc<RwLock<TantivyWriter>>,
}

/// Schema of the index. Mainly used to retrieve fields.
#[derive(Debug, Clone)]
struct Schema {
    id: TantivyField,
    text: TantivyField,
}

/// Index reader.
///
/// Provides a read-only access to the index.
pub struct Reader {
    schema: Schema,
    reader: TantivyReader,
    tokenizer: TantivyTokenizer,
}

/// Index writer.
///
/// Provides a write-only access to the index. This locks the index in the process.
/// Users are required to call the [commit] method to write the changes.
///
/// [commit]: Writer::commit
pub struct Writer {
    schema: Schema,
    writer: OwnedRwLockWriteGuard<TantivyWriter>,
}

impl Index {
    /// Open index from directory.
    ///
    /// This creates the index if it doesn't exists (including any missing directory).
    pub async fn open<P: AsRef<Path>>(path: P) -> tantivy::Result<Self> {
        let path = path.as_ref();

        // Create required parent directories.
        fs::create_dir_all(&path).await?;

        // Create index schema.
        let (schema, tantivy_schema) = {
            use tantivy::schema::{
                IndexRecordOption,
                NumericOptions,
                Schema as TantivySchema,
                TextFieldIndexing,
                TextOptions,
            };

            let numeric_options = NumericOptions::default().set_stored().set_indexed();
            let text_options = TextOptions::default().set_indexing_options(
                TextFieldIndexing::default()
                    .set_tokenizer(TOKENIZER_NAME)
                    .set_index_option(IndexRecordOption::WithFreqsAndPositions)
            );

            let mut builder = TantivySchema::builder();
            let id = builder.add_i64_field(ID_FIELD_NAME, numeric_options);
            let text = builder.add_text_field(TEXT_FIELD_NAME, text_options);
            (Schema::new(id, text), builder.build())
        };

        // Create index tokenizer.
        let tokenizer = {
            use tantivy::tokenizer::{AsciiFoldingFilter, LowerCaser, SimpleTokenizer, StopWordFilter, Language};

            TantivyTokenizer::builder(SimpleTokenizer::default())
                .filter(LowerCaser)
                .filter(AsciiFoldingFilter)
                .filter(StopWordFilter::new(Language::English).expect("english stop words should exist"))
                .filter(StopWordFilter::new(Language::French).expect("french stop words should exist"))
                .build()
        };

        // Create index.
        let index = {
            use tantivy::directory::MmapDirectory;

            let index = TantivyIndex::open_or_create(MmapDirectory::open(path)?, tantivy_schema)?;
            index.tokenizers().register(TOKENIZER_NAME, tokenizer.clone());
            index
        };

        // Create reader and writer.
        let reader = index.reader_builder().reload_policy(tantivy::ReloadPolicy::OnCommit).try_into()?;
        let writer = index.writer(WRITER_CACHE_SIZE)?;


        Ok(Self {
            schema,
            index,
            tokenizer,
            reader,
            writer: Arc::new(RwLock::new(writer)),
        })
    }

    /// Provide a read-only access to the index.
    pub async fn read(&self) -> tantivy::Result<Reader> {
        Reader::new(self).await
    }

    /// Provide a write-only access to the index.
    pub async fn write(&self) -> tantivy::Result<Writer> {
        Writer::new(self).await
    }
}

impl Reader {
    /// Create a new index reader.
    async fn new(index: &Index) -> tantivy::Result<Self> {
        Ok(Self {
            schema: index.schema.clone(),
            reader: index.reader.clone(),
            tokenizer: index.tokenizer.clone(),
        })
    }

    /// Find documents inside the index that correspond to provided sequence of words.
    pub async fn find(&self, keywords: &str, max_results: usize) -> tantivy::Result<Vec<i64>> {
        use tantivy::query::{BooleanQuery, FuzzyTermQuery, Occur, Query};
        use tantivy::tokenizer::Token;
        use tantivy::collector::TopDocs;
        use tantivy::schema::Value;

        // Number of letters of leniency (typos). See Fuzzy query bellow.
        const MAX_DISTANCE: u8 = 1;

        let schema = self.schema.clone();
        let reader = self.reader.clone();
        let mut tokenizer = self.tokenizer.clone();
        let keywords = String::from(keywords);

        tokio::task::spawn_blocking(move || -> tantivy::Result<Vec<i64>> {
            let query = {
                let mut queries: Vec<(Occur, Box<dyn Query>)> = Vec::new();
                let mut tokens = tokenizer.token_stream(&keywords);
                while let Some(Token { text, .. }) = tokens.next() {
                    queries.push((
                        Occur::Should,
                        Box::new(FuzzyTermQuery::new(TantivyTerm::from_field_text(schema.text, text), MAX_DISTANCE, true))
                    ));
                }
                BooleanQuery::new(queries)
            };

            let searcher = reader.searcher();
            let mut ids = Vec::with_capacity(max_results);
            for (_, address) in searcher.search(&query, &TopDocs::with_limit(max_results))? {
                match searcher.doc(address) {
                    Ok(document) => {
                        match document.get_first(schema.id) {
                            Some(Value::I64(id)) => ids.push(*id),
                            _ => warn!("missing id field in index document"),
                        }
                    }
                    Err(e) => {
                        return Err(e);
                    }
                }
            }
            Ok(ids)
        }).await.unwrap()
    }
}

impl Writer {
    /// Create a new index writer.
    async fn new(index: &Index) -> tantivy::Result<Self> {
        Ok(Self {
            schema: index.schema.clone(),
            writer: index.writer.clone().write_owned().await,
        })
    }

    /// Add document into the index.
    pub fn insert(&self, id: i64, text: &str) -> tantivy::Result<()> {
        self.writer.add_document(tantivy::doc!(
            self.schema.id => id,
            self.schema.text => text.to_string()
        ))?;
        Ok(())
    }

    /// Update document inside the index.
    ///
    /// This does not check if the document exists.
    pub fn update(&self, id: i64, text: &str) -> tantivy::Result<()> {
        self.delete(id)?;
        self.insert(id, text)?;
        Ok(())
    }

    /// Delete document inside the index.
    ///
    /// This does not check if the document exists.
    pub fn delete(&self, id: i64) -> tantivy::Result<()> {
        self.writer.delete_term(TantivyTerm::from_field_i64(
            self.schema.id, id,
        ));
        Ok(())
    }

    /// Delete every document inside the index.
    ///
    /// Use very carefully.
    pub fn clear(&self) -> tantivy::Result<()> {
        self.writer.delete_all_documents()?;
        Ok(())
    }

    /// Commit all pending changes to the index.
    pub async fn commit(mut self) -> tantivy::Result<()> {
        tokio::task::spawn_blocking(move || {
            self.writer.commit()
        }).await.unwrap()?;
        Ok(())
    }
}

impl Drop for Writer {
    fn drop(&mut self) {
        if let Err(e) = self.writer.rollback() {
            error!("failed to rollback index : {e}");
        }
    }
}

impl Schema {
    /// Create a new index schema.
    fn new(id: TantivyField, text: TantivyField) -> Self {
        Self {
            id,
            text,
        }
    }
}

impl Debug for Index {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Index")
            .field("index", &self.index)
            .field("reader", &"<IndexReader>")
            .field("writer", &"<IndexWriter>")
            .finish()
    }
}

impl Debug for Reader {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Reader")
            .field("schema", &self.schema)
            .field("reader", &"<IndexReader>")
            .finish()
    }
}

impl Debug for Writer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Writer")
            .field("schema", &self.schema)
            .field("writer", &"<IndexWriter>")
            .finish()
    }
}

const ID_FIELD_NAME: &str = "id";
const TEXT_FIELD_NAME: &str = "text";
const TOKENIZER_NAME: &str = "tokenizer";
const WRITER_CACHE_SIZE: usize = 50_000_000; // 50MB.