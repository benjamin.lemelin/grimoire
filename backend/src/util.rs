/// Encode byte array into base64 string.
pub fn base64_encode<T : AsRef<[u8]>>(value: T) -> String {
    use base64::{Engine, engine::general_purpose::STANDARD_NO_PAD};

    STANDARD_NO_PAD.encode(value)
}

/// Hash byte array using the sha3 algorithm. Then, return the result as a base64 encoded string.
pub fn base64_sha3_hash<T : AsRef<[u8]>>(value: T) -> String {
    use sha3::{Digest, Sha3_512};

    let mut hasher = Sha3_512::new();
    hasher.update(value);
    base64_encode(&*hasher.finalize())
}