use std::io;

/// Repository result.
pub type Result<T> = std::result::Result<T, Error>;

/// Repository errors.
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Resource not found.
    #[error("resource not found")]
    NotFound,
    /// Resource already exists.
    #[error("resource already exists")]
    AlreadyExists,
    /// Error while using the database.
    #[error("error using the database : {0}")]
    Database(#[source] sqlx::Error),
    /// Error while using the index.
    #[error("error using the index : {0}")]
    Index(#[from] tantivy::TantivyError),
    /// Error while using the storage.
    #[error("error using the storage : {0}")]
    Storage(#[source] io::Error),
}

impl From<sqlx::Error> for Error {
    fn from(err: sqlx::Error) -> Self {
        match err {
            sqlx::Error::RowNotFound => Self::NotFound,
            _ => Self::Database(err)
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        match err.kind() {
            io::ErrorKind::NotFound => Self::NotFound,
            _ => Self::Storage(err)
        }
    }
}