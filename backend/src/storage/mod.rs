use std::io;
use std::path::{Path, PathBuf};

use tokio::fs;

/// Application file storage.
///
/// A bucket acts like a directory, but does not contains any sub-directories and usually
/// only hold files of the same kind.
///
/// There is no transactions. Use carefully.
#[derive(Debug, Clone)]
pub struct Bucket {
    path: PathBuf,
}

impl Bucket {
    /// Open a bucket from a directory.
    ///
    /// This creates any missing directory.
    pub async fn open<P: AsRef<Path>>(path: P) -> io::Result<Self> {
        let path = path.as_ref();

        // Create required parent directories.
        fs::create_dir_all(&path).await?;

        Ok(Self {
            path: path.into()
        })
    }

    /// Read file inside the bucket.
    ///
    /// Returns an error if the file doesn't exists.
    pub async fn read(&self, name: &str) -> io::Result<Vec<u8>> {
        fs::read(self.path.join(name)).await
    }

    /// Write file inside the bucket.
    ///
    /// File is created/overwritten.
    pub async fn write(&self, name: &str, file: &[u8]) -> io::Result<()> {
        fs::write(self.path.join(name), &file).await
    }

    /// Delete file from bucket.
    ///
    /// Returns an error if the file doesn't exists.
    pub async fn delete(&self, name: &str) -> io::Result<()> {
        fs::remove_file(self.path.join(name)).await
    }

    /// Delete all files inside the bucket.
    ///
    /// Use very carefully.
    pub async fn clear(&self) -> io::Result<()> {
        let mut files = fs::read_dir(&self.path).await?;
        while let Some(file) = files.next_entry().await? {
            fs::remove_file(file.path()).await?;
        }
        Ok(())
    }
}
