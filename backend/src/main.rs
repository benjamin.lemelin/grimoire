#![deny(missing_debug_implementations)]
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

use config::Config;

mod app;
mod config;
mod repository;
mod database;
mod index;
mod storage;
mod util;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // Load environment variables from ".env" file, if any.
    dotenvy::dotenv().ok();

    // Configure tracing/logging.
    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| {
            "app=info".into()
        }))
        .with(tracing_subscriber::fmt::layer())
        .init();

    // Start and run server.
    app::run(Config::parse()).await
}