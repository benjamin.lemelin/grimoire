fn main() {
    // Trigger recompilation when the Cargo.toml file is changed.
    // To ensure Clap about message always has up to date information about the binary.
    println!("cargo:rerun-if-changed=Cargo.toml");
    // Trigger recompilation when a new migration is added.
    // To ensure SQLX migrations are always up to date.
    println!("cargo:rerun-if-changed=migrations");
}