#/bin/sh
set -e

target=$(if [ -n "$1" ]; then echo "--target $1"; fi)

cd backend
sed -i "s/^version = \".*\"$/version = \"${PACKAGE_VERSION}\"/g" Cargo.toml
cargo build $target --release