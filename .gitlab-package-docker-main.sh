#/bin/sh
set -e

build() {
  backend_artifacts=$1
  suffix=$2
  platform=$(if [ -n "$3" ]; then echo "--platform $3"; fi)

  docker build \
         --build-arg backend_dir=$backend_artifacts \
         --build-arg frontend_dir=${FRONTEND_ARTIFACTS} \
         $platform \
         -t ${PACKAGE_DOCKER_IMAGE}:latest-$suffix \
         -t ${PACKAGE_DOCKER_IMAGE}:${PACKAGE_VERSION}-$suffix \
         .
  docker push ${PACKAGE_DOCKER_IMAGE}:latest-$suffix
  docker push ${PACKAGE_DOCKER_IMAGE}:${PACKAGE_VERSION}-$suffix
}

publish() {
  version=$1

  docker manifest create \
     ${PACKAGE_DOCKER_IMAGE}:$version \
     --amend ${PACKAGE_DOCKER_IMAGE}:$version-amd64 \
     --amend ${PACKAGE_DOCKER_IMAGE}:$version-arm64
  docker manifest push ${PACKAGE_DOCKER_IMAGE}:$version
}

build ${BACKEND_ARTIFACTS_X86_64} amd64
build ${BACKEND_ARTIFACTS_AARCH64} arm64 linux/arm64/v8

publish latest
publish ${PACKAGE_VERSION}