<div align="center">

![Grimoire](../.docs/icon-large.svg)

# Frontend

</div>

## Building from sources

These instructions will guide you on how to build the frontend of the application. First, you'll need to install the
[NodeJs] JavaScript runtime. Then, install the dependencies :

```shell
npm install
```

Finally, build the application :

```shell
npm run build
```

## Running in development mode

The frontend requires a running backend on port `8080`. Please refer to the backend [README](../backend/README.md)
file for more information on how to build and. Then, you should be able to start a development server for the frontend 
and the backend using those commands :

```shell
cargo run
npm run dev
```

The frontend will start listening for http connections on the `3000` port. If you want to use https, you'll need to
provide your own self-signed certificate inside the `.cert` folder. This might sound scary, but in reality, this is
very easy to do. Refer to the [appendix section](../backend/README.md#generating-self-signed-certificates) of the 
backend [README](../backend/README.md) file for more information. After all that, start the frontend and the backend in
https mode using those commands :

```shell
cargo run -- --secure-cookies
npm run dev-https
```

[NodeJs]: https://nodejs.org/en/