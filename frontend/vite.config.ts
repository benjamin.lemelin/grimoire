import {defineConfig} from "vite";
import SolidPlugin from "vite-plugin-solid";
import {VitePWA} from "vite-plugin-pwa";

export default defineConfig({
  server: {
    port: 3000,
    https: {
      key: ".cert/key.pem",
      cert: ".cert/cert.pem"
    },
    proxy: {
      "/api": "http://127.0.0.1:8080/"
    }
  },
  css: {
    modules: {
      localsConvention: "camelCase"
    },
  },
  plugins: [
    SolidPlugin(),
    VitePWA({
      manifest: {
        name: "Grimoire",
        short_name: "Grimoire",
        description: "Outil pour la consignation de recettes.",
        lang: "fr",
        theme_color: "#222222",
        background_color: "#222222",
        icons: [
          {
            src: "icon-192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "icon-512.png",
            sizes: "512x512",
            type: "image/png"
          }
        ]
      },
      registerType: "autoUpdate",
      workbox: {
        globPatterns: ["**/*.{js,css,html,ico,png,svg}"]
      }
    })
  ],
  build: {
    target: 'esnext',
  },
});
