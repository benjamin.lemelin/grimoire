import {Icon} from "../icon";

import Styles from "./style.module.css";

type Props = {
  yield: string
}

export function YieldTag(props: Props) {
  const yielding = () => {
    if (props.yield.trim().length === 0) return "--"
    else return props.yield;
  };

  return (
    <span class={Styles.yield}>
      <Icon icon="soup_kitchen"/>
      <span>{yielding()}</span>
    </span>
  );
}