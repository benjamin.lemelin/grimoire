import {ThemedImg} from "../theme";

import Style from "./style.module.css";
import BrandIconLight from "/img/brand-icon-light.svg";
import BrandIconDark from "/img/brand-icon-dark.svg";
import {useI18n} from "../i18n";

type Props = {
  class?: string
}

export function Brand(props: Props) {
  const [t] = useI18n();
  
  return (
    <span class={`${Style.brand} ${props.class ?? ""}`} aria-label={t("grimoire")}>
      <ThemedImg class={Style.logo} src={{light: BrandIconLight, dark: BrandIconDark}} alt={t("grimoire")}/>
      {t("grimoire")}
    </span>
  );
}