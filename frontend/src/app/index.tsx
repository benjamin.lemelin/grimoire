import {Show} from "solid-js";
import {Navigate, Route, Routes, useSearchParams} from "@solidjs/router";
import {useAuth} from "../auth";
import {HeaderBar} from "../headerbar";
import {Navbar} from "../navbar";
import {NotificationDrawer} from "../notification";
import {LoginRoute} from "../login";
import {SetupRoute} from "../setup";
import {HomeRoute} from "../home";
import {RecipeEditRoute, RecipeListRoute, RecipeNewRoute, RecipeSearchRoute, RecipeViewRoute} from "../recipe";
import {CategoryListRoute, CategoryViewRoute} from "../category";
import {
  SettingsAccountRoute,
  SettingsClearDatabaseRoute,
  SettingsDatabaseRoute,
  SettingsDeleteUserRoute,
  SettingsDownloadDatabaseRoute,
  SettingsNewUserRoute,
  SettingsPasswordRoute,
  SettingsRestoreDatabaseRoute,
  SettingsRoute,
  SettingsUsersRoute
} from "../settings";
import {AboutRoute} from "../about";
import {NotFoundView} from "../error";
import {Dialog} from "../util/url";

type QueryParams = {
  dialog?: Dialog
}

export function App() {
  const [auth] = useAuth();
  const [queryParams] = useSearchParams<QueryParams>();

  return (
    <>
      <HeaderBar/>
      <Navbar/>
      <main tabIndex="">
        <Routes>
          {/* Unauthenticated routes */}
          <Show when={!auth()} keyed>
            <Route path="/" component={LoginRoute}/>
            <Route path="/setup" component={SetupRoute}/>
            <Route path="*" element={<Navigate href="/"/>}/>
          </Show>
          {/* Authenticated routes */}
          <Show when={auth()} keyed>
            <Route path="/" component={HomeRoute}/>
            <Route path="/new" element={<Navigate href="..?dialog=new"/>}/>
            <Route path="/search" component={RecipeSearchRoute}/>
            <Route path="/recipes">
              <Route path="/" component={RecipeListRoute}/>
              <Route path="/:id" component={RecipeViewRoute} matchFilters={{id: /\d+/}}>
                <Route path="/"/>
                <Route path="/edit" component={RecipeEditRoute}/>
              </Route>
            </Route>
            <Route path="/categories">
              <Route path="/" component={CategoryListRoute}/>
              <Route path="/:name" component={CategoryViewRoute} matchFilters={{name: /.+/}}/>
            </Route>
            <Route path="/settings" component={SettingsRoute}>
              <Route path={["/", "/account"]} component={SettingsAccountRoute}/>
              <Route path="/password" component={SettingsPasswordRoute}/>
              <Route path="/users" component={SettingsUsersRoute}>
                <Route path="/"/>
                <Route path="/new" component={SettingsNewUserRoute}/>
                <Route path="/:username/delete" component={SettingsDeleteUserRoute} matchFilters={{username: /.+/}}/>
              </Route>
              <Route path="/users/:username" component={SettingsAccountRoute} matchFilters={{username: /.+/}}/>
              <Route path="/database" component={SettingsDatabaseRoute}>
                <Route path="/"/>
                <Route path="/download" component={SettingsDownloadDatabaseRoute}/>
                <Route path="/restore" component={SettingsRestoreDatabaseRoute}/>
                <Route path="/clear" component={SettingsClearDatabaseRoute}/>
              </Route>
              <Route path="/help" component={AboutRoute}/>
            </Route>
            <Route path="*" component={NotFoundView}/>
          </Show>
        </Routes>
      </main>
      <NotificationDrawer/>
      {/* Global dialogs, accessible anywhere. */}
      <Show when={queryParams.dialog === "new"} keyed>
        <RecipeNewRoute/>
      </Show>
    </>
  );
}
