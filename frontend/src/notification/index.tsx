import {JSX} from "solid-js";
import {toast, Toaster} from "solid-toast";

import Style from "./style.module.css";

type Notify = BasicNotification & {
  success: BasicNotification,
  error: BasicNotification,
  loading: BasicNotification,
  promise: PromiseNotification
}

type BasicNotification = (message: NotificationMessage, options?: { id?: string }) => string;
type PromiseNotification = <T>(promise: Promise<T>, options: {
  pending: NotificationMessage,
  success: PromiseMessage<T>,
  error: PromiseMessage<any>
}) => string;

type NotificationMessage = (() => JSX.Element) | JSX.Element | string;
type PromiseMessage<T> = ((value: T) => JSX.Element) | JSX.Element | string;

export const useNotify = () => notify;

const notify: Notify = (message, options) => {
  return toast(message, {id: options?.id, duration: 5000});
};

notify.success = (message, options) => {
  return toast.success(message, {id: options?.id, duration: 5000});
};

notify.error = (message, options) => {
  return toast.error(message, {id: options?.id, duration: 10000});
};

notify.loading = (message, options) => {
  return toast.loading(message, {id: options?.id, duration: Infinity});
};

notify.promise = (promise, options) => {
  const id = notify.loading(options.pending);
  promise.then(value => {
    notify.success(typeof options.success === "function" ? options.success(value) : options.success, {
      id
    });
  }).catch(error => {
    notify.error(typeof options.error === "function" ? options.error(error) : options.error, {
      id
    });
  });
  return id;
};

export function NotificationDrawer() {
  return (
    <section class={Style.notifications}>
      <Toaster containerClassName={Style.container} toastOptions={{
        className: Style.notification,
        position: "bottom-left",
        iconTheme: {
          primary: "#fff",
          secondary: "#000"
        },
        ariaProps: {
          role: "alert",
          "aria-live": "polite"
        }
      }}/>
    </section>
  );
}