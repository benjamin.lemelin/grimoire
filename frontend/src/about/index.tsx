import {A} from "@solidjs/router";
import {Brand} from "../brand";
import {Icon} from "../icon";
import {useI18n} from "../i18n";

import Style from "./style.module.css";

export function AboutRoute() {
  const [t] = useI18n();

  return (
    <section class={Style.about}>
      <header class={Style.header}>
        <A class="button circle transparent desktop-hide" href="/settings">
          <Icon icon="arrow_back"/>
        </A>
        <Brand/>
      </header>
      <h1>{t("about.title")}</h1>
      <p>
        {t("about.description")}
      </p>
      <h2>{t("about.license")}</h2>
      <p>
        The Software is provided “as is”, without warranty of any kind, express or implied, including but not limited to
        the warranties of merchantability, fitness for a particular purpose and noninfringement. In no event shall the
        authors or copyright holders be liable for any claim, damages or other liability, whether in an action of
        contract, tort or otherwise, arising from, out of or in connection with the software or the use or other
        dealings in the Software.
      </p>
    </section>
  );
}