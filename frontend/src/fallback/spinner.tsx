import {Spinner} from "../spinner";

import Style from "./spinner.module.css";

type Props = {
  class?: string
}

export function SpinnerFallback(props : Props) {
  return (
    <div class={`${Style.spinnerFallback} ${props.class ?? ""}`} role="alert" aria-busy="true" aria-live="polite">
      <Spinner/>
    </div>
  )
}