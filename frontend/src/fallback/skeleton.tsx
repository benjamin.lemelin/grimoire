import {For, createSignal, onMount} from "solid-js";
import {ClassList, isDesktop} from "../util/styling";

import Style from "./skeleton.module.css";

const CHIP_GAP = 10;
const CHIP_ROWS = 2;
const CHIP_LAST_LINE_OFFSET = 1;
const CHIP_WIDTH = 150 + CHIP_GAP;

const CARD_GAP = 20;
const CARD_ROWS = 1;
const CARD_RECIPE_WIDTH = 350 + CARD_GAP;
const CARD_RECIPE_WIDTH_HORIZONTAL = 450 + CARD_GAP;
const CARD_RECIPE_HEIGHT = 300 + CARD_GAP;
const CARD_RECIPE_HEIGHT_HORIZONTAL = 100 + CARD_GAP;
const CARD_CATEGORY_WIDTH = 200 + CARD_GAP;
const CARD_CATEGORY_WIDTH_HORIZONTAL = 450 + CARD_GAP;
const CARD_CATEGORY_HEIGHT = 300 + CARD_GAP;
const CARD_CATEGORY_HEIGHT_HORIZONTAL = 100 + CARD_GAP;
const CARD_USER_WIDTH = 400 + CARD_GAP;
const CARD_USER_HEIGHT = 70 + CARD_GAP;

type SkeletonProps = {
  class?: string,
  classList?: ClassList,
}

export function Skeleton(props: SkeletonProps) {
  return (
    <div class={`${Style.skeleton} ${props.class ?? ""} shine`} classList={props.classList}></div>
  )
}

type ChipSkeletonProps = {
  class?: string
}

export function ChipSkeleton(props: ChipSkeletonProps) {
  return (
    <Skeleton class={`${Style.chip} ${props.class ?? ""} chip`}/>
  );
}

type ChipListSkeletonProps = {
  class?: string
}

export function ChipListSkeleton(props: ChipListSkeletonProps) {
  let chipListElement: HTMLDivElement;
  const [nbItems, setNbItems] = createSignal(0);

  // Calculate how many chips can fit inside the skeleton.
  onMount(() => {
    const containerWidth = chipListElement.getBoundingClientRect().width;

    const rows = CHIP_ROWS;
    const cols = Math.floor(containerWidth / CHIP_WIDTH);

    setNbItems(rows * cols - CHIP_LAST_LINE_OFFSET);
  });

  return (
    <div ref={el => chipListElement = el} class={`${Style.chipList} ${props.class ?? ""}`}>
      <For each={Array(nbItems())}>{() => <ChipSkeleton/>}</For>
    </div>
  );
}

type CardSkeletonType = "recipe" | "category" | "user";

type CardSkeletonProps = {
  class?: string,
  type: CardSkeletonType,
  horizontal?: boolean
}

export function CardSkeleton(props: CardSkeletonProps) {
  return (
    <Skeleton class={`${Style.card} ${props.class ?? ""} card`}
              classList={{
                [Style.horizontal]: props.horizontal,
                [Style.recipe]: props.type === "recipe",
                [Style.category]: props.type === "category",
                [Style.user]: props.type === "user",
              }}
    />
  );
}

type CardListSkeletonProps = {
  class?: string,
  minItems?: number,
  skeleton: CardSkeletonType,
  horizontal?: boolean,
}

export function CardListSkeleton(props: CardListSkeletonProps) {
  let cardListElement: HTMLDivElement;
  const [nbItems, setNbItems] = createSignal(0);

  // Calculate how many cards can fit inside the skeleton.
  onMount(() => {
    const containerBounds = cardListElement.getBoundingClientRect();
    const containerWidth = containerBounds.width;
    const containerHeight = containerBounds.height;

    const isHorizontal = props.horizontal || !isDesktop();

    let itemWidth;
    let itemHeight;
    if (props.skeleton === "recipe") {
      itemWidth = isHorizontal ? CARD_RECIPE_WIDTH_HORIZONTAL : CARD_RECIPE_WIDTH;
      itemHeight = isHorizontal ? CARD_RECIPE_HEIGHT_HORIZONTAL : CARD_RECIPE_HEIGHT;
    } else if (props.skeleton === "category") {
      itemWidth = isHorizontal ? CARD_CATEGORY_WIDTH_HORIZONTAL : CARD_CATEGORY_WIDTH;
      itemHeight = isHorizontal ? CARD_CATEGORY_HEIGHT_HORIZONTAL : CARD_CATEGORY_HEIGHT;
    } else {
      itemWidth = CARD_USER_WIDTH;
      itemHeight = CARD_USER_HEIGHT;
    }

    const rows = Math.floor(containerHeight / itemHeight);
    const cols = Math.max(Math.floor(containerWidth / itemWidth), CARD_ROWS);

    setNbItems(Math.max(props.minItems ?? 0, rows * cols));
  });

  return (
    <div ref={el => cardListElement = el} class={`${Style.cardList} ${props.class ?? ""}`}>
      <For each={Array(nbItems())}>{() => <CardSkeleton type={props.skeleton} horizontal={props.horizontal}/>}</For>
    </div>
  );
}