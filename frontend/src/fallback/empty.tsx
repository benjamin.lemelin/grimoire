import {JSX} from "solid-js";
import {Icon} from "../icon";

import Style from "./empty.module.css";

type Props = {
  class?: string,
  icon?: string,
  children?: JSX.Element
}

export function EmptyStateFallback(props: Props) {
  return (
    <div class={`${Style.empty} ${props.class ?? ""}`}>
      <Icon icon={props.icon ?? "menu_book"}/>
      <h1>{props.children}</h1>
    </div>
  );
}