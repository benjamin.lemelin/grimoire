import Style from "./style.module.css";

type Props = {
  small?: boolean
}

export function Spinner(props: Props) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg"
         class={Style.spinner}
         classList={{[Style.small]: props.small}}
         width="48px"
         height="48px"
         viewBox="0 0 48 48">
      <circle class={Style.path} fill="none" stroke-width="4" stroke-linecap="round" cx="24" cy="24" r="18"/>
    </svg>
  );
}