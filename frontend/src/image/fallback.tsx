import {createRenderEffect, createSignal} from "solid-js";
import {ImageFadeIn} from "./fade";

import Style from "./fallback.module.css";
import BlankImg from "/img/blank.svg";

type Props = {
  class?: string,
  src?: string,
  alt: string,
}

export function ImageFallback(props: Props) {
  const [src, setSrc] = createSignal<string>();

  createRenderEffect(() => {
    setSrc(props.src);
  });

  const onError = () => {
    setSrc();
  };

  return (
    <ImageFadeIn
      class={`${Style.imageFallback} ${props.class ?? ""}`}
      src={src() ?? BlankImg}
      alt={props.alt}
      onError={onError}
    />
  );
}