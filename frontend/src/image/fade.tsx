import {createRenderEffect, on, createSignal} from "solid-js";
import {createCssTransition} from "../util/styling";

import Style from "./fade.module.css";

type Props = {
  class?: string,
  src: string,
  alt: string,
  onLoad?: () => void,
  onError?: () => void,
}

export function ImageFadeIn(props: Props) {
  let skeletonElement: HTMLDivElement;
  const [isLoading, setLoading] = createSignal(false);

  createRenderEffect(on(() => props.src,() => {
    setLoading(true);
  }));

  createCssTransition(() => skeletonElement, isLoading, {
    enter: Style.visibleEnter, enterImmediate: true,
    active: Style.visible,
    exit: Style.visibleExit
  });

  function onLoad() {
    setLoading(false);
    props.onLoad?.();
  }

  return (
    <div class={`${Style.container} ${props.class ?? ""}`}>
      <img class={Style.image}
           src={props.src}
           alt={props.alt}
           onLoad={onLoad}
           onError={props.onError}
      />
      <div ref={el => skeletonElement = el} class={Style.skeleton}>
        <div class="shine"></div>
      </div>
    </div>
  );
}