import {useI18n} from "../i18n";
import {Icon} from "../icon";

import Style from "./not-found-view.module.css";

export function NotFoundView() {
  const [t] = useI18n();

  return (
    <div class={Style.notFound}>
      <Icon class={Style.icon} icon="cookie"/>
      <h1 class={Style.title}>{t("notFound.title")}</h1>
      <h2>{t("notFound.subtitle")}</h2>
      {t("notFound.text")}
    </div>
  )
}