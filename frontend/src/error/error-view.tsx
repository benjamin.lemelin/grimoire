import {Show} from "solid-js";
import {ApiError} from "../api";
import {NotFoundView} from "./not-found-view";
import {useErrorHandler} from "./index";

type Props = {
  error: Error
}

export function ErrorView(props: Props) {
  const handleError = useErrorHandler();

  handleError(props.error);

  return (
    <Show when={props.error.message === ApiError.NotFound} keyed>
      <NotFoundView/>
    </Show>
  );
}