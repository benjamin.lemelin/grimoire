import {useNavigate} from "@solidjs/router";
import {ApiError} from "../api";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {useNotify} from "../notification";

export * from "./error-view";
export * from "./not-found-view";

export function useErrorHandler() {
  const [t] = useI18n();
  const [, setAuth] = useAuth();
  const navigate = useNavigate();
  const notify = useNotify();

  return function (error: any, skipNotification?: boolean) {
    if (error instanceof Error) {
      const message = error.message;

      if (message === ApiError.Unauthorized) {
        // User needs to authenticate.
        setAuth();
        navigate("/");
        return t("error.unauthorized");
      } else if (message === ApiError.Forbidden) {
        // User doesn't have access to requested ressource.
        let message = t("error.forbidden");
        if (!skipNotification) notify.error(message);
        return message;
      } else if (message === ApiError.NotFound) {
        // Resource not found.
        return t("error.notFound");
      } else if (message === ApiError.DemoMode) {
        // Unavailable due to being in demo mode.
        let message = t("error.demoMode");
        if (!skipNotification) notify.error(message);
        return message;
      }
    }

    // Unexpected error. Tell the user to try again and log the error.
    console.error(error);
    if (!skipNotification) notify.error(t("error.unknown"));
    return t("error.unknown");
  };
}