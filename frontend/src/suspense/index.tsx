import {JSX, Match, Resource, Switch} from "solid-js";
import {ErrorView} from "../error";

type Props<T> = {
  resource: Resource<T>,
  loading?: JSX.Element,
  fallback?: JSX.Element,
  children: JSX.Element | ((value: T) => JSX.Element)
}

export function Suspense<T>(props: Props<T>) {
  // We are not using Solid's suspense, because fallbacks are not shown when a refresh is requested, while this
  // version does. Also, this component automatically handles application specific api errors, so there is less
  // code on call side (no more <Show> inside a <Suspense> inside an <ErrorBoundary> ...).
  return (
    <Switch fallback={props.fallback}>
      <Match when={props.resource.loading} keyed>{
        props.loading
      }</Match>
      <Match when={props.resource.error} keyed>{e =>
        <ErrorView error={e}/>
      }</Match>
      <Match when={props.resource()} keyed>{resource =>
        typeof props.children === "function" ? props.children(resource) : props.children}
      </Match>
    </Switch>
  );
}