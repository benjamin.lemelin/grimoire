import {createSignal, Accessor} from "solid-js";
import {createSingletonRoot} from "@solid-primitives/rootless";
import {API, User} from "../api";

export type Auth = {
  user: Accessor<User | undefined>,
}

type AuthSignal = [get: AuthAccessor, set: AuthSetter];
type AuthAccessor = () => Auth | undefined;
type AuthSetter = (user?: User) => void;

export const useAuth: () => AuthSignal = createSingletonRoot<AuthSignal>(() => {
  const [auth, setAuth] = createSignal<Auth>(); // Login information.
  const [user, setUser] = createSignal<User>(); // Logged in user.

  // Try to load username from local storage. If found, fetch user information.
  const username = loadUsername();
  if (username) {
    setAuth({user} as Auth); // User is considered logged in ...
    setUser(undefined);      // ... but user information is not available.

    API.findUserByUsername(username).then(user => {
      setAuthUser(user);
    }).catch(() => {
      setAuthUser(undefined);
    });
  }

  function setAuthUser(newUser?: User) {
    if (newUser) {
      setAuth({user} as Auth); // User is logged in ...
      setUser(newUser);        // ... and here is their profile.

      saveUsername(newUser.username()); // Save username to local storage.
    } else {
      setAuth(undefined); // No user is logged in ...
      setUser(undefined); // ... and thus, there is no profile available.

      saveUsername(undefined); // Remove username from local storage.
    }
  }

  return [auth, setAuthUser];
});

const USERNAME_LOCAL_STORAGE_KEY = "username";

function loadUsername(): string | undefined {
  const username = localStorage.getItem(USERNAME_LOCAL_STORAGE_KEY);
  if (username) return username;
  else return undefined;
}

function saveUsername(username?: string): void {
  if (username) localStorage.setItem(USERNAME_LOCAL_STORAGE_KEY, username);
  else localStorage.removeItem(USERNAME_LOCAL_STORAGE_KEY);
}