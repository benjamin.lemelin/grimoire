import {Category, Difficulty, Duration, Ingredient, Instruction, Recipe, Role} from "./model";

export type RecipeDTO = {
  readonly id: number,
  readonly name: string,
  readonly author: string,
  readonly description: string,
  readonly duration: DurationDTO,
  readonly yield: string,
  readonly difficulty: DifficultyDTO,
  readonly isFavorite: boolean,
  readonly hasImage: boolean,
  readonly categories: CategoryDTO[],
  readonly ingredients: IngredientDTO[],
  readonly instructions: InstructionDTO[]
}

export type IngredientDTO = {
  readonly name: string,
  readonly quantity: string,
  readonly comment: string,
}

export type InstructionDTO = {
  readonly text: string
}

export type RecipeSummaryDTO = {
  readonly id: number,
  readonly name: string,
  readonly author: string,
  readonly description: string,
  readonly duration: DurationDTO,
  readonly yield: string,
  readonly difficulty: DifficultyDTO,
  readonly isFavorite: boolean,
  readonly hasImage: boolean
}

export type NewRecipeDTO = {
  readonly name: string,
  readonly author: string,
  readonly description: string,
  readonly duration: DurationDTO,
  readonly yield: string,
  readonly difficulty: DifficultyDTO,
  readonly isFavorite: boolean,
  readonly hasImage: boolean,
  readonly categories: CategoryDTO[],
  readonly ingredients: IngredientDTO[],
  readonly instructions: InstructionDTO[]
}

export type UserDTO = {
  readonly username: string,
  readonly email: string,
  readonly firstName: string,
  readonly lastName: string,
  readonly role: RoleDTO
}

export type NewUserDTO = {
  readonly username: string,
  readonly password: string,
  readonly role: RoleDTO,
}

export type UpdateUsernameDTO = {
  readonly username: string
}

export type UpdatePasswordDTO = {
  readonly old: string,
  readonly new: string,
}

export type UpdateUserProfileDTO = {
  readonly email: string,
  readonly firstName: string,
  readonly lastName: string,
}

export type UpdateRoleDTO = {
  readonly role: RoleDTO
}

export type LoginDTO = {
  readonly username: String,
  readonly password: String,
}

export type SetupDTO = {
  readonly username: String,
  readonly password: String,
}

export type DurationDTO = Duration;
export type DifficultyDTO = Difficulty;
export type CategoryDTO = Category;
export type RoleDTO = Role;

function createJson<T extends object>(dto: T): Blob {
  return new Blob([JSON.stringify(dto)], {type: "application/json"})
}

function createNewRecipeDTO(recipe: Recipe): FormData {
  const currentRecipe = recipe;
  const currentImage = recipe.image();

  const data = new FormData();

  // Recipe.
  data.set("recipe", createJson({
    name: currentRecipe.name(),
    author: currentRecipe.author(),
    description: currentRecipe.description(),
    duration: currentRecipe.duration(),
    yield: currentRecipe.yield(),
    difficulty: currentRecipe.difficulty(),
    isFavorite: currentRecipe.isFavorite(),
    hasImage: currentRecipe.image() !== undefined,
    categories: currentRecipe.categories(),
    ingredients: currentRecipe.ingredients().map(createIngredientDto),
    instructions: currentRecipe.instructions().map(createInstructionDto)
  }));

  // Image.
  if (currentImage && typeof currentImage !== "string") {
    data.set("image", currentImage);
  }

  return data;
}

function createIngredientDto(ingredient: Ingredient): IngredientDTO {
  return {
    name: ingredient.name(),
    quantity: ingredient.quantity(),
    comment: ingredient.comment(),
  }
}

function createInstructionDto(instruction: Instruction): InstructionDTO {
  return {
    text: instruction.text()
  }
}

function createNewUserDTO(username: string, password: string, role: Role): Blob {
  return createJson<NewUserDTO>({
    username,
    password,
    role
  });
}

function createUpdateUsernameDTO(username: string): Blob {
  return createJson<UpdateUsernameDTO>({
    username,
  });
}

function createUpdatePasswordDTO(oldPassword: string, newPassword: string): Blob {
  return createJson<UpdatePasswordDTO>({
    old: oldPassword,
    new: newPassword
  });
}


function createUpdateUserProfileDTO(firstName: string, lastName: string, email: string): Blob {
  return createJson<UpdateUserProfileDTO>({
    firstName,
    lastName,
    email
  });
}

function createUpdateRoleDTO(role: Role): Blob {
  return createJson<UpdateRoleDTO>({
    role
  });
}

function createLoginDTO(username: string, password: string): Blob {
  return createJson<LoginDTO>({
    username,
    password
  });
}

function createSetupDTO(username: string, password: string): Blob {
  return createJson<SetupDTO>({
    username,
    password
  });
}

export const DtoFactory = {
  createNewRecipeDTO,
  createUpdateRecipeDTO: createNewRecipeDTO,
  createNewUserDTO,
  createUpdateUsernameDTO,
  createUpdatePasswordDTO,
  createUpdateUserProfileDTO,
  createUpdateRoleDTO,
  createLoginDTO,
  createSetupDTO,
}