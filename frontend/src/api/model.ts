import {Accessor, createSignal, Setter} from "solid-js";
import {DifficultyDTO, DurationDTO, IngredientDTO, InstructionDTO, RecipeDTO, RecipeSummaryDTO, UserDTO} from "./dto";
import {move, push, remove} from "../util/array";

type Adder<T> = (value: T) => void;
type Remover<T> = (index: number) => void;
type Mover<T> = (from: number, to: number) => void;

export type Recipe = {
  readonly id: Accessor<number>,
  readonly url: Accessor<string>,
  readonly setId: Setter<number>,

  readonly name: Accessor<string>,
  readonly setName: Setter<string>

  readonly author: Accessor<string>,
  readonly setAuthor: Setter<string>,

  readonly description: Accessor<string>,
  readonly setDescription: Setter<string>,

  readonly duration: Accessor<Duration>,
  readonly setDuration: Setter<Duration>,

  readonly yield: Accessor<string>,
  readonly setYield: Setter<string>

  readonly difficulty: Accessor<Difficulty>,
  readonly setDifficulty: Setter<Difficulty>

  readonly isFavorite: Accessor<boolean>,
  readonly setFavorite: Setter<boolean>,

  readonly image: Accessor<Image | undefined>,
  readonly imageUrl: Accessor<string | undefined>,
  readonly setImage: Setter<Image | undefined>,

  readonly categories: Accessor<Category[]>,
  readonly addCategory: Adder<Category>,
  readonly removeCategory: Remover<Category>,
  readonly moveCategory: Mover<Category>,

  readonly ingredients: Accessor<Ingredient[]>,
  readonly addIngredient: Adder<Ingredient>,
  readonly removeIngredient: Remover<Ingredient>,
  readonly moveIngredient: Mover<Ingredient>,

  readonly instructions: Accessor<Instruction[]>,
  readonly addInstruction: Adder<Instruction>,
  readonly removeInstruction: Remover<Instruction>,
  readonly moveInstruction: Mover<Instruction>,
}

export type Ingredient = {
  readonly name: Accessor<string>,
  readonly setName: Setter<string>,

  readonly quantity: Accessor<string>,
  readonly setQuantity: Setter<string>,

  readonly comment: Accessor<string>,
  readonly setComment: Setter<string>
}

export type Instruction = {
  readonly text: Accessor<string>,
  readonly setText: Setter<string>
}

export type RecipeSummary = {
  readonly id: Accessor<number>,
  readonly url: Accessor<string>,
  readonly name: Accessor<string>,
  readonly author: Accessor<string>,
  readonly description: Accessor<string>,
  readonly duration: Accessor<Duration>,
  readonly yield: Accessor<string>,
  readonly difficulty: Accessor<Difficulty>,
  readonly isFavorite: Accessor<boolean>,
  readonly image: Accessor<Image | undefined>,
  readonly imageUrl: Accessor<string | undefined>,
}

export type User = {
  readonly username: Accessor<string>,
  readonly setUsername: Setter<string>,

  readonly email: Accessor<string>,
  readonly setEmail: Setter<string>,

  readonly firstName: Accessor<string>,
  readonly setFirstName: Setter<string>,

  readonly lastName: Accessor<string>,
  readonly setLastName: Setter<string>,

  readonly role: Accessor<Role>,
  readonly setRole: Setter<Role>,
  readonly hasRole: (role: Role) => boolean
}

export type Duration = { readonly seconds: number }
export type Difficulty = "easy" | "normal" | "hard";
export type Image = File | string;
export type Category = string;

export type Role = "guest" | "user" | "admin";

function createAccessor<T>(value: T): Accessor<T> {
  return () => value;
}

function createAdder<T>(setter: Setter<T[]>): Adder<T> {
  return value => setter(values => push(values, value));
}

function createRemover<T>(setter: Setter<T[]>): Remover<T> {
  return index => setter(values => remove(values, index));
}

function createMover<T>(setter: Setter<T[]>): Mover<T> {
  return (from, to) => setter(values => move(values, from, to));
}

function createRecipeUrl(id: Accessor<number>): Accessor<string> {
  return () => {
    const currentId = id();
    return `/recipes/${currentId}`;
  };
}

function createImageUrl(image: Accessor<Image | undefined>): Accessor<string | undefined> {
  return () => {
    const currentImage = image();

    return typeof currentImage === "string" ? currentImage : undefined;
  };
}

function createRecipe(dto?: RecipeDTO): Recipe {
  if (!dto) {
    dto = {
      id: 0,
      name: "",
      author: "",
      description: "",
      duration: {seconds: 0},
      yield: "",
      difficulty: "normal",
      isFavorite: false,
      hasImage: false,
      categories: [],
      ingredients: [],
      instructions: []
    };
  }

  const [id, setId] = createSignal(dto.id);
  const url = createRecipeUrl(id);
  const [name, setName] = createSignal(dto.name);
  const [author, setAuthor] = createSignal(dto.author);
  const [description, setDescription] = createSignal(dto.description);
  const [duration, setDuration] = createSignal(createDuration(dto.duration));
  const [_yield, setYield] = createSignal(dto.yield);
  const [difficulty, setDifficulty] = createSignal(createDifficulty(dto.difficulty));
  const [isFavorite, setFavorite] = createSignal(dto.isFavorite);
  const [image, setImage] = createSignal(createImage(dto.id, dto.hasImage));
  const imageUrl = createImageUrl(image);
  const [categories, setCategories] = createSignal(dto.categories);
  const addCategory = createAdder(setCategories);
  const removeCategory = createRemover(setCategories);
  const moveCategory = createMover(setCategories);
  const [ingredients, setIngredients] = createSignal(dto.ingredients.map(createIngredient));
  const addIngredient = createAdder(setIngredients);
  const removeIngredient = createRemover(setIngredients);
  const moveIngredient = createMover(setIngredients);
  const [instructions, setInstructions] = createSignal(dto.instructions.map(createInstruction));
  const addInstruction = createAdder(setInstructions);
  const removeInstruction = createRemover(setInstructions);
  const moveInstruction = createMover(setInstructions);

  return {
    id, url, setId,
    name, setName,
    author, setAuthor,
    description, setDescription,
    duration, setDuration,
    yield: _yield, setYield,
    difficulty, setDifficulty,
    isFavorite, setFavorite,
    image, imageUrl, setImage,
    categories, addCategory, removeCategory, moveCategory,
    ingredients, addIngredient, removeIngredient, moveIngredient,
    instructions, addInstruction, removeInstruction, moveInstruction
  };
}

function createIngredient(dto?: IngredientDTO): Ingredient {
  if (!dto) {
    dto = {
      name: "",
      quantity: "",
      comment: ""
    };
  }

  const [name, setName] = createSignal(dto.name);
  const [quantity, setQuantity] = createSignal(dto.quantity);
  const [comment, setComment] = createSignal(dto.comment);

  return {
    name, setName,
    quantity, setQuantity,
    comment, setComment,
  };
}

function createInstruction(dto?: InstructionDTO): Instruction {
  if (!dto) {
    dto = {
      text: ""
    };
  }

  const [text, setText] = createSignal(dto.text);

  return {
    text, setText
  };
}

function createRecipeSummary(dto: RecipeSummaryDTO): RecipeSummary {
  const id = createAccessor(dto.id);
  const url = createRecipeUrl(id);
  const name = createAccessor(dto.name);
  const author = createAccessor(dto.author);
  const description = createAccessor(dto.description);
  const duration = createAccessor(createDuration(dto.duration));
  const _yield = createAccessor(dto.yield);
  const difficulty = createAccessor(createDifficulty(dto.difficulty));
  const image = createAccessor(createImage(dto.id, dto.hasImage));
  const imageUrl = createImageUrl(image);
  const isFavorite = createAccessor(dto.isFavorite);

  return {
    id,
    url,
    name,
    author,
    description,
    duration,
    yield: _yield,
    difficulty,
    image,
    imageUrl,
    isFavorite
  };
}

function createUser(dto: UserDTO): User {
  const [username, setUsername] = createSignal(dto.username);
  const [email, setEmail] = createSignal(dto.email);
  const [firstName, setFirstName] = createSignal(dto.firstName);
  const [lastName, setLastName] = createSignal(dto.lastName);
  const [role, setRole] = createSignal(dto.role);
  const hasRole = (otherRole: Role) => roleToNumber(role()) >= roleToNumber(otherRole);

  return {
    username, setUsername,
    email, setEmail,
    firstName, setFirstName,
    lastName, setLastName,
    role, setRole, hasRole
  };
}

function createDuration(dto: DurationDTO): Duration {
  return dto;
}

function createDifficulty(dto: DifficultyDTO): Difficulty {
  return dto;
}

function createImage(id: number, hasImage: boolean): Image | undefined {
  return hasImage ? `/api/recipes/${id}/image` : undefined;
}

function roleToNumber(role: Role): number {
  switch (role) {
    case "admin":
      return 3;
    case "user":
      return 2;
    default:
      return 1;
  }
}

export const ModelFactory = {
  createRecipe,
  createIngredient,
  createInstruction,
  createRecipeSummary,
  createUser,
}