import {ApiError} from "../index";
import {Category, ModelFactory, Recipe, RecipeSummary, Role, User} from "../model";
import type {API as Api} from "../normal/index";

import RECIPE_1 from "./recipes/1" ;
import RECIPE_2 from "./recipes/2" ;
import RECIPE_3 from "./recipes/3" ;
import RECIPE_4 from "./recipes/4" ;
import RECIPE_5 from "./recipes/5" ;
import RECIPE_6 from "./recipes/6" ;
import RECIPE_1_IMAGE from "./images/1.jpg";
import RECIPE_2_IMAGE from "./images/2.jpg";
import RECIPE_3_IMAGE from "./images/3.jpg";
import RECIPE_4_IMAGE from "./images/4.jpg";
import RECIPE_5_IMAGE from "./images/5.jpg";
import RECIPE_6_IMAGE from "./images/6.jpg";

const RECIPES: Recipe[] = [
  ModelFactory.createRecipe(RECIPE_1),
  ModelFactory.createRecipe(RECIPE_2),
  ModelFactory.createRecipe(RECIPE_3),
  ModelFactory.createRecipe(RECIPE_4),
  ModelFactory.createRecipe(RECIPE_5),
  ModelFactory.createRecipe(RECIPE_6),
];

const IMAGES = [
  RECIPE_1_IMAGE,
  RECIPE_2_IMAGE,
  RECIPE_3_IMAGE,
  RECIPE_4_IMAGE,
  RECIPE_5_IMAGE,
  RECIPE_6_IMAGE
];

const USERS: User[] = [
  ModelFactory.createUser({
    username: "admin",
    email: "jsmith@gmail.com",
    firstName: "John",
    lastName: "Smith",
    role: "admin",
  }),
  ModelFactory.createUser({
    username: "user",
    email: "msmith@gmail.com",
    firstName: "Mary",
    lastName: "Smith",
    role: "user",
  }),
  ModelFactory.createUser({
    username: "guest",
    email: "lsmith@gmail.com",
    firstName: "Larry",
    lastName: "Smith",
    role: "guest",
  }),
];

RECIPES.forEach((recipe, i) => recipe.setImage(IMAGES[i]));

async function findAllRecipes(): Promise<RecipeSummary[]> {
  return RECIPES;
}

async function findFavoritesRecipes(): Promise<RecipeSummary[]> {
  return RECIPES.filter(it => it.isFavorite());
}

async function findRecipesByCategory(category: Category): Promise<RecipeSummary[]> {
  return RECIPES.filter(it => it.categories().includes(category));
}

async function findRecipesByKeywords(keywords: string): Promise<RecipeSummary[]> {
  const keywordsArray = keywords.trim().split(" ");
  return RECIPES.filter(recipe => {
    const name = recipe.name().toLowerCase();
    return keywordsArray.some(it => name.includes(it));
  });
}

async function findRecipeById(id: number): Promise<Recipe> {
  const recipe = RECIPES.find(it => it.id() === id);

  if (recipe) return recipe;
  else throw new Error(ApiError.NotFound);
}

async function findAllCategories(): Promise<Category[]> {
  const categories : Category[] = [];
  for (let recipe of RECIPES) {
    for (let category of recipe.categories()) {
      if (!categories.includes(category)) categories.push(category);
    }
  }
  return categories;
}

async function addRecipe(recipe: Recipe): Promise<Recipe> {
  throw new Error(ApiError.DemoMode);
}

async function updateRecipe(recipe: Recipe): Promise<Recipe> {
  throw new Error(ApiError.DemoMode);
}

async function setFavoriteRecipe(id: number): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function unsetFavoriteRecipe(id: number): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function deleteRecipe(id: number): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function login(username: string, password: string): Promise<User> {
  const user = USERS.find(it => {
    const currentUsername = it.username();
    return currentUsername === username && currentUsername === password;
  });

  if (user) {
    return user;
  } else {
    throw new Error(ApiError.BadRequest);
  }
}

async function logout(): Promise<void> {
  // Nothing to do.
}

async function findAllUsers(): Promise<User[]> {
  return USERS;
}

async function findUserByUsername(username: string): Promise<User> {
  const user = USERS.find(it => it.username() === username);

  if (user) {
    return user;
  } else {
    throw new Error(ApiError.NotFound);
  }
}

async function addUser(username: string, password: string, role: Role): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function updateUsername(oldUsername: string, username: string): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function updatePassword(username: string, oldPassword: string, newPassword: string): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function updateProfile(username: string, firstName: string, lastName: string, email: string): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function updateRole(username: string, role: Role): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function deleteUser(username: string): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function isSetupRequired(): Promise<boolean> {
  return false;
}

async function createSetupUser(username: string, password: string): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function importDatabase(database: File): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

async function exportDatabase(): Promise<File> {
  throw new Error(ApiError.DemoMode);
}

async function clearDatabase(): Promise<void> {
  throw new Error(ApiError.DemoMode);
}

export const API: typeof Api = {
  findAllRecipes,
  findFavoritesRecipes,
  findRecipesByCategory,
  findRecipesByKeywords,
  findRecipeById,
  findAllCategories,
  addRecipe,
  updateRecipe,
  setFavoriteRecipe,
  unsetFavoriteRecipe,
  deleteRecipe,
  login,
  logout,
  findAllUsers,
  findUserByUsername,
  addUser,
  updateUsername,
  updatePassword,
  updateProfile,
  updateRole,
  deleteUser,
  isSetupRequired,
  createSetupUser,
  importDatabase,
  exportDatabase,
  clearDatabase,
}