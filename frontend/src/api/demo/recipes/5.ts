import {RecipeDTO} from "../../dto";

export default {
  "id": 5,
  "name": "Coquille St-Jacques",
  "author": "Mary",
  "description": "For 8 large scallop shells.",
  "duration": {
    "seconds": 6300
  },
  "yield": "8 portions",
  "difficulty": "normal",
  "isFavorite": true,
  "hasImage": true,
  "categories": [
    "Main dish"
  ],
  "ingredients": [
    {
      "name": "#Main dish",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Butter",
      "quantity": "2 tablespoons",
      "comment": ""
    },
    {
      "name": "Large mashed potatoes",
      "quantity": "800 grams",
      "comment": ""
    },
    {
      "name": "Flour",
      "quantity": "1/3 cup",
      "comment": ""
    },
    {
      "name": "Pepper",
      "quantity": "1 pinch",
      "comment": ""
    },
    {
      "name": "Warm milk",
      "quantity": "4 1/2 cups",
      "comment": ""
    },
    {
      "name": "Scallops",
      "quantity": "8",
      "comment": "Large U15"
    },
    {
      "name": "Imitation crabmeat",
      "quantity": "8 pieces",
      "comment": ""
    },
    {
      "name": "Shallots",
      "quantity": "5",
      "comment": ""
    },
    {
      "name": "#Final touches (optional)",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Grated cheese",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Paprika",
      "quantity": "",
      "comment": ""
    },
  ],
  "instructions": [
    {
      "text": "In a pan, sauté the shallots in a little butter."
    },
    {
      "text": "In a pot, melt the butter, mix in the flour, salt, and pepper, cook the mixture briefly. Add the slightly warmed microwave milk and stir to thicken the mixture. Place the seafood and shallots in the shells, top with sauce. Put the mashed potatoes in a pastry bag, top the shells. Add grated cheese to the center. Sprinkle paprika around the potato crown. Broil in the oven at 450°F or broil for about ten minutes."
    },
    {
      "text": "It can be frozen; wrap in plastic wrap once frozen. Do not add paprika for freezing."
    }
  ]
} as RecipeDTO;