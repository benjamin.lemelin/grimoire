import {RecipeDTO} from "../../dto";

export default {
  "id": 1,
  "name": "Christmas Spiced Cookies",
  "author": "John",
  "description": "Wonderful as decoration with lemon mousse.",
  "duration": {
    "seconds": 0
  },
  "yield": "",
  "difficulty": "easy",
  "isFavorite": false,
  "hasImage": true,
  "categories": [
    "Cookies"
  ],
  "ingredients": [
    {
      "name": "Softened butter",
      "quantity": "1/2 Cup",
      "comment": ""
    },
    {
      "name": "Brown sugar",
      "quantity": "1 Cup",
      "comment": ""
    },
    {
      "name": "Egg",
      "quantity": "1",
      "comment": ""
    },
    {
      "name": "Vanilla extract",
      "quantity": "1/2 Teaspoon",
      "comment": ""
    },
    {
      "name": "Flour",
      "quantity": "1 3/4 Cups",
      "comment": ""
    },
    {
      "name": "Baking powder",
      "quantity": "1 Teaspoon",
      "comment": ""
    },
    {
      "name": "Cinnamon",
      "quantity": "1 Teaspoon",
      "comment": ""
    },
    {
      "name": "Nutmeg",
      "quantity": "1/4 Teaspoon",
      "comment": ""
    },
    {
      "name": "Crushed cloves",
      "quantity": "1 Pinch",
      "comment": ""
    }
  ],
  "instructions": [
    {
      "text": "In a bowl, beat the butter with brown sugar until the mixture is creamy. Then, add the egg and vanilla while beating. Gradually mix in all the dry ingredients. Shape into a ball, wrap, and refrigerate for at least 1 hour."
    },
    {
      "text": "On a floured surface, roll out the dough. Cut out cookies with a cookie cutter. Place them on a baking sheet. Bake in the oven at 350°F for 10 to 12 minutes. With convection, bake at 325°F for 9 minutes."
    }
  ]
} as RecipeDTO;