import {RecipeDTO} from "../../dto";

export default {
  "id": 2,
  "name": "Fruit Ketchup",
  "author": "Mary",
  "description": "What to do with green tomatoes... fruit ketchup! Mix green, less green, red, and less red tomatoes. Nothing is going to waste here.",
  "duration": {
    "seconds": 10800
  },
  "yield": "3 jars of 500 ml and 1 jar of 250 ml",
  "difficulty": "easy",
  "isFavorite": false,
  "hasImage": true,
  "categories": [
    "Preserves"
  ],
  "ingredients": [
    {
      "name": "Green or red tomatoes, diced",
      "quantity": "1 kg",
      "comment": ""
    },
    {
      "name": "Bell pepper, diced",
      "quantity": "1",
      "comment": "about 100 gr"
    },
    {
      "name": "Pears, diced",
      "quantity": "2",
      "comment": "about 320 gr"
    },
    {
      "name": "Peaches, diced",
      "quantity": "2",
      "comment": "about 230 gr"
    },
    {
      "name": "Spartan, Empire, or Cortland apples",
      "quantity": "3",
      "comment": "about 380 gr (avoid Macintosh, too mushy)"
    },
    {
      "name": "Red onion, diced",
      "quantity": "1",
      "comment": "about 200 gr"
    },
    {
      "name": "White vinegar",
      "quantity": "450 ml",
      "comment": ""
    },
    {
      "name": "Brown sugar",
      "quantity": "500 ml",
      "comment": ""
    },
    {
      "name": "Marinade spices",
      "quantity": "60 ml",
      "comment": ""
    },
    {
      "name": "Kosher salt",
      "quantity": "2.5 ml",
      "comment": ""
    }
  ],
  "instructions": [
    {
      "text": "Soak the tomatoes in boiling water for 15 to 20 seconds.\nRemove from the water; the skin should have started to split.\nLet them cool a bit and peel off the skin.\nFor the larger tomatoes, remove the tough core, then dice or chop."
    },
    {
      "text": "Peel the fruits and dice them.\nAlso, dice the bell pepper and onion."
    },
    {
      "text": "In a tall pot, combine all the ingredients except the marinade spices.\nBring to a boil, then reduce the heat and simmer for 2 hours.\n\nPlace the marinade spices in a cheesecloth and tie it with twine or use a tea ball.\nAdd it to the mixture for the last 30 minutes."
    },
    {
      "text": "Remove the cheesecloth bag or tea ball.\n\nIf you want to thicken the ketchup, take some vinegar from the pot and dissolve cornstarch in it. CAUTION: The mixture can thicken as it cools."
    },
    {
      "text": "Sterilize the jars in boiling water for 5 minutes. Pour the boiling mixture into the jars, leaving a 3 mm headspace.\nClean the jar rims, briefly dip the lids in boiling water, and screw them on not too tightly.\n\nRepeat these steps for each jar."
    },
    {
      "text": "If you want to store them in the pantry, sterilize in boiling water for 15 minutes in a pot. To sterilize, the jars should be covered with about 3 cm of water.\n\nRemove from the pot and let them cool. The lids will become concave in the center, and they will make a 'pop' sound; this is essential for preservation."
    },
    {
      "text": "Unsterilized ketchup can be stored in the refrigerator for three weeks. You can also freeze it without sterilization."
    }
  ]
} as RecipeDTO;