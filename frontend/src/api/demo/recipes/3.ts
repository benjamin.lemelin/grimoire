import {RecipeDTO} from "../../dto";

export default {
  "id": 3,
  "name": "Lemon Mousse",
  "author": "John",
  "description": "Light and tangy, the lemon mousse will make you melt. Serve it in glass cups to beautifully conclude any feast.",
  "duration": {
    "seconds": 900
  },
  "yield": "8 portions",
  "difficulty": "easy",
  "isFavorite": true,
  "hasImage": true,
  "categories": [
    "Desserts"
  ],
  "ingredients": [
    {
      "name": "#Lemon flavor",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Unflavored gelatin",
      "quantity": "7 Grams",
      "comment": ""
    },
    {
      "name": "Cold water",
      "quantity": "1/4 Cup",
      "comment": ""
    },
    {
      "name": "Lemon zest",
      "quantity": "1 Tablespoon",
      "comment": ""
    },
    {
      "name": "Lemon juice",
      "quantity": "1/2 Cup",
      "comment": ""
    },
    {
      "name": "Sugar",
      "quantity": "1 Cup",
      "comment": ""
    },
    {
      "name": "#Dairy ingredients",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Plain yogurt",
      "quantity": "2/3 Cup",
      "comment": ""
    },
    {
      "name": "Whipping cream (35%)",
      "quantity": "1 Cup",
      "comment": ""
    }
  ],
  "instructions": [
    {
      "text": "#Mixing"
    },
    {
      "text": "In a saucepan, sprinkle cold water over the unflavored gelatin, let it sit for 1 minute. Using low heat, stir until the gelatin is dissolved. Add in the lemon zest, lemon juice, and half of the sugar. Add the yogurt."
    },
    {
      "text": "In a bowl, whip the cream with the remaining sugar. Fold it into the lemon mixture. Put it into individual glass cups."
    },
    {
      "text": "#Final touches"
    },
    {
      "text": "Cover and chill in the refrigerator for 2 hours. Decorate the cups with a sauce or with thin biscuits."
    }
  ]
} as RecipeDTO;