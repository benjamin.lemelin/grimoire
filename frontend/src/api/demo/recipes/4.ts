import {RecipeDTO} from "../../dto";

export default {
  "id": 4,
  "name": "Oatmeal and Raisin Cookies",
  "author": "John",
  "description": "Hard to beat, they are so delicious.",
  "duration": {
    "seconds": 1800
  },
  "yield": "24 cookies",
  "difficulty": "easy",
  "isFavorite": true,
  "hasImage": true,
  "categories": [
    "Cookies"
  ],
  "ingredients": [
    {
      "name": "#Cookies",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Softened butter",
      "quantity": "1 Cup",
      "comment": "220 Grams"
    },
    {
      "name": "Brown sugar",
      "quantity": "2 Cups",
      "comment": ""
    },
    {
      "name": "Eggs",
      "quantity": "2",
      "comment": ""
    },
    {
      "name": "Vanilla",
      "quantity": "1 Teaspoon",
      "comment": ""
    },
    {
      "name": "Flour",
      "quantity": "2 3/4 Cups",
      "comment": ""
    },
    {
      "name": "Baking soda",
      "quantity": "2 Teaspoons",
      "comment": ""
    },
    {
      "name": "Salt",
      "quantity": "1 Teaspoon",
      "comment": ""
    },
    {
      "name": "Cooking oats",
      "quantity": "2 Cups",
      "comment": ""
    },
    {
      "name": "#For flavor (choose one)",
      "quantity": "",
      "comment": ""
    },
    {
      "name": "Raisins",
      "quantity": "1 Cup",
      "comment": ""
    },
    {
      "name": "Chocolate chips",
      "quantity": "1 Cup",
      "comment": ""
    },
  ],
  "instructions": [
    {
      "text": "#Making the cookies"
    },
    {
      "text": "Cream the softened butter, add brown sugar, eggs, and vanilla."
    },
    {
      "text": "Sift together the dry ingredients and add to the first mixture. Add the oats, mix well."
    },
    {
      "text": "Drop onto a cookie sheet. Bake in a 360°F oven for 8 minutes."
    },
    {
      "text": "#Tips"
    },
    {
      "text": "To freeze the dough, roll it into a log."
    }
  ]
} as RecipeDTO;