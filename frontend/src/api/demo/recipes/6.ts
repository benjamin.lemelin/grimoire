import {RecipeDTO} from "../../dto";

export default {
  "id": 245,
  "name": "Smoked salmon spirals",
  "author": "Mary",
  "description": "",
  "duration": {
    "seconds": 0
  },
  "yield": "",
  "difficulty": "easy",
  "isFavorite": false,
  "hasImage": true,
  "categories": [
    "Appetizers"
  ],
  "ingredients": [
    {
      "name": "Thinly sliced smoked salmon",
      "quantity": "1/2 Pound",
      "comment": ""
    },
    {
      "name": "Unflavored gelatin",
      "quantity": "2 Teaspoons",
      "comment": ""
    },
    {
      "name": "White wine",
      "quantity": "1/4 Cup",
      "comment": ""
    },
    {
      "name": "Cream cheese",
      "quantity": "1/2 Pound",
      "comment": ""
    },
    {
      "name": "Drained crab meat",
      "quantity": "1",
      "comment": ""
    },
    {
      "name": "Finely chopped green onions",
      "quantity": "2",
      "comment": ""
    },
    {
      "name": "Mayonnaise",
      "quantity": "1/4 Cup",
      "comment": ""
    }
  ],
  "instructions": [
    {
      "text": "Sprinkle gelatin over wine and let it sit for five minutes. Heat over low heat to dissolve."
    },
    {
      "text": "Mix softened cream cheese, crab, onions, and mayonnaise. Add the wine."
    },
    {
      "text": "Spread the salmon on wax paper to form a 7x15-inch rectangle."
    },
    {
      "text": "Spread the filling on the salmon, leaving a 1/4-inch border."
    },
    {
      "text": "Roll it up like a jelly roll."
    },
    {
      "text": "Wrap it in plastic wrap and refrigerate for 4 hours."
    },
    {
      "text": "Slice thinly to serve. Why not add some color with a layer of fresh spinach."
    }
  ]
} as RecipeDTO;