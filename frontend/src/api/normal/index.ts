import {ApiError} from "../index";
import {Category, ModelFactory, Recipe, RecipeSummary, Role, User} from "../model";
import {DtoFactory, RecipeDTO, RecipeSummaryDTO, UserDTO} from "../dto";

async function call(call: Promise<Response>): Promise<Response> {
  // To delay apis and simulate a slower connection, add this line of code to it.
  // await new Promise(resolve => setTimeout(resolve, 2000));

  let response: Response;
  try {
    response = await call;
  } catch (e) {
    throw e;
  }

  if (response.status === 400) throw new Error(ApiError.BadRequest);
  else if (response.status === 401) throw new Error(ApiError.Unauthorized);
  else if (response.status === 403) throw new Error(ApiError.Forbidden);
  else if (response.status === 404) throw new Error(ApiError.NotFound);
  else if (response.status === 409) throw new Error(ApiError.Conflict);
  else if (!response.ok) throw new Error(await response.text());

  return response;
}

function get(url: string): Promise<Response> {
  return call(fetch(url));
}

function post(url: string, body?: BodyInit): Promise<Response> {
  return call(fetch(url, {method: "POST", body: body}));
}

function put(url: string, body?: BodyInit): Promise<Response> {
  return call(fetch(url, {method: "PUT", body: body}));
}

function del(url: string): Promise<Response> {
  return call(fetch(url, {method: "DELETE"}));
}

function params(params: [string, string][]): string {
  return `${new URLSearchParams(params)}`;
}

async function findAllRecipes(): Promise<RecipeSummary[]> {
  const response = await get(`/api/recipes`);
  const dto = await response.json() as RecipeSummaryDTO[];
  return dto.map(ModelFactory.createRecipeSummary);
}

async function findFavoritesRecipes(): Promise<RecipeSummary[]> {
  const response = await get(`/api/recipes/favorites`);
  const dto = await response.json() as RecipeSummaryDTO[];
  return dto.map(ModelFactory.createRecipeSummary);
}

async function findRecipesByCategory(category: Category): Promise<RecipeSummary[]> {
  const response = await get(`/api/categories/${category}`);
  const dto = await response.json() as RecipeSummaryDTO[];
  return dto.map(ModelFactory.createRecipeSummary);
}

async function findRecipesByKeywords(keywords: string): Promise<RecipeSummary[]> {
  const response = await get(`/api/recipes/search?${params([["q", keywords]])}`);
  const dto = await response.json() as RecipeSummaryDTO[];
  return dto.map(ModelFactory.createRecipeSummary);
}

async function findRecipeById(id: number): Promise<Recipe> {
  const response = await get(`/api/recipes/${id}`);
  const dto = await response.json() as RecipeDTO;
  return ModelFactory.createRecipe(dto);
}

async function findAllCategories(): Promise<Category[]> {
  const response = await get(`/api/categories`);
  return await response.json() as Category[];
}

async function addRecipe(recipe: Recipe): Promise<Recipe> {
  const response = await post(`/api/recipes`, DtoFactory.createNewRecipeDTO(recipe));
  const dto = await response.json() as RecipeDTO;
  return ModelFactory.createRecipe(dto);
}

async function updateRecipe(recipe: Recipe): Promise<Recipe> {
  const response = await put(`/api/recipes/${recipe.id()}`, DtoFactory.createUpdateRecipeDTO(recipe));
  const dto = await response.json() as RecipeDTO;
  return ModelFactory.createRecipe(dto);
}

async function setFavoriteRecipe(id: number): Promise<void> {
  await put(`/api/recipes/${id}/favorite`);
}

async function unsetFavoriteRecipe(id: number): Promise<void> {
  await del(`/api/recipes/${id}/favorite`);
}

async function deleteRecipe(id: number): Promise<void> {
  await del(`/api/recipes/${id}`);
}

async function login(username: string, password: string): Promise<User> {
  const response = await post(`/api/users/login`, DtoFactory.createLoginDTO(username, password));
  const dto = await response.json() as UserDTO;
  return ModelFactory.createUser(dto);
}

async function logout(): Promise<void> {
  await post(`/api/users/logout`);
}

async function findAllUsers(): Promise<User[]> {
  const response = await get(`/api/users`);
  const dto = await response.json() as UserDTO[];
  return dto.map(ModelFactory.createUser);
}

async function findUserByUsername(username: string): Promise<User> {
  const response = await get(`/api/users/${username}`);
  const dto = await response.json() as UserDTO;
  return ModelFactory.createUser(dto);
}

async function addUser(username: string, password: string, role: Role): Promise<void> {
  await post(`/api/users`, DtoFactory.createNewUserDTO(username, password, role));
}

async function updateUsername(oldUsername: string, username: string): Promise<void> {
  await put(`/api/users/${oldUsername}/username`, DtoFactory.createUpdateUsernameDTO(username));
}

async function updatePassword(username: string, oldPassword: string, newPassword: string): Promise<void> {
  await put(`/api/users/${username}/password`, DtoFactory.createUpdatePasswordDTO(oldPassword, newPassword));
}

async function updateProfile(username: string, firstName: string, lastName: string, email: string): Promise<void> {
  await put(`/api/users/${username}/profile`, DtoFactory.createUpdateUserProfileDTO(firstName, lastName, email));
}

async function updateRole(username: string, role: Role): Promise<void> {
  await put(`/api/users/${username}/role`, DtoFactory.createUpdateRoleDTO(role));
}

async function deleteUser(username: string): Promise<void> {
  await del(`/api/users/${username}`);
}

async function isSetupRequired(): Promise<boolean> {
  const response = await get(`/api/admin/setup`);
  return await response.json() as boolean;
}

async function createSetupUser(username: string, password: string): Promise<void> {
  await post(`/api/admin/setup`, DtoFactory.createSetupDTO(username, password));
}

async function importDatabase(database: File): Promise<void> {
  await put(`/api/admin/database`, database);
}

async function exportDatabase(): Promise<File> {
  const response = await get(`/api/admin/database`);
  return new File([await response.blob()], "Database.zip");
}

async function clearDatabase(): Promise<void> {
  await del(`/api/admin/database`);
}

export const API = {
  findAllRecipes,
  findFavoritesRecipes,
  findRecipesByCategory,
  findRecipesByKeywords,
  findRecipeById,
  findAllCategories,
  addRecipe,
  updateRecipe,
  setFavoriteRecipe,
  unsetFavoriteRecipe,
  deleteRecipe,
  login,
  logout,
  findAllUsers,
  findUserByUsername,
  addUser,
  updateUsername,
  updatePassword,
  updateProfile,
  updateRole,
  deleteUser,
  isSetupRequired,
  createSetupUser,
  importDatabase,
  exportDatabase,
  clearDatabase,
}