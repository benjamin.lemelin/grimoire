export enum ApiError {
  BadRequest = "BadRequest",
  Unauthorized = "Unauthorized",
  Forbidden = "Forbidden",
  NotFound = "NotFound",
  Conflict = "Conflict",
  DemoMode = "DemoMode",
}

export * from "./normal";
export * from "./model";