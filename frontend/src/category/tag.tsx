import {A} from "@solidjs/router";
import {Category} from "../api";
import {Icon} from "../icon";

import Style from "./tag.module.css";

type Props = {
  category: Category
}

export function CategoryTag(props: Props) {
  return (
    <A class={Style.category} href={`/categories/${props.category}`}>
      <Icon icon="sell"/>
      <span class={Style.name}>{props.category}</span>
    </A>
  )
}