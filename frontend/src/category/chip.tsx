import {A} from "@solidjs/router";
import {Category} from "../api";

type Props = {
  category: Category
}

export function CategoryChip(props: Props) {
  return (
    <A class="chip fade-in" href={`/categories/${props.category}`}>
      {props.category}
    </A>
  )
}