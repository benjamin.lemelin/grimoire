import {createMemo, createResource, Show} from "solid-js";
import {useParams} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {Suspense} from "../suspense";
import {RecipeList} from "../recipe";
import {CardListSkeleton, EmptyStateFallback} from "../fallback";

import Style from "./view-route.module.css";

type Params = {
  name: string
}

export function CategoryViewRoute() {
  const params = useParams<Params>();
  const [t] = useI18n();
  const category = createMemo(() => decodeURI(params.name));
  const [recipes] = createResource(() => category(), name => API.findRecipesByCategory(name));

  return (
    <section class={Style.category}>
      <h1>{category()}</h1>
      <Suspense resource={recipes} loading={<CardListSkeleton skeleton="recipe"/>}>{recipes =>
        // Fallback should never happen, but here it is just in case.
        <Show when={recipes.length > 0}
              fallback={<EmptyStateFallback>{t("category.view.empty", {category: category()})}</EmptyStateFallback>}>
          <RecipeList class={Style.recipes} recipes={recipes}/>
        </Show>
      }</Suspense>
    </section>
  )
}