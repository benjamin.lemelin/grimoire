import {A} from "@solidjs/router";
import {Category} from "../api";
import {ImageFallback} from "../image";

import Style from "./card.module.css";

type Props = {
  category: Category
}

export function CategoryCard(props: Props) {
  return (
    <A class="card fade-in" href={`/categories/${props.category}`} aria-label={props.category}>
      <article class={Style.category}>
        <ImageFallback class={Style.image}
                       src={`/api/categories/${props.category}/image`}
                       alt={props.category}/>
        <h2 class={`${Style.title} ellipsis`}>{props.category}</h2>
      </article>
    </A>
  );
}