import {createResource, Show} from "solid-js";
import {API} from "../api";
import {useI18n} from "../i18n";
import {Suspense} from "../suspense";
import {CardListSkeleton, EmptyStateFallback} from "../fallback";
import {CategoryList} from "./list";

import Style from "./list-route.module.css";

export function CategoryListRoute() {
  const [t] = useI18n();
  const [categories] = createResource(() => API.findAllCategories());

  return (
    <Suspense resource={categories}
              loading={<CardListSkeleton class={Style.categories} skeleton="category"/>}>{categories =>
      <section class={Style.categories} aria-label={t("category.list.title")} data-empty={categories.length === 0}>
        <Show when={categories.length > 0}
              fallback={<EmptyStateFallback>{t("category.list.notFound")}</EmptyStateFallback>}>
          <CategoryList categories={categories}/>
        </Show>
      </section>
    }</Suspense>
  );
}