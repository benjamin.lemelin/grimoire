import {For, Match, Switch} from "solid-js";
import {Category} from "../api";
import {CategoryCard} from "./card";
import {CategoryChip} from "./chip";

import Style from "./list.module.css";

type Props = {
  class?: string,
  categories: Category[],
  horizontal?: boolean
}

export function CategoryList(props: Props) {
  return (
    <ul class={`${Style.categories} ${props.class ?? ""}`} classList={{[Style.horizontal]: props.horizontal}}>
      <For each={props.categories}>{(category) =>
        <li>
          <Switch>
            <Match when={props.horizontal}>
              <CategoryChip category={category}/>
            </Match>
            <Match when={!props.horizontal}>
              <CategoryCard category={category}/>
            </Match>
          </Switch>
        </li>
      }</For>
    </ul>
  );
}