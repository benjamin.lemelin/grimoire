import {createEffect, createSignal, JSX, Show} from "solid-js";
import {useI18n} from "../i18n";
import {NotificationDrawer} from "../notification";
import {Icon} from "../icon";
import {useScrolled} from "../util/scroll";

import Style from "./style.module.css";

type Props = {
  class?: string,
  title?: string,
  icon?: string,
  countdown?: number,
  confirmText?: string,
  cancelText?: string,
  size?: Size,
  alignButtons?: ButtonAlignment,
  open?: boolean,
  children?: JSX.Element,
  onConfirm?: () => void,
  onCancel?: () => void,
}

type Size = "medium" | "large";
type ButtonAlignment = "end";

export function Dialog(props: Props) {
  let dialogElement: HTMLDialogElement;
  let contentElement: HTMLDivElement;
  const [t] = useI18n();
  const isHighlighted = useScrolled(() => contentElement);
  const [countdown, setCountdown] = createSignal(props.countdown ?? 0);
  const confirmText = () => countdown() > 0 ? countdown() : props.confirmText ?? t("dialog.ok");
  const cancelText = () => props.cancelText ?? t("dialog.cancel");

  createEffect(() => {
    if (props.open) dialogElement.showModal();
    else dialogElement.close();
  });

  createEffect(() => {
    if (props.open) {
      if (props.countdown) {
        setCountdown(props.countdown);
        const interval = setInterval(() => {
          if (setCountdown(countdown => countdown - 1) <= 0) {
            clearInterval(interval);
          }
        }, 1000);
      }
    }
  });

  function onConfirmButtonClick(event: Event) {
    event.preventDefault();
    props.onConfirm?.();
  }

  function onCancelButtonClick(event: Event) {
    event.preventDefault();
    props.onCancel?.();
  }

  function onBackdropClick(event: Event) {
    // The backdrop contains the dialog. We might receive an event from a button bubbling up here. Ignore those.
    if (event.target === dialogElement) {
      event.preventDefault();
      props.onCancel?.();
    }
  }

  function onDialogCancel(event: Event) {
    // There may be sub-dialogs inside this dialog. Make sure we cancel only this one.
    if (event.target === dialogElement) {
      event.preventDefault();
      props.onCancel?.();
    }
  }

  return (
    <dialog ref={el => dialogElement = el}
            class={Style.backdrop}
            onClick={onBackdropClick}
            onCancel={onDialogCancel}>
      <form class={`${Style.dialog} ${props.class ?? ""}`}
            classList={{[Style.medium]: props.size === "medium", [Style.large]: props.size === "large"}}
            method="dialog">
        <header class={Style.header}
                classList={{[Style.highlight]: isHighlighted(), [Style.icon]: !!props.icon}}>
          <Show when={props.icon} keyed>{icon =>
            <Icon icon={icon}/>
          }</Show>
          <h1 class={Style.title}>
            {props.title}
          </h1>
          <button class={`${Style.close} circle transparent`} onClick={onCancelButtonClick}>
            <Icon icon="close"/>
          </button>
          <button class={`${Style.confirm} primary pill text-center`} onClick={onConfirmButtonClick}>
            {confirmText()}
          </button>
        </header>
        <div ref={el => contentElement = el} class={Style.content}>
          {props.children}
        </div>
        <footer class={Style.footer}
                classList={{[Style.alignEnd]: props.alignButtons === "end"}}>
          <button class="primary pill text-center" onClick={onConfirmButtonClick} disabled={countdown() > 0}>
            {confirmText()}
          </button>
          <Show when={props.onCancel} keyed>
            <button class="pill text-center" onClick={onCancelButtonClick}>
              {cancelText()}
            </button>
          </Show>
        </footer>
      </form>
      <Show when={props.open} keyed>
        <NotificationDrawer/>
      </Show>
    </dialog>
  );
}