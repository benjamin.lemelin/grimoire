import {Show} from "solid-js";
import {A} from "@solidjs/router";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {Icon} from "../icon";
import {useScrolled} from "../util/scroll";

import Style from "./style.module.css";

export function Navbar() {
  const [t] = useI18n();
  const [isAuth] = useAuth();
  const isScrolled = useScrolled(window);

  return (
    <Show when={isAuth()} keyed>
      <nav class={Style.navbar} classList={{[Style.lower]: isScrolled()}}>
        <A class="button" href="/" end>
          <Icon class="fill" icon="home"/> {t("navbar.home")}
        </A>
        <A class="button" href="/search" end>
          <Icon icon="search"/> {t("navbar.search")}
        </A>
        <A class="button" href="/recipes" end>
          <Icon icon="ramen_dining"/> {t("navbar.recipes")}
        </A>
        <A class="button" href="/categories" end>
          <Icon icon="menu_book"/> {t("navbar.categories")}
        </A>
      </nav>
    </Show>
  );
}