import {JSX, onCleanup, onMount} from "solid-js";

import Style from "./style.module.css";

type DropdownProps = {
  class?: string,
  text?: JSX.Element,
  children?: JSX.Element,
  align?: "left" | "center" | "right",
  "aria-label"?: string
}

export function Dropdown(props: DropdownProps) {
  let detailsElement: HTMLDetailsElement;

  onMount(() => {
    // Once opened, clicking anywhere on the document should close it.
    document.addEventListener("click", onDocumentClick);

    // Remove this event once the dropdown is destroyed.
    onCleanup(() => {
      document.removeEventListener("click", onDocumentClick);
    });
  })

  function onKeyDown(event: KeyboardEvent) {
    if (event.key === "Escape") {
      detailsElement.open = false;
    }
  }


  function onDocumentClick(event: Event) {
    // Skip if dropdown is not open.
    if (!detailsElement.open) return;

    let close = true;

    // When inside the dropdown, only close when clicking on a hyperlink.
    const target = event.target;
    if (target instanceof Node && detailsElement.contains(target)) {
      // HTMLAnchorElement is the <a> tag.
      if (!(target instanceof HTMLAnchorElement)) {
        close = false;
      }
    }

    if (close) {
      // The document click event is fired before the toggle event.
      // If we close the dropdown right away, the toggle event will reopen the dropdown.
      setTimeout(() => detailsElement.open = false);
    }
  }

  return (
    <details ref={el => detailsElement = el}
             class={Style.dropdown}
             onKeyDown={onKeyDown}>
      <summary class={`${Style.button} button ${props.class ?? ""}`} aria-label={props["aria-label"]}>
        {props.text}
      </summary>
      <menu class={Style.content}
            classList={{
              [Style.alignLeft]: !props.align || props.align === "left",
              [Style.alignCenter]: props.align === "center",
              [Style.alignRight]: props.align === "right",
            }}>
        {props.children}
      </menu>
    </details>
  );
}

type DropdownItemProps = {
  children?: JSX.Element
}

export function DropdownItem(props: DropdownItemProps) {
  return (
    <li>{props.children}</li>
  );
}

export function DropdownSeparator() {
  return (
    <li aria-hidden="true">
      <hr/>
    </li>
  );
}