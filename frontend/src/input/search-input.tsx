import {JSX} from "solid-js";

type Props = {
  class?: string,
  name?: string,
  value?: string,
  placeholder?: string,
  onInput?: (value: string) => void
}

export function SearchInput(props: Props) {
  const onInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    if (props.onInput) {
      props.onInput(event.currentTarget.value);
    }
  };

  return (
    <input class={`${props.class ?? ""} icon-search`}
           type="search"
           placeholder={props.placeholder}
           name={props.name}
           value={props.value}
           onInput={onInput}/>
  );
}