import {createSignal, For, JSX} from "solid-js";
import {Duration} from "../api";
import {useI18n} from "../i18n";
import {Dialog} from "../dialog";

import Style from "./time-input.module.css";

type Props = {
  class?: string,
  value?: Duration,
  onInput?: (value: Duration) => void
}

export function TimeInput(props: Props) {
  const [t] = useI18n();
  const [hours, setHours] = createSignal(0);
  const [minutes, setMinutes] = createSignal(0);
  const [isDialogVisible, setDialogVisible] = createSignal(false);

  function showDialog() {
    setHours(Math.floor((props.value?.seconds ?? 0) / 3600));
    setMinutes(Math.floor((props.value?.seconds ?? 0) % 3600 / 60));

    setDialogVisible(true);
  }

  function closeDialog() {
    setDialogVisible(false);
  }

  const onClick = () => {
    showDialog();
  };

  const onKeyDown = (event: KeyboardEvent) => {
    if (event.key === "Enter") {
      event.preventDefault();
      showDialog();
    }
  };

  const onDialogConfirm = () => {
    if (props.onInput) {
      props.onInput({seconds: hours() * 3600 + minutes() * 60});
    }
    closeDialog();
  };

  const onDialogCancel = () => {
    closeDialog();
  };

  const onHourInput: JSX.EventHandler<HTMLSelectElement, InputEvent> = (event) => {
    setHours(Number(event.currentTarget.value));
  };

  const onMinuteInput: JSX.EventHandler<HTMLSelectElement, InputEvent> = (event) => {
    setMinutes(Number(event.currentTarget.value));
  };

  return (
    <>
      <input class={`${props.class ?? ""} icon-time`}
             type="text"
             value={t("duration", props.value?.seconds ?? 0)}
             onClick={onClick}
             onkeydown={onKeyDown}
             readonly/>
      <Dialog title={t("dialog.time.title")}
              open={isDialogVisible()}
              onConfirm={onDialogConfirm}
              onCancel={onDialogCancel}>
        <div class={Style.dialogContent}>
          <select value={hours()} onInput={onHourInput} autofocus>
            <For each={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}>{(hours) =>
              <option value={hours}>{String(hours).padStart(2, "0")}</option>
            }</For>
          </select>
          :
          <select value={minutes()} onInput={onMinuteInput}>
            <For each={[0, 5, 10, 15, 30, 45, 60]}>{(minutes) =>
              <option value={minutes}>{String(minutes).padStart(2, "0")}</option>
            }</For>
          </select>
        </div>
      </Dialog>
    </>
  );
}