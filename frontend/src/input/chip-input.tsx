import {createSignal, For, JSX} from "solid-js";
import {push, remove} from "../util/array";
import {Icon} from "../icon";

import Style from "./chip-input.module.css";

type Props = {
  class?: string,
  value?: string[],
  placeholder?: string,
  onAdd?: (value: string) => void,
  onRemove?: (index: number) => void
}

export function ChipInput(props: Props) {
  const [values, setValues] = createSignal(props.value ?? []);
  const placeholder = () => {
    const text = props.placeholder ?? "";
    return values().length === 0 ? text : "";
  };

  function addValue(value: string) {
    setValues(values => push(values, value));
    if (props.onAdd) props.onAdd(value);
  }

  function removeValue(index: number) {
    setValues(values => remove(values, index));
    if (props.onRemove) props.onRemove(index);
  }

  const onKeyPress: JSX.EventHandler<HTMLInputElement, KeyboardEvent> = (event) => {
    if (event.key === "Enter" || event.key === ";") {
      event.preventDefault();

      const input = event.currentTarget;
      const value = input.value.trim();

      if (value) {
        addValue(value);
        input.value = "";
      }
    }
  };

  const onKeyDown: JSX.EventHandler<HTMLInputElement, KeyboardEvent> = (event) => {
    if (event.key === "Backspace") {
      const input = event.currentTarget;

      if (input.selectionStart === 0 && input.selectionEnd === 0) {
        const valuesLength = values().length;
        if (valuesLength > 0) removeValue(valuesLength - 1);
      }
    }
  };

  const onFocusOut: JSX.EventHandler<HTMLInputElement, FocusEvent> = (event) => {
    const input = event.currentTarget;
    const value = input.value.trim();

    if (value) {
      addValue(value);
      input.value = "";
    }
  };

  const onDeleteClick = (index: number) => {
    removeValue(index);
  };

  return (
    <div class={`${Style.chipInput} ${props.class ?? ""} input icon-tag`}>
      <For each={values()}>{(value, i) =>
        <span class="chip">
          {value}
          <button class="transparent" onClick={() => onDeleteClick(i())}>
            <Icon icon="cancel"/>
          </button>
        </span>
      }</For>
      <input class="transparent"
             type="text"
             placeholder={placeholder()}
             onKeyPress={onKeyPress}
             onKeyDown={onKeyDown}
             onfocusout={onFocusOut}/>
    </div>
  );
}