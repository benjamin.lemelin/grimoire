import Style from "./image-input.module.css";
import BlankImg from "/img/blank.svg";

type Props = {
  class?: string,
  value?: File | string,
  alt?: string,
  onInput?: (image: File) => void
}

export function ImageInput(props: Props) {
  let imageElement: HTMLImageElement;
  let inputElement: HTMLInputElement;
  const src = () => {
    if (!props.value) return BlankImg;
    if (typeof props.value === "string") return props.value;
    return URL.createObjectURL(props.value);
  };

  function onClick(event: Event) {
    event.preventDefault(); // In case this is inside a form, prevent form submit.

    if (document.activeElement === event.currentTarget) {
      inputElement.click();
    }
  }

  function onError() {
    imageElement.src = BlankImg;
  }

  function onInput() {
    if (props.onInput) {
      const file = inputElement.files?.item(0);
      if (file) props.onInput(file);
    }
  }

  return (
    <div class={`${Style.imageInput} ${props.class ?? ""}`}>
      <button class="image" onClick={onClick}>
        <img ref={el => imageElement = el}
             src={src()}
             alt={props.alt}
             onError={onError}/>
      </button>
      <input ref={el => inputElement = el}
             type="file"
             accept="image/jpeg,image/png,image/webp"
             onInput={onInput}/>
    </div>
  );
}