import {JSX, For, createSignal, createRenderEffect, onMount, onCleanup, Accessor} from "solid-js";
import Sortable from "sortablejs";
import {move} from "../util/array";
import {ClassList} from "../util/styling";

import Style from "./style.module.css";

type ReorderableWrapper<T> = {
  value: T
}

type ReorderableListProps<T> = {
  items: T[]
  children: (item: T, index: Accessor<number>) => JSX.Element,
  onMove?: (oldIndex: number, newIndex: number) => void
}

export function ReorderableList<T>(props: ReorderableListProps<T>) {
  const [items, setItems] = createSignal<ReorderableWrapper<T>[]>([]);
  let listElement: HTMLUListElement;

  const updateItems = () => {
    setItems(items => {
      // Remove superfluous items.
      items = items.slice(0, props.items.length);

      // Update items.
      items = items.map((item, index) => {
        const other = props.items[index];
        return item.value === other ? item : {value: other};
      });

      // Add new items.
      return items.concat(props.items.slice(items.length).map(item => ({value: item})));
    });
  };

  const moveItems = (oldIndex: number, newIndex: number) => {
    // Undo what SortableJs did to the DOM, as it will interfere with what SolidJs expects.
    const oldElement = listElement.children[oldIndex];
    const newElement = listElement.children[newIndex];

    if (oldIndex < newIndex) oldElement.before(newElement);
    else if (newIndex < oldIndex) oldElement.after(newElement);

    // Then, make the real update using Solid.
    setItems(items => move(items, oldIndex, newIndex));

    // Finally, notify that the move occurred.
    if (props.onMove) props.onMove(oldIndex, newIndex);
  };

  // Update items when receiving props.
  createRenderEffect(() => {
    updateItems();
  });

  // Initialize SortableJs.
  onMount(() => {
    const sortable = Sortable.create(listElement, {
      animation: 150,
      easing: "ease",
      draggable: `.${Style.item}`,
      handle: `.${Style.handle}`,
      forceFallback: true,
      ghostClass: Style.ghost,
      onEnd: (event) => {
        const oldIndex = event.oldIndex;
        const newIndex = event.newIndex;
        if (oldIndex !== undefined && newIndex !== undefined && oldIndex !== newIndex) {
          moveItems(oldIndex, newIndex);
        }
      }
    });

    onCleanup(() => {
      sortable.destroy();
    });
  });

  return (
    <ul ref={el => listElement = el} class={Style.list}>
      <For each={items()}>{(item, i) => props.children(item.value, i)}</For>
    </ul>
  )
}

type ReorderableItemProps = {
  class?: string,
  classList?: ClassList,
  children?: JSX.Element
}

export function ReorderableItem(props: ReorderableItemProps) {
  return (
    <li class={`${Style.item} ${props.class ?? ""}`} classList={props.classList}>
      {props.children}
    </li>
  )
}

type ReorderableHandleProps = {
  class?: string,
}

export function ReorderableHandle(props: ReorderableHandleProps) {
  return (
    <span class={`${Style.handle} ${props.class ?? ""} icon`}>drag_handle</span>
  );
}