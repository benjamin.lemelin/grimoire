/* @refresh reload */

import {render} from "solid-js/web";
import {Router} from "@solidjs/router";
import "./index.css"; /* Import base Css before any other Css file to facilitate style redefinitions. */
import {App} from "./app";

render(
  () => <Router base={import.meta.env.BASE_URL}><App/></Router>,
  document.getElementById('root') as HTMLElement
);