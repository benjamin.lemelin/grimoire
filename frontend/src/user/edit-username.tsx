import {createSignal, JSX, Show} from "solid-js";
import {API, User} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Spinner} from "../spinner";

import Style from "./edit.module.css";

type Props = {
  user: User,
  onUpdated?: (username: string) => void
}

export function UserEditUsername(props: Props) {
  const [t] = useI18n();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [username, setUsername] = createSignal(props.user.username());
  const isUsernameValid = () => !!username().trim();
  const [validate, setValidate] = createSignal(false);
  const [isSending, setSending] = createSignal(false);
  const isSendDisabled = () => props.user.username() === username();

  const onUsernameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setUsername(event.currentTarget.value);
  };

  async function onSubmit(event: SubmitEvent) {
    event.preventDefault();

    setValidate(true);
    if (!isUsernameValid()) return;
    setValidate(false);

    setSending(true);
    try {
      await API.updateUsername(props.user.username(), username());
      notify.success(t("settings.account.username.usernameUpdated"));
      props.onUpdated?.(username())
    } catch (e) {
      handleError(e);
    }
    setSending(false);
  }

  return (
    <form class={Style.user} onSubmit={onSubmit} novalidate>
      <div class={Style.field}>
        <label for="username">{t("settings.account.username.username")} :</label>
        <input id="username"
               class="bordered"
               classList={{"invalid": validate() && !isUsernameValid()}}
               type="text"
               value={username()}
               onInput={onUsernameInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isUsernameValid()}}>
          {t("settings.account.username.usernameEmptyError")}
        </div>
      </div>
      <p class={Style.info}>
        {t("settings.account.username.usernameAlreadyInUseWarning")}
      </p>
      <button disabled={isSendDisabled()}>
        <Show when={!isSending()} fallback={<Spinner small/>} keyed>
          {t("settings.account.username.confirm")}
        </Show>
      </button>
    </form>
  );
}