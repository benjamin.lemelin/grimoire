import {createSignal, JSX, Show} from "solid-js";
import {API, Role, User} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Spinner} from "../spinner";

import Style from "./edit.module.css";

type Props = {
  user: User,
  onUpdated?: (role : Role) => void
}

export function UserEditRole(props: Props) {
  const [t] = useI18n();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [role, setRole] = createSignal(props.user.role());
  const [isSending, setSending] = createSignal(false);

  const onRoleInput: JSX.EventHandler<HTMLSelectElement, InputEvent> = (event) => {
    setRole(event.currentTarget.value as Role);
  };

  async function onSubmit(event: SubmitEvent) {
    event.preventDefault();

    setSending(true);
    try {
      await API.updateRole(props.user.username(), role());
      notify.success(t("settings.account.role.roleUpdated"));
      props.onUpdated?.(role())
    } catch (e) {
      handleError(e);
    }
    setSending(false);
  }

  return (
    <form class={Style.user} onSubmit={onSubmit} novalidate>
      <div class={Style.field}>
        <label for="role">{t("settings.account.role.title")} :</label>
        <select id="role"
                class="bordered"
                value={role()}
                onInput={onRoleInput}>
          <option value="admin">{t("role", "admin")}</option>
          <option value="user">{t("role", "user")}</option>
          <option value="guest">{t("role", "guest")}</option>
        </select>
      </div>
      <p class={Style.info}>
        {t("settings.account.role.roleExplanation")}
      </p>
      <button>
        <Show when={!isSending()} fallback={<Spinner small/>} keyed>
          {t("settings.account.role.confirm")}
        </Show>
      </button>
    </form>
  );
}