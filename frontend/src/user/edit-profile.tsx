import {createMemo, createSignal, JSX, Show} from "solid-js";
import {API, User} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Spinner} from "../spinner";

import Style from "./edit.module.css";

const EMAIL_VALIDATION_REGEX = /^(.{0}|(.+@.+\..+))$/;

type Props = {
  user: User,
  onUpdated?: (firstName: string, lastName: string, email: string) => void
}

export function UserEditProfile(props: Props) {
  const [t] = useI18n();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [firstName, setFirstName] = createSignal(props.user.firstName());
  const [lastName, setLastName] = createSignal(props.user.lastName());
  const [email, setEmail] = createSignal(props.user.email());
  const isEmailValid = createMemo(() => EMAIL_VALIDATION_REGEX.test(email()));
  const [validate, setValidate] = createSignal(false);
  const [isSending, setSending] = createSignal(false);

  const onFirstNameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setFirstName(event.currentTarget.value);
  };

  const onLastNameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setLastName(event.currentTarget.value);
  };

  const onEmailInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setEmail(event.currentTarget.value);
  };

  async function onSubmit(event: SubmitEvent) {
    event.preventDefault();

    setValidate(true);
    if (!isEmailValid()) return;
    setValidate(false);

    setSending(true);
    try {
      await API.updateProfile(props.user.username(), firstName(), lastName(), email());
      notify.success(t("settings.account.profile.profileUpdated"));
      props.onUpdated?.(firstName(), lastName(), email());
    } catch (e) {
      handleError(e);
    }
    setSending(false);
  }

  return (
    <form class={Style.user} onSubmit={onSubmit} novalidate>
      <div class={Style.field}>
        <label for="firstName">{t("settings.account.profile.firstName")} :</label>
        <input id="firstName"
               class="bordered"
               type="text"
               value={firstName()}
               onInput={onFirstNameInput}/>
      </div>
      <div class={Style.field}>
        <label for="lastName">{t("settings.account.profile.lastName")} :</label>
        <input id="lastName"
               class="bordered"
               type="text"
               value={lastName()}
               onInput={onLastNameInput}/>
      </div>
      <div class={Style.field}>
        <label for="email">{t("settings.account.profile.email")} :</label>
        <input id="email"
               class="bordered"
               classList={{"invalid": validate() && !isEmailValid()}}
               type="email"
               value={email()}
               onInput={onEmailInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isEmailValid()}}>
          {t("settings.account.profile.invalidEmailError")}
        </div>
      </div>
      <button>
        <Show when={!isSending()} fallback={<Spinner small/>} keyed>
          {t("settings.account.profile.confirm")}
        </Show>
      </button>
    </form>
  );
}