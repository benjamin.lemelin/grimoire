import {createMemo} from "solid-js";
import {User} from "../api";
import {Icon} from "../icon";

import Style from "./summary.module.css";
import {useI18n} from "../i18n";

type Props = {
  class?: string,
  user: User,
  self?: boolean
}

export function UserSummary(props: Props) {
  const [t] = useI18n();

  const initials = createMemo(() => {
    const firstName = props.user.firstName();
    const lastName = props.user.lastName();

    if (firstName && lastName) {
      return `${firstName.at(0)}${lastName.at(0)}`.toUpperCase();
    }

    return undefined;
  });

  const name = createMemo(() => {
    let value = props.user.username();
    const firstName = props.user.firstName();
    const lastName = props.user.lastName();

    if (props.self) return t("account.defaultName");

    if (firstName) {
      value = firstName;
      if (lastName) value = `${value} ${lastName}`;
    }

    return value;
  });

  const email = createMemo(() => {
    const email = props.user.email();
    const username = props.user.username();

    if (name() === username) return "";
    if (email) return email;

    return username;
  })

  return (
    <div class={`${Style.summary} ${props.class ?? ""}`} aria-label={t("account.title")}>
      <span class={Style.initials} aria-hidden="true">
        {initials() ?? <Icon icon="person"/>}
      </span>
      <span class={`${Style.name} ellipsis`}>
        {name()}
      </span>
      <span class={`${Style.email} ellipsis muted`}>
        {email()}
      </span>
    </div>
  );
}