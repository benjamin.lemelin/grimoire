import {createSignal, JSX, Show} from "solid-js";
import {MIN_PASSWORD_LEN} from "../config";
import {ApiError, API, User} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Spinner} from "../spinner";

import Style from "./edit.module.css";

type Props = {
  user: User,
  onUpdated?: () => void
}

export function UserEditPassword(props: Props) {
  const [t] = useI18n();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [currentPassword, setCurrentPassword] = createSignal("");
  const isCurrentPasswordValid = () => !!currentPassword();
  const [newPassword, setNewPassword] = createSignal("");
  const isNewPasswordValid = () => currentPassword().length >= MIN_PASSWORD_LEN;
  const [newPasswordConfirmation, setNewPasswordConfirmation] = createSignal("");
  const isNewPasswordConfirmationValid = () => isNewPasswordValid() && newPassword() === newPasswordConfirmation();
  const [validate, setValidate] = createSignal(false);
  const [isSending, setSending] = createSignal(false);

  const onCurrentPasswordInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setCurrentPassword(event.currentTarget.value);
  };

  const onNewPasswordInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setNewPassword(event.currentTarget.value);
  };

  const onNewPasswordConfirmationInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setNewPasswordConfirmation(event.currentTarget.value);
  };

  async function onSubmit(event: SubmitEvent) {
    event.preventDefault();

    setValidate(true);
    if (!isCurrentPasswordValid()) return;
    if (!isNewPasswordValid()) return;
    if (!isNewPasswordConfirmationValid()) return;
    setValidate(false);

    setSending(true);
    try {
      await API.updatePassword(props.user.username(), currentPassword(), newPassword());
      notify.success("Mot de passe modifié.");
      props.onUpdated?.();
    } catch (e) {
      if (e instanceof Error && e.message === ApiError.BadRequest) {
        notify.error("Mot de passe incorrect.");
      } else {
        handleError(e);
      }
    }
    setSending(false);
  }

  return (
    <form class={Style.user} onSubmit={onSubmit} novalidate>
      <div class={Style.field}>
        <label for="currentPassword">{t("settings.password.currentPassword")} :</label>
        <input id="currentPassword"
               class="bordered"
               classList={{"invalid": validate() && !isCurrentPasswordValid()}}
               type="password"
               value={currentPassword()}
               onInput={onCurrentPasswordInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isCurrentPasswordValid()}}>
          {t("settings.password.currentPasswordEmptyError")}
        </div>
      </div>
      <div class={Style.field}>
        <label for="newPassword">{t("settings.password.newPassword")} :</label>
        <input id="newPassword"
               class="bordered"
               classList={{"invalid": validate() && !isNewPasswordValid()}}
               type="password"
               value={newPassword()}
               onInput={onNewPasswordInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isNewPasswordValid()}}>
          {t("settings.password.newPasswordLengthError", {length: MIN_PASSWORD_LEN})}
        </div>
      </div>
      <div class={Style.field}>
        <label for="newPasswordConfirmation">{t("settings.password.newPasswordConfirmation")} :</label>
        <input id="newPasswordConfirmation"
               class="bordered"
               classList={{"invalid": validate() && !isNewPasswordConfirmationValid()}}
               type="password"
               value={newPasswordConfirmation()}
               onInput={onNewPasswordConfirmationInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isNewPasswordConfirmationValid()}}>
          {t("settings.password.newPasswordConfirmationError")}
        </div>
      </div>
      <button>
        <Show when={!isSending()} fallback={<Spinner small/>} keyed>
          {t("settings.password.confirm")}
        </Show>
      </button>
    </form>
  );
}