import {createResource, createSignal} from "solid-js";
import {createSingletonRoot} from "@solid-primitives/rootless";
import * as i18n from "@solid-primitives/i18n";
import en from "./dict/en";

export * from "./locale-toggle-button";

export type Locale = "en" | "fr";

export type Dictionary = typeof en;
type Translations = i18n.Flatten<Dictionary>;
type Translator = i18n.Translator<Translations>;
type LocaleAccessor = () => Locale;
type LocaleSetter = (theme: Locale | ((prev: Locale) => Locale)) => void;
type I18NSignal = [translator: Translator, getLocale: LocaleAccessor, setLocale: LocaleSetter];

export const useI18n: () => I18NSignal = createSingletonRoot<I18NSignal>(() => {
  const [locale, setLocale] = createSignal(loadLocale());
  const [dictionary] = createResource(locale, fetchDictionary, {initialValue: i18n.flatten(en)});

  return [
    i18n.translator(dictionary, i18n.resolveTemplate),
    locale,
    (locale) => saveLocale(setLocale(locale))
  ];
});

const LOCALE_STORAGE_KEY = "locale";

function loadLocale(): Locale {
  return (localStorage.getItem(LOCALE_STORAGE_KEY) ?? preferredLocale()) as Locale;
}

function saveLocale(locale: Locale) {
  localStorage.setItem(LOCALE_STORAGE_KEY, locale);
}

function preferredLocale(): Locale {
  const language = navigator.language;

  if (language.startsWith("fr")) return "fr"
  else return "en";
}

async function fetchDictionary(locale: Locale): Promise<Translations> {
  // English dictionary is always loaded as the default language.
  let dictionary: Dictionary;
  if (locale === "en")
    dictionary = en;
  else
    dictionary = (await import(`./dict/${locale}.tsx`)).default;

  return i18n.flatten(dictionary);
}