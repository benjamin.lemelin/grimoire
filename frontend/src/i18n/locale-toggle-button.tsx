import {useI18n} from "./index";
import {Icon} from "../icon";

import Style from "./locale-toggle-button.module.css";

type Props = {
  class?: string
}

export function LocaleToggleButton(props: Props) {
  const [t, locale, setLocale] = useI18n();

  return (
    <div class={`${Style.localeToggle} ${props.class ?? ""}`}>
      <Icon icon="language"/>
      <fieldset aria-label={t("headerBar.language")}>
        <label class={Style.first} classList={{[Style.active]: locale() === "en"}} for="locale-en">
          En
          <input id="locale-en"
                 type="radio"
                 name="locale"
                 value="en"
                 onClick={() => setLocale("en")}
                 checked={locale() === "en"}/>
        </label>

        <label class={Style.last} classList={{[Style.active]: locale() === "fr"}} for="locale-fr">
          Fr
          <input id="locale-fr"
                 type="radio"
                 name="locale"
                 value="fr"
                 onClick={() => setLocale("fr")}
                 checked={locale() === "fr"}/>
        </label>
      </fieldset>
    </div>
  );
}