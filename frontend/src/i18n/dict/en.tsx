import {Difficulty, Role} from "../../api";
import {isNumeric, toDuration} from "./helpers";

const dict = {
  grimoire: "Grimoire",
  headerBar: {
    home: "Home",
    search: "Search",
    createRecipe: "Create a recipe",
    userAccount: "User account",
    settings: "Settings",
    lightMode: "Light theme",
    darkMode: "Dark theme",
    language: "Language",
    logout: "Log out",
    logoutError: "Unexpected error during logout.",
  },
  navbar: {
    home: "Home",
    search: "Search",
    recipes: "Recipes",
    categories: "Categories",
  },
  home: {
    recipes: "Recipes",
    categories: "Categories",
    notFound: "No recipes found",
  },
  search: {
    title: "Search",
    empty: "No recipes",
    notFound: "No recipes found for \"{{ query }}\""
  },
  category: {
    list: {
      title: "Categories",
      notFound: "No categories found",
    },
    view: {
      empty: "No recipes found for \"{{ category }}\"",
    },
  },
  recipe: {
    list: {
      title: "Recipes",
      empty: "No recipes found",
    },
    view: {
      ingredient: {
        title: "Ingredients",
        content: (name: string, quantity: string, comment: string) => {
          const isQuantityNumeric = isNumeric(quantity);

          let content = name;
          if (quantity.length > 0) {
            if (isQuantityNumeric) {
              content = `${quantity} ${name}`;
            } else {
              content = `${quantity} of ${name}`;
            }
          }
          if (comment.length > 0) {
            content = `${content} - ${comment}`;
          }

          return content;
        }
      },
      instruction: {
        title: "Instructions",
      },
      options: {
        title: "Options",
        addToFavorites: "Add to favorites",
        addingToFavorites: "Adding recipe to favorites.",
        addedToFavorites: "Recipe added to favorites.",
        removeFromFavorites: "Remove from favorites",
        removingFromFavorites: "Removing recipe from favorites.",
        removedFromFavorites: "Recipe removed from favorites.",
        printRecipe: "Print",
        editRecipe: "Edit recipe",
        deleteRecipe: "Delete recipe",
        deleteDialogTitle: "Delete recipe ?",
        deleteDialogText: "Are you sure you want to delete this recipe ?",
        deleteDialogConfirm: "Delete",
        deletingRecipe: "Deleting recipe.",
        deletedRecipe: "Recipe deleted.",
      },
    },
    new: {
      title: "Create recipe",
      confirm: "Save",
      recipeCreating: "Creating recipe.",
      recipeCreated: "Recipe created.",
      recipeNameEmptyError: "Please give your recipe a name.",
    },
    edit: {
      title: "Edit recipe",
      recipe: {
        title: "General informations",
        name: "Recipe name",
        author: "Author",
        description: "Description",
        yield: "Yield",
        categories: "Categories",
      },
      ingredient: {
        title: "Ingredients",
        name: "Name",
        quantity: "Quantity",
        comment: "Comment",
        add: "Add ingredient",
      },
      instruction: {
        title: "Instructions",
        add: "Add instruction",
      },
      confirm: "Save",
      recipeUpdating: "Saving recipe.",
      recipeUpdated: "Recipe saved.",
      recipeNameEmptyError: "Please give your recipe a name.",
    },
  },
  about: {
    title: "About Grimoire",
    license: "License",
    description: `Grimoire is a recipe management application that allows users to create and store their own unique recipes. Users can
                   easily write out their favorite recipes, including ingredients and preparation steps, and view them all in one
                   convenient location. Whether you're a seasoned chef or a beginner cook, Grimoire is the perfect tool for organizing and
                   keeping track of all your favorite recipes.`,
  },
  login: {
    title: "Log in to your account",
    subtitle: "Using your Grimoire account",
    username: "Username",
    password: "Password",
    login: "Log in",
    usernameEmptyError: "Please input your username.",
    passwordEmptyError: "Please input your password.",
    wrongCredentialsError: "Invalid credentials. Username or password is incorrect.",
  },
  setup: {
    title: "Initial setup",
    subtitle: "Creation of the admin account",
    confirm: "Create the administrator account",
    adminUserCreated: "Administrator account created. Please log in.",
  },
  account : {
    title : "User account",
    defaultName : "My account",
  },
  settings: {
    account: {
      title: (username: string, isOwnAccount: boolean) => {
        return isOwnAccount ? "My account" : `${username}'s account`
      },
      profile: {
        title: "Profile",
        confirm : "Save",
        firstName: "First name",
        lastName: "Last name",
        email : "Email address",
        invalidEmailError : "Email address is invalid.",
        profileUpdated: "Profile updated.",
      },
      username: {
        title: "Username",
        confirm: "Save",
        username: "Username",
        usernameEmptyError: "Username must be at least 1 letter long.",
        usernameAlreadyInUseWarning: "The new username must not already be used by another person.",
        usernameUpdated : "Username changed.",
      },
      role: {
        title: "Role",
        confirm: "Save",
        roleExplanation: "Admins have complete control over the application. Users can create and edit recipes. Guests can only view them.",
        editingOwnAccountWarning: () => <>
          This operation will change the role of your <strong>own account</strong>.
        </>,
        roleUpdated : "User role changed.",
      },
    },
    password: {
      title: "Change password",
      confirm: "Save",
      currentPassword: "Current password",
      newPassword: "New password",
      newPasswordConfirmation: "Password confirmation",
      currentPasswordEmptyError: "Please input your current password.",
      newPasswordLengthError: "New password must be at least {{ length }} characters long.",
      newPasswordConfirmationError: "Password and password confirmation do not match.",
      passwordChanged: "Password changed.",
      wrongPasswordError: "Incorrect password.",
    },
    user: {
      title: "Users",
      add: "Create new user",
      new: {
        title: "New user",
        confirm: "Create",
        username: "Username",
        password: "Password",
        passwordConfirmation: "Password confirmation",
        role: "Role",
        roleExplanation: "Admins have complete control over the application. Users can create and edit recipes. Guests can only view them.",
        usernameEmptyError: "Username must be at least 1 letter long.",
        passwordLengthError: "Password must be at least {{ length }} characters long.",
        passwordConfirmationError: "Password and password confirmation do not match.",
        creatingUser: "Creating user.",
        createdUser: "User created.",
      },
      delete: {
        dialogTitle: "Delete user ?",
        dialogConfirm: "Delete",
        dialogContent: (username: string) => <>
          This operation will delete {username}'s account <strong>irreversibly</strong>.
        </>,
        deletingUser: "Deleting user.",
        deletedUser: "User deleted."
      },
    },
    database: {
      title: "Database",
      export: {
        exportDatabase: "Create database backup",
        exportingDatabase: "Export in progress.",
        exportedDatabase: "Database exported.",
      },
      restore: {
        restoreDatabase: "Restore database backup",
        dialogTitle: "Restore the database?",
        dialogConfirm: "Choose a backup",
        dialogContent: <>
          This will <strong>replace</strong> all existing data.
        </>,
        confirmDialogTitle: "Confirmation required",
        confirmDialogConfirm: "Confirm restoration",
        confirmDialogContent: () => <>
          <p>
            You are about to <strong>replace</strong> all existing recipes with those from the
            provided archive. <strong>This operation is irreversible.</strong>
          </p>
          <p>
            Are you <strong>absolutely sure</strong> you want to continue ?
          </p>
        </>,
        restoringDatabase: "Restoration in progress.",
        restoredDatabase: "Database restored.",
      },
      clear: {
        clearDatabase: "Clear database",
        dialogTitle: "Delete everything ?",
        dialogConfirm: "Delete all",
        dialogContent: () => <>
          This operation will <strong>delete</strong> all recipes inside the application.
          Are you sure you want to continue ?
        </>,
        confirmDialogTitle: "Confirmation required",
        confirmDialogConfirm: "Confirm deletion",
        confirmDialogContent: () => <>
          <p>
            You are about to <strong>delete</strong> all your recipes. <strong>This operation is irreversible.</strong>
          </p>
          <p>
            Are-you <strong>absolutely sure</strong> you want to continue ?
          </p>
        </>,
        clearingDatabase: "Database deletion in progress.",
        clearedDatabase: "Database emptied.",
      },
    },
    about : {
      title : "About"
    },
  },
  dialog: {
    ok: "Ok",
    cancel: "Cancel",
    time: {
      title: "Select duration",
    },
  },
  duration: (seconds: number): string => {
    const {hours, minutes} = toDuration(seconds);

    let text = "";
    if (hours > 0) text += `${hours}h `;
    if (minutes > 0) text += `${minutes} min`;
    if (text.length === 0) text = "--";

    return text;
  },
  difficulty: (difficulty: Difficulty): string => {
    switch (difficulty) {
      case "normal":
        return "Normal";
      case "hard":
        return "Hard";
      default:
        return "Easy";
    }
  },
  role: (role: Role): string => {
    switch (role) {
      case "admin":
        return "Admin";
      case "user":
        return "User";
      default:
        return "Guest";
    }
  },
  notFound: {
    title: "Oups!",
    subtitle: "404 - Page Not Found",
    text: <>
      <p>
        It seems that the recipe you're looking for has been eaten just before you arrived. But don't worry, there
        are still plenty of other recipes on the site waiting just for you.
      </p>
      <p>
        What, this? No, those aren't cookie crumbs. You must be seeing things.
      </p>
    </>,
  },
  error: {
    notFound: "Page not found. Please try Again.",
    unauthorized: "Please log in.",
    forbidden: "Access denied. Please log in with an account with the required privileges and try again.",
    demoMode : "This action cannot be done in Demo mode.",
    unknown: "An unexpected error occurred. Please try again later.",
  }
};

export default dict;