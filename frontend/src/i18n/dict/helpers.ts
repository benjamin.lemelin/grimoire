export function parseIntStrict(text: string): number {
  if (/^([-+])?(\d+)$/.test(text))
    return Number(text);
  return NaN;
}

export function isNumeric(text: string) : boolean {
  return !Number.isNaN(parseIntStrict(text));
}

export function isStartWithVowel(text: string): boolean {
  const firstLetter = (text[0] ?? "").toLowerCase();
  return (
    firstLetter === 'a' ||
    firstLetter === 'e' ||
    firstLetter === 'i' ||
    firstLetter === 'o' ||
    firstLetter === 'u' ||
    firstLetter === 'y'
  );
}

export function toDuration(seconds: number) {
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor(seconds % 3600 / 60);

  return {
    hours,
    minutes
  }
}