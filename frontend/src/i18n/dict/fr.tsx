import {Dictionary} from "../index";
import {Difficulty, Role} from "../../api";
import {isNumeric, isStartWithVowel, toDuration} from "./helpers";

const dict: Dictionary = {
  grimoire: "Grimoire",
  headerBar: {
    home: "Accueil",
    search: "Rechercher",
    createRecipe: "Créer une recette",
    userAccount: "Compte utilisateur",
    settings: "Paramètres",
    lightMode: "Thème clair",
    darkMode: "Thème foncé",
    language: "Langue",
    logout: "Se déconnecter",
    logoutError: "Erreur inattendue lors de la déconnexion.",
  },
  navbar: {
    home: "Accueil",
    search: "Recherche",
    recipes: "Recettes",
    categories: "Catégories",
  },
  home: {
    recipes: "Recettes",
    categories: "Catégories",
    notFound: "Aucune recette trouvée",
  },
  search: {
    title: "Recherche",
    empty: "Aucune recette",
    notFound: "Aucune recette trouvée pour \"{{ query }}\"",
  },
  category: {
    list: {
      title: "Catégories",
      notFound: "Aucune catégorie trouvée",
    },
    view: {
      empty: "Aucune recette trouvée pour \"{{ category }}\"",
    },
  },
  recipe: {
    list: {
      title: "Recettes",
      empty: "Aucune recette trouvée",
    },
    view: {
      ingredient: {
        title: "Ingrédients",
        content: (name: string, quantity: string, comment: string) => {
          const isNameStartingWithVowel = isStartWithVowel(name);
          const isQuantityNumeric = isNumeric(quantity);

          let content = name;
          if (quantity.length > 0) {
            if (isQuantityNumeric) {
              content = `${quantity} ${name}`;
            } else {
              content = `${quantity}${isNameStartingWithVowel ? " d'" : " de "}${name}`;
            }
          }
          if (comment.length > 0) {
            content = `${content} - ${comment}`;
          }

          return content;
        },
      },
      instruction: {
        title: "Préparation",
      },
      options: {
        title: "Options",
        addToFavorites: "Ajouter aux favoris",
        addingToFavorites: "Ajout de la recette aux favoris.",
        addedToFavorites: "Recette ajoutée aux favoris.",
        removeFromFavorites: "Retirer des favoris",
        removingFromFavorites: "Retrait de la recette des favoris.",
        removedFromFavorites: "Recette retirée des favoris.",
        printRecipe: "Imprimer",
        editRecipe: "Modifier la recette",
        deleteRecipe: "Supprimer la recette",
        deleteDialogTitle: "Supprimer définitivement ?",
        deleteDialogText: "Cette opération est irréversible.",
        deleteDialogConfirm: "Supprimer",
        deletingRecipe: "Suppression de la recette.",
        deletedRecipe: "Recette supprimée.",
      },
    },
    new: {
      title: "Créer une recette",
      confirm: "Enregistrer",
      recipeCreating: "Création de la recette.",
      recipeCreated: "Recette crée",
      recipeNameEmptyError: "Veuillez donner un nom à votre recette.",
    },
    edit: {
      title: "Modifier une recette",
      recipe: {
        title: "Informations générales",
        name: "Nom de votre recette",
        author: "Auteur",
        description: "Description",
        yield: "Rendement",
        categories: "Catégories",
      },
      ingredient: {
        title: "Ingrédients",
        name: "Nom",
        quantity: "Quantité",
        comment: "Commentaire",
        add: "Ajouter un ingrédient",
      },
      instruction: {
        title: "Instructions",
        add: "Ajouter une instruction",
      },
      confirm: "Enregistrer",
      recipeUpdating: "Sauvegarde de la recette en cours.",
      recipeUpdated: "Recette sauvegardée.",
      recipeNameEmptyError: "Veuillez donner un nom à votre recette.",
    },
  },
  about: {
    title: "À propos du Grimoire",
    license: "Informations légales",
    description: `Le Grimoire est une application de gestion de recettes qui permet aux utilisateurs de créer et de stocker leurs
                  propres recettes uniques. Les utilisateurs peuvent facilement écrire leurs recettes préférées, y compris les
                  ingrédients et les étapes de préparation, et les visualiser tous dans un emplacement pratique. Que vous soyez un
                  chef expérimenté ou un débutant en cuisine, le Grimoire est l'outil parfait pour organiser et suivre toutes vos
                  recettes préférées.`,
  },
  login: {
    title: "Connexion",
    subtitle: "Utiliser votre compte Grimoire",
    username: "Nom d'utilisateur",
    password: "Mot de passe",
    login: "Se connecter",
    usernameEmptyError: "Veuillez entrer votre nom d'utilisateur.",
    passwordEmptyError: "Veuillez entrer votre mot de passe.",
    wrongCredentialsError: "Nom d'utilisateur ou mot de passe incorrect.",
  },
  setup: {
    title: "Configuration",
    subtitle: "Création du compte administrateur",
    confirm: "Créer le compte administrateur",
    adminUserCreated: "Compte administrateur créé. Veuillez vous connecter.",
  },
  account : {
    title : "Compte utilisateur",
    defaultName : "Mon Compte",
  },
  settings: {
    account: {
      title: (username: string, isOwnAccount: boolean) => {
        return isOwnAccount ? "Mon compte" : `Compte de ${username}`
      },
      profile: {
        title: "Informations générales",
        confirm: "Enregistrer",
        firstName: "Prénom",
        lastName: "Nom",
        email: "Address de courriel",
        invalidEmailError: "L'adresse de courriel est invalide.",
        profileUpdated: "Profil modifié.",
      },
      username: {
        title: "Nom d'utilisateur",
        confirm: "Modifier",
        username: "Nom d'utilisateur ",
        usernameEmptyError: "Votre nom d'utilisateur doit comporter au moins 1 lettre.",
        usernameAlreadyInUseWarning: "Le nouveau nom ne doit pas être déjà utilisé par une autre personne.",
        usernameUpdated : "Nom d'utilisateur modifié.",
      },
      role: {
        title: "Role",
        confirm: "Modifier",
        roleExplanation: "Les administrateurs ont le contrôle complet sur l'application. Les utilisateurs peuvent créer et modifier des recettes. Les invités ne peuvent que les consulter.",
        editingOwnAccountWarning: () => <>
          Cette opération modifiera le rôle de votre <strong>propre compte</strong>.
        </>,
        roleUpdated: "Role de l'utilisateur modifié.",
      },
    },
    password: {
      title: "Mot de passe",
      confirm: "Enregistrer",
      currentPassword: "Mot de passe actuel",
      newPassword: "Nouveau mot de passe",
      newPasswordConfirmation: "Confirmation du mot de passe",
      currentPasswordEmptyError: "Veuillez saisir votre mot de passe actuel.",
      newPasswordLengthError: "Le nouveau mot de passe doit avoir au moins {{ length }} caractères de long.",
      newPasswordConfirmationError: "Le mot de passe et la confirmation du mot de passe ne correspondent pas.",
      passwordChanged: "Mot de passe modifié.",
      wrongPasswordError: "Mot de passe incorrect.",
    },
    user: {
      title: "Utilisateurs",
      add: "Ajouter un compte",
      new: {
        title: "Créer un utilisateur",
        confirm: "Créer",
        username: "Nom d'utilisateur",
        password: "Mot de passe",
        passwordConfirmation: "Confirmation du mot de passe",
        role: "Role",
        roleExplanation: "Les administrateurs ont le contrôle complet sur l'application. Les utilisateurs peuvent créer et modifier des recettes. Les invités ne peuvent que les consulter.",
        usernameEmptyError: "Votre nom d'utilisateur doit comporter au moins 1 lettre.",
        passwordLengthError: "Le mot de passe doit avoir au moins {{ length }} caractères de long.",
        passwordConfirmationError: "Le mot de passe et la confirmation du mot de passe ne correspondent pas.",
        creatingUser: "Création de l'utilisateur",
        createdUser: "Utilisateur créé.",
      },
      delete: {
        dialogTitle: "Supprimer l'utilisateur ?",
        dialogConfirm: "Supprimer",
        dialogContent: (username: string) => <>
          Cette opération supprimera le compte utilisateur {username} <strong>de manière irréversible</strong>.
        </>,
        deletingUser: "Suppression de l'utilisateur.",
        deletedUser: "Utilisateur supprimé."
      },
    },
    database: {
      title: "Base de données",
      export: {
        exportDatabase: "Sauvegarder la base de données",
        exportingDatabase: "Exportation en cours.",
        exportedDatabase: "Base de données exportée.",
      },
      restore: {
        restoreDatabase: "Restaurer la base de données",
        dialogTitle: "Restaurer la base de données ?",
        dialogConfirm: "Choisir une sauvegarde",
        dialogContent: <>
          Cela <strong>remplacera</strong> toutes les données existantes.
        </>,
        confirmDialogTitle: "Confirmation demandée",
        confirmDialogConfirm: "Confirmer la restauration",
        confirmDialogContent: () => <>
          <p>
            Vous êtes sur le point de <strong>remplacer</strong> les recettes existantes par celles de l'archive
            fournie. <strong>Cette opération est irréversible.</strong>
          </p>
          <p>
            Est-vous certain <strong>absolument certain</strong> de vouloir continuer ?
          </p>
        </>,
        restoringDatabase: "Restauration en cours.",
        restoredDatabase: "Base de données restaurée.",
      },
      clear: {
        clearDatabase: "Vider la base de données",
        dialogTitle: "Tout supprimer ?",
        dialogConfirm: "Tout supprimer",
        dialogContent: () => <>
          Cela <strong>supprimera</strong> toutes les recettes contenues dans l'application. Cette opération
          est <strong>irréversible</strong>.
        </>,
        confirmDialogTitle: "Confirmation demandée",
        confirmDialogConfirm: "Confirmer la suppression",
        confirmDialogContent: () => <>
          <p>
            Vous êtes sur le point de <strong>supprimer</strong> toutes vos recettes. <strong>Cette opération est irréversible.</strong>
          </p>
          <p>
            Est-vous certain <strong>absolument certain</strong> de vouloir continuer ?
          </p>
        </>,
        clearingDatabase: "Suppression des données en cours.",
        clearedDatabase: "Base de données vidée.",
      },
    },
    about : {
      title : "À propos"
    },
  },
  dialog: {
    ok: "Ok",
    cancel: "Annuler",
    time: {
      title: "Sélectionner durée",
    },
  },
  duration: (seconds: number): string => {
    const {hours, minutes} = toDuration(seconds);

    let text = "";
    if (hours > 0) text += `${hours}h `;
    if (minutes > 0) text += `${minutes} min`;
    if (text.length === 0) text = "--";

    return text;
  },
  difficulty: (difficulty: Difficulty) => {
    switch (difficulty) {
      case "normal":
        return "Moyen";
      case "hard":
        return "Difficile";
      default:
        return "Facile";
    }
  },
  role: (role: Role): string => {
    switch (role) {
      case "admin":
        return "Administrateur";
      case "user":
        return "Utilisateur";
      default:
        return "Invité";
    }
  },
  notFound: {
    title: "Oups!",
    subtitle: "404 - Page Not Found",
    text: <>
      <p>
        Apparemment, la recette que vous cherchez a été mangée juste avant que vous arriviez. Mais ne vous inquiétez
        pas, il y a encore plein d'autres recettes sur le site qui n'attendent que vous.
      </p>
      <p>
        Quoi, ça ? Non, ce ne sont pas des miettes de biscuits. Vous avez la berlue.
      </p>
    </>,
  },
  error: {
    notFound: "Page not trouvée. Veuillez réessayer.",
    unauthorized: "Veuillez vous connecter.",
    forbidden: "Accès refusé. Veuillez utiliser un compte ayant les bons accès et réessayer.",
    demoMode : "Cette action n'est pas possible en mode Demo.",
    unknown: "Une erreur inattendue est survenue. Veuillez réessayer plus tard.",
  }
};

export default dict;