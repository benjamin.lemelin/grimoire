import {createEffect, createMemo, createSignal, on, Show} from "solid-js";
import {A, useLocation, useMatch, useNavigate} from "@solidjs/router";
import {API} from "../api";
import {LocaleToggleButton, useI18n} from "../i18n";
import {useAuth} from "../auth";
import {useNotify} from "../notification";
import {Brand} from "../brand";
import {Icon} from "../icon";
import {SearchInput} from "../input";
import {Dropdown, DropdownItem, DropdownSeparator} from "../dropdown";
import {ThemeToggleButton} from "../theme";
import {UserSummary} from "../user";
import {Skeleton} from "../fallback";
import {useScrolled} from "../util/scroll";
import {toDialog, useSearchQuery} from "../util/url";
import {createCssTransition} from "../util/styling";

import Style from "./style.module.css"

export function HeaderBar() {
  let headerElement: HTMLElement;
  const [t] = useI18n();
  const [auth, setAuth] = useAuth();
  const hasUserRole = createMemo(() => auth()?.user()?.hasRole("user"));
  const location = useLocation();
  const navigate = useNavigate();
  const notify = useNotify();
  const [searchQuery, setSearchQuery] = useSearchQuery();
  const isScrolled = useScrolled(window);
  const isRecipePage = useMatch(() => "/recipes/*any", {any: /.+/});
  const isCategoryPage = useMatch(() => "/categories/*any", {any: /.+/});
  const isBackButtonVisible = () => !!isRecipePage() || !!isCategoryPage();
  const [backButtonDestination, setBackButtonDestination] = createSignal("/");

  function isDialog(path: string, search: string): boolean {
    return path.endsWith("/edit") || search.endsWith("dialog=new");
  }

  /* Highlight header when scrolled. */
  createCssTransition(() => headerElement, isScrolled, {
    enter: Style.highlightEnter, enterImmediate: true,
    active: Style.highlight,
    exit: Style.highlightExit
  });

  // Update back button destination on location change.
  // The only consistent way to manage this is to find where we came from and where we are going to.
  createEffect(on(() => [location.pathname, location.search], (current, previous) => {
    if (!previous) return; // Skip first run.

    const [currentPathname, currentSearch] = current;
    const [previousPathname, previousSearch] = previous;
    if (previousPathname === currentPathname) return; // Skip if path is the same as previous (prevent navigating to dialogs).

    // Skip if destination or previous is dialog.
    if (isDialog(currentPathname, currentSearch) || isDialog(previousPathname, previousSearch)) {
      return;
    }

    // From any to recipe, destination is previous page (including search params).
    if (isRecipePage()) {
      setBackButtonDestination(previousPathname + previousSearch);
    }
    // From any to category, destination is categories page.
    else if (isCategoryPage()) {
      setBackButtonDestination("/categories");
    }
    // For anything else, destination is root.
    else {
      setBackButtonDestination("/");
    }
  }));

  function onSearchSubmit(event: SubmitEvent) {
    event.preventDefault();

    if (searchQuery().trim())
      navigate(`/search?q=${searchQuery()}`);
  }

  function onNewButtonClick(event: MouseEvent) {
    event.preventDefault();
    navigate(toDialog("new"));
  }

  function onLogoutButtonClick() {
    API.logout().catch(() => notify.error(t("headerBar.logoutError")));
    setAuth();
    navigate("/");
  }

  return (
    <header ref={el => headerElement = el} class={Style.headerBar}>
      <Show when={auth()} keyed>
        <section class={Style.previous} classList={{[Style.visible]: isBackButtonVisible()}}>
          <A class="button circle transparent" href={backButtonDestination()}
             tabindex={isBackButtonVisible() ? "" : "-1"}>
            <Icon icon="arrow_back"/>
          </A>
        </section>
        <section class={Style.brand}>
          <A class="undecorated" href="/" aria-label={t("headerBar.home")}><Brand/></A>
        </section>
        <form action="/search" class={Style.search} onSubmit={onSearchSubmit}>
          <SearchInput name="q" placeholder={t("headerBar.search")} value={searchQuery()} onInput={setSearchQuery}/>
        </form>
      </Show>
      <section class={Style.actions}>
        <Show when={!auth()} keyed>
          <ThemeToggleButton class="circle transparent" iconOnly/>
        </Show>
        <Show when={auth()} keyed>
          <A class="button circle transparent desktop-hide" href="/search" aria-label={t("headerBar.search")}>
            <Icon icon="search"/>
          </A>
          <Show when={hasUserRole()} keyed>
            <A class="button circle transparent"
               href="/new"
               onClick={onNewButtonClick}
               aria-label={t("headerBar.createRecipe")}>
              <Icon icon="add"/>
            </A>
          </Show>
        </Show>
        <Show when={auth()} keyed>{auth =>
          <Dropdown class="compact transparent"
                    align="right"
                    text={<Icon icon="account_circle"/>}
                    aria-label={t("headerBar.userAccount")}>
            <Show when={auth.user()}
                  fallback={<Skeleton class={`${Style.profile} ${Style.skeleton}`}/>}
                  keyed>{user =>
              <DropdownItem>
                <UserSummary class={Style.profile} user={user} self/>
              </DropdownItem>
            }</Show>
            <DropdownSeparator/>
            <DropdownItem>
              <A class="button sharp transparent nowrap" href="/settings">
                <Icon icon="settings"/> {t("headerBar.settings")}
              </A>
            </DropdownItem>
            <DropdownItem>
              <ThemeToggleButton class="sharp transparent nowrap"/>
            </DropdownItem>
            <DropdownSeparator/>
            <DropdownItem>
              <LocaleToggleButton class="sharp transparent nowrap"/>
            </DropdownItem>
            <DropdownSeparator/>
            <DropdownItem>
              <button class="sharp transparent nowrap" onClick={onLogoutButtonClick}>
                <Icon icon="logout"/> {t("headerBar.logout")}
              </button>
            </DropdownItem>
          </Dropdown>
        }</Show>
      </section>
    </header>
  );
}