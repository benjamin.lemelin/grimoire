import {Duration} from "../api";
import {useI18n} from "../i18n";
import {Icon} from "../icon";

import Style from "./style.module.css";

type Props = {
  value: Duration
}

export function DurationTag(props: Props) {
  const [t] = useI18n();

  return (
    <span class={Style.duration}>
      <Icon icon="schedule"/>
      <span>{t("duration", props.value.seconds)}</span>
    </span>
  )
}