export function push<T>(values: T[], value: T): T[] {
  return [...values, value];
}

export function remove<T>(values: T[], element: number | T): T[] {
  let index;
  if (typeof element === "number") index = element;
  else index = values.indexOf(element);
  return [...values.slice(0, index), ...values.slice(index + 1)];
}

export function move<T>(values: T[], from: number, to: number): T[] {
  values = [...values];
  const [removed] = values.splice(from, 1);
  values.splice(to, 0, removed);
  return values
}