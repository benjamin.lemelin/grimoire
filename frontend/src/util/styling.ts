import {Accessor, createEffect} from "solid-js";
import {createMediaQuery} from "@solid-primitives/media";

export type ClassList = { [k: string]: boolean | undefined };

export function createCssTransition(
  element: Accessor<Element>,
  active: Accessor<boolean>,
  options: {
    enter: string,
    enterImmediate?: boolean,
    active: string,
    exit: string,
    exitImmediate?: boolean
  }
) {
  createEffect(() => {
    const currentElement = element();

    function onTransitionEnd() {
      if (active()) {
        currentElement.classList.add(options.active);
        currentElement.classList.remove(options.enter);
        currentElement.classList.remove(options.exit);
      } else {
        currentElement.classList.remove(options.active);
        currentElement.classList.remove(options.enter);
        currentElement.classList.remove(options.exit);
      }
    }

    if (active() && !currentElement.classList.contains(options.active)) {
      currentElement.classList.add(options.enter);
      if (options.enterImmediate) setTimeout(onTransitionEnd)
      else currentElement.addEventListener("transitionend", onTransitionEnd, {once: true});

    } else if (!active() && currentElement.classList.contains(options.active)) {
      currentElement.classList.add(options.exit);
      if (options.exitImmediate) setTimeout(onTransitionEnd)
      else currentElement.addEventListener("transitionend", onTransitionEnd, {once: true});
    }
  });
}

const isDesktopInternal = createMediaQuery("(min-width:940px)");

export function isDesktop(): boolean {
  return isDesktopInternal();
}