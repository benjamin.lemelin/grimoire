import {createSignal, Signal} from "solid-js";
import {createSingletonRoot} from "@solid-primitives/rootless";

type SearchSignal = Signal<string>;

export const useSearchQuery = createSingletonRoot<SearchSignal>(() => {
  const searchParams = new URLSearchParams(window.location.search);
  return createSignal(searchParams.get("q") ?? "");
})

export type Dialog = "new";

export function toDialog(dialog: Dialog): string {
  const url = new URL(window.location.href);
  url.searchParams.set("dialog", dialog);
  return `${url.pathname}${url.search}`;
}

export function toDialogExit(): string {
  const url = new URL(window.location.href);
  url.searchParams.delete("dialog");
  return `${url.pathname}${url.search}`;
}