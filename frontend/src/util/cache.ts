import {createSignal} from "solid-js";
import {createSingletonRoot} from "@solid-primitives/rootless";

type CacheHashSignal = [
  hash: () => number,
  cache: (url: string | undefined) => string | undefined,
  invalidate: () => void
];

export const useCacheHash = createSingletonRoot<CacheHashSignal>(() => {
  const [hash, setHash] = createSignal(0);
  return [
    hash,
    url => {
      const current = hash();
      return url ? `${url}?hash=${current}` : undefined;
    },
    () => {
      return setHash(hash => hash + 1);
    }
  ];
});