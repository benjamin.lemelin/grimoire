import {createEffect, createSignal, createMemo, onCleanup, Accessor} from "solid-js";

export function useScrolled(selector: string | Window | Accessor<Element>): Accessor<boolean> {
  const [isScrolled, setIsScrolled] = createSignal(false);

  createEffect(() => {
    const element = getElement(selector);
    const scrollY = getScrollAccessor(element);

    function onScroll() {
      setIsScrolled(scrollY() > 0);
    }

    element.addEventListener("scroll", onScroll);
    onCleanup(() => element.removeEventListener("scroll", onScroll));
  });

  // Scroll event can happen very often. This memo will debounce it.
  return createMemo(() => isScrolled());
}

function getElement(selector: string | Window | Accessor<Element>) : Element | Window {
  if (typeof selector === "string") {
    return document.querySelector(selector)!;
  } else if (typeof selector === "function") {
    return selector();
  } else {
    return selector;
  }
}

function getScrollAccessor(element: Element | Window): Accessor<number> {
  if (element instanceof Element) {
    return () => element.scrollTop;
  } else {
    return () => element.scrollY;
  }
}