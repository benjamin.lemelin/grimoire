import {createSignal, JSX, Show} from "solid-js";
import {useNavigate} from "@solidjs/router";
import {MIN_PASSWORD_LEN} from "../config";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Spinner} from "../spinner";
import {Brand} from "../brand";

import Style from "./setup-route.module.css";

export function SetupRoute() {
  const [t] = useI18n();
  const handleError = useErrorHandler();
  const navigate = useNavigate();
  const notify = useNotify();
  const [username, setUsername] = createSignal("");
  const isUsernameValid = () => !!username().trim();
  const [password, setPassword] = createSignal("");
  const isPasswordValid = () => password().length >= MIN_PASSWORD_LEN;
  const [passwordConfirmation, setPasswordConfirmation] = createSignal("");
  const isPasswordConfirmationValid = () => isPasswordValid() && password() === passwordConfirmation();
  const [validate, setValidate] = createSignal(false);
  const [isSending, setSending] = createSignal(false);

  const onUsernameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setUsername(event.currentTarget.value);
  };

  const onPasswordInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setPassword(event.currentTarget.value);
  };

  const onPasswordConfirmationInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setPasswordConfirmation(event.currentTarget.value);
  };

  async function onSubmit(event: SubmitEvent) {
    event.preventDefault();

    setValidate(true);
    if (!isUsernameValid()) return;
    if (!isPasswordValid()) return;
    setValidate(false);

    setSending(true);
    try {
      await API.createSetupUser(username(), password());
      notify.success(t("setup.adminUserCreated"));
      navigate("/");
    } catch (e) {
      handleError(e);
    }
    setSending(false);
  }

  return (
    <section class={Style.setup}>
      <header class={Style.header}>
        <Brand class={Style.brand}/>
        <h1>{t("setup.title")}</h1>
        <p class="muted">{t("setup.subtitle")}</p>
      </header>
      <form class={Style.form} onSubmit={onSubmit} novalidate>
        <label for="username">{t("settings.user.new.username")} :</label>
        <input id="username"
               class="bordered"
               classList={{"invalid": validate() && !isUsernameValid()}}
               type="text"
               value={username()}
               onInput={onUsernameInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isUsernameValid()}}>
          {t("settings.user.new.usernameEmptyError")}
        </div>
        <label for="newPassword">{t("settings.user.new.password")} :</label>
        <input id="newPassword"
               class="bordered"
               classList={{"invalid": validate() && !isPasswordValid()}}
               type="password"
               value={password()}
               onInput={onPasswordInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isPasswordValid()}}>
          {t("settings.user.new.passwordLengthError", {length: MIN_PASSWORD_LEN})}
        </div>
        <label for="passwordConfirmation">{t("settings.user.new.passwordConfirmation")} :</label>
        <input id="passwordConfirmation"
               class="bordered"
               classList={{"invalid": validate() && !isPasswordConfirmationValid()}}
               type="password"
               value={passwordConfirmation()}
               onInput={onPasswordConfirmationInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isPasswordConfirmationValid()}}>
          {t("settings.user.new.passwordConfirmationError")}
        </div>
        <button class="primary">
          <Show when={!isSending()} fallback={<Spinner small/>} keyed>
            {t("setup.confirm")}
          </Show>
        </button>
      </form>
    </section>
  );
}