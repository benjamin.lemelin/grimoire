import {createEffect, createSignal} from "solid-js";
import {createSingletonRoot} from "@solid-primitives/rootless";

export * from "./themed-img";
export * from "./theme-toggle-button";

export type Theme = "light" | "dark";
type ThemeSignal = [get: ThemeAccessor, set: ThemeSetter];
type ThemeAccessor = () => Theme;
type ThemeSetter = (theme: Theme | ((prev: Theme) => Theme)) => void;

export const useTheme: () => ThemeSignal = createSingletonRoot<ThemeSignal>(() => {
  const [theme, setTheme] = createSignal(loadTheme());

  createEffect(() => {
    updateTheme(theme());
  })

  return [theme, (theme) => saveTheme(setTheme(theme))];
});

const THEME_STORAGE_KEY = "theme";

function loadTheme(): Theme {
  return (localStorage.getItem(THEME_STORAGE_KEY) ?? preferredTheme()) as Theme;
}

function saveTheme(theme: Theme) {
  localStorage.setItem(THEME_STORAGE_KEY, theme);
}

function updateTheme(theme: Theme) {
  document.body.dataset.theme = theme;
}

function preferredTheme(): Theme {
  return window.matchMedia("(prefers-color-scheme: light)").matches ? "light" : "dark";
}