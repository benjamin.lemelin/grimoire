import {Show} from "solid-js";
import {Icon} from "../icon";
import {useI18n} from "../i18n";
import {useTheme} from "./index";

type Props = {
  class?: string,
  iconOnly?: boolean
}

export function ThemeToggleButton(props: Props) {
  const [t] = useI18n();
  const [theme, setTheme] = useTheme();

  function onClick() {
    setTheme(theme => theme === "light" ? "dark" : "light");
  }

  return (
    <button class={props.class} onClick={onClick}>
      <Icon icon={theme() == "light" ? "dark_mode" : "light_mode"}/>
      <Show when={!props.iconOnly}>
        {theme() == "light" ? t("headerBar.darkMode") : t("headerBar.lightMode")}
      </Show>
    </button>
  );
}