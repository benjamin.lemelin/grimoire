import {useTheme} from "./index";
import {ClassList} from "../util/styling";

type Props = {
  class?: string,
  classList?: ClassList,
  src: { light: string, dark: string },
  alt: string
}

export function ThemedImg(props: Props) {
  const [theme, _] = useTheme();

  return (
    <img class={props.class}
         classList={props.classList}
         src={theme() === "light" ? props.src.light : props.src.dark}
         alt={props.alt}/>
  );
}