import {Navigate} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";

export function SettingsDownloadDatabaseRoute() {
  const [t] = useI18n();
  const notify = useNotify();
  const handleError = useErrorHandler();

  notify.promise(API.exportDatabase(), {
    pending: t("settings.database.export.exportingDatabase"),
    success: file => {
      window.location.assign(URL.createObjectURL(file));
      return t("settings.database.export.exportedDatabase");
    },
    error: e => handleError(e, true),
  });

  return (
    <Navigate href="/settings/database"/>
  );
}