import {Show, createMemo, createEffect} from "solid-js";
import {A, Outlet, useMatch, useNavigate} from "@solidjs/router";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {Icon} from "../icon";
import {isDesktop} from "../util/styling";

import Style from "./settings-route.module.css";

export function SettingsRoute() {
  const [t] = useI18n();
  const [auth] = useAuth();
  const navigate = useNavigate();

  const isSubpage = useMatch(() => "/settings/*any", {any: /.+/});
  const isNavVisible = createMemo(() => isDesktop() || !isSubpage());
  const isContentVisible = createMemo(() => isDesktop() || isSubpage());

  createEffect(() => {
    if (isDesktop() && !isSubpage()) navigate("/settings/account");
  });

  return (
    <div class={Style.container}>
      <section class={`${Style.settings} card`}>
        <nav class={Style.nav} classList={{[Style.visible]: isNavVisible()}}>
          <A class="button" href="/settings/account" end>
            <Icon icon="person"/> {t("settings.account.title", "", true)}
          </A>
          <A class="button" href="/settings/password" end>
            <Icon icon="lock"/> {t("settings.password.title")}
          </A>
          <Show when={auth()?.user()?.role() === "admin"} keyed>
            <A class="button" href="/settings/users">
              <Icon icon="group"/> {t("settings.user.title")}
            </A>
            <A class="button" href="/settings/database">
              <Icon icon="database"/> {t("settings.database.title")}
            </A>
          </Show>
          <A class="button" href="/settings/help" end>
            <Icon icon="help"/> {t("settings.about.title")}
          </A>
        </nav>
        <Show when={isContentVisible()} keyed={false}>
          <Outlet/>
        </Show>
      </section>
    </div>
  )
}

