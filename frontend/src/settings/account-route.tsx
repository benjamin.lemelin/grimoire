import {createMemo, createResource, Show} from "solid-js";
import {A, useNavigate, useParams} from "@solidjs/router";
import {API, Role} from "../api";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {Suspense} from "../suspense";
import {SpinnerFallback} from "../fallback";
import {Icon} from "../icon";
import {UserEditProfile, UserEditRole, UserEditUsername} from "../user";

import Style from "./account-route.module.css";

type Params = {
  username?: string
}

export function SettingsAccountRoute() {
  const params = useParams<Params>();
  const [t] = useI18n();
  const [auth, setAuth] = useAuth();
  const navigate = useNavigate();
  const username = createMemo(() => params.username ?? auth()?.user()?.username() ?? "")
  const isOwnAccount = () => !params.username || auth()?.user()?.username() === params.username;
  const [user] = createResource(() => username(), username => {
    if (isOwnAccount()) return Promise.resolve(auth()!.user()!);
    else return API.findUserByUsername(username);
  });

  function onProfileUpdated(firstName: string, lastName: string, email: string) {
    const it = user();
    if (it) {
      it.setFirstName(firstName);
      it.setLastName(lastName);
      it.setEmail(email);
    }
  }

  function onUsernameUpdated(username: string) {
    const it = user();
    if (it) {
      it.setUsername(username);
    }

    if (isOwnAccount()) {
      setAuth();
      navigate("/");
    } else {
      navigate(`/settings/users/${username}`);
    }
  }

  function onRoleUpdated(role: Role) {
    const it = user();
    if (it) {
      it.setRole(role);
    }
  }

  return (
    <Suspense resource={user} loading={<SpinnerFallback/>} fallback={<SpinnerFallback/>}>{user =>
      <section class={Style.account}>
        <header class={Style.header}>
          <A class="button circle transparent"
             classList={{"desktop-hide": !params.username}}
             href={params.username ? "/settings/users" : "/settings"}>
            <Icon icon="arrow_back"/>
          </A>
          <h1>{t("settings.account.title", username(), isOwnAccount())}</h1>
        </header>
        <section class={Style.section}>
          <h2>{t("settings.account.profile.title")}</h2>
          <UserEditProfile user={user} onUpdated={onProfileUpdated}/>
        </section>
        <section class={Style.section}>
          <h2>{t("settings.account.username.title")}</h2>
          <UserEditUsername user={user} onUpdated={onUsernameUpdated}/>
        </section>
        <Show when={auth()?.user()?.role() === "admin"} keyed>
          <section class={Style.section}>
            <h2>{t("settings.account.role.title")}</h2>
            <Show when={isOwnAccount()} keyed>
              <p class="alert danger">
                <Icon icon="error"/>
                {t("settings.account.role.editingOwnAccountWarning")}
              </p>
            </Show>
            <UserEditRole user={user} onUpdated={onRoleUpdated}/>
          </section>
        </Show>
      </section>
    }</Suspense>
  )
}