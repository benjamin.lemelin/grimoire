import {createSignal} from "solid-js";
import {useNavigate} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useErrorHandler} from "../error";
import {useNotify} from "../notification";
import {Dialog} from "../dialog";

import StyleSettings from "./settings-route.module.css";

export function SettingsClearDatabaseRoute() {
  const [t] = useI18n();
  const navigate = useNavigate();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [isStep1Visible, setStep1Visible] = createSignal(true);
  const [isStep2Visible, setStep2Visible] = createSignal(false);

  function navigatePrevious() {
    navigate("/settings/database");
  }

  function onStep1Confirm() {
    setStep1Visible(false);
    setStep2Visible(true);
  }

  function onStep2Confirm() {
    notify.promise(API.clearDatabase(), {
      pending: t("settings.database.clear.clearingDatabase"),
      success: t("settings.database.clear.clearedDatabase"),
      error: e => handleError(e, true)
    });

    // Navigate immediately instead of waiting for importation to finish.
    navigatePrevious();
  }

  function onCancel() {
    navigatePrevious();
  }

  return (
    <>
      <Dialog class={StyleSettings.dialog}
              title={t("settings.database.clear.dialogTitle")}
              icon="delete"
              confirmText={t("settings.database.clear.dialogConfirm")}
              open={isStep1Visible()}
              onConfirm={onStep1Confirm}
              onCancel={onCancel}>
        <div class={StyleSettings.dialogContent}>
          {t("settings.database.clear.dialogContent")}
        </div>
      </Dialog>
      <Dialog class={StyleSettings.dialog}
              title={t("settings.database.clear.confirmDialogTitle")}
              icon="warning"
              confirmText={t("settings.database.clear.confirmDialogConfirm")}
              countdown={3}
              open={isStep2Visible()}
              onConfirm={onStep2Confirm}
              onCancel={onCancel}>
        <div class={StyleSettings.dialogContent}>
          {t("settings.database.clear.confirmDialogContent")}
        </div>
      </Dialog>
    </>
  );
}