import {useNavigate, useParams} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Dialog} from "../dialog";
import {useCacheHash} from "../util/cache";

import StyleSettings from "./settings-route.module.css";

type Params = {
  username: string
}

export function SettingsDeleteUserRoute() {
  const params = useParams<Params>();
  const [t] = useI18n();
  const navigate = useNavigate();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [, , invalidateCache] = useCacheHash();

  const onConfirmed = () => {
    notify.promise(API.deleteUser(params.username), {
      pending: t("settings.user.delete.deletingUser"),
      success: () => {
        invalidateCache();
        navigate("/settings/users");
        return t("settings.user.delete.deletedUser");
      },
      error: e => handleError(e, true),
    });
  }

  const onCancel = () => {
    navigate("/settings/users");
  }

  return (
    <Dialog class={StyleSettings.dialog}
            title={t("settings.user.delete.dialogTitle")}
            icon="delete"
            confirmText={t("settings.user.delete.dialogConfirm")}
            open
            onConfirm={onConfirmed}
            onCancel={onCancel}>
      <div class={StyleSettings.dialogContent}>
        {t("settings.user.delete.dialogContent", params.username)}
      </div>
    </Dialog>
  );
}