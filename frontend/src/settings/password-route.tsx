import {Show} from "solid-js";
import {A} from "@solidjs/router";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {UserEditPassword} from "../user";
import {Icon} from "../icon";
import {SpinnerFallback} from "../fallback";

import Style from "./password-route.module.css";

export function SettingsPasswordRoute() {
  const [t] = useI18n();
  const [auth] = useAuth();

  return (
    <Show when={auth()?.user()} fallback={<SpinnerFallback/>} keyed>{user =>
      <section class={Style.password}>
        <header class={Style.header}>
          <A class="button circle transparent desktop-hide" href="/settings">
            <Icon icon="arrow_back"/>
          </A>
          <h1>{t("settings.password.title")}</h1>
        </header>
        <UserEditPassword user={user}/>
      </section>
    }</Show>
  );
}