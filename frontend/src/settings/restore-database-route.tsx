import {createSignal} from "solid-js";
import {useNavigate} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Dialog} from "../dialog";

import StyleSettings from "./settings-route.module.css";

export function SettingsRestoreDatabaseRoute() {
  const [t] = useI18n();
  const navigate = useNavigate();
  const handleError = useErrorHandler();
  const notify = useNotify();
  const [file, setFile] = createSignal<File>();
  const [isStep1Visible, setStep1Visible] = createSignal(true);
  const isStep2Visible = () => !!file();

  function navigatePrevious() {
    navigate("/settings/database");
  }

  function onStep1Confirm() {
    setStep1Visible(false);

    // Show file selection dialog on mount.
    const fileInput = document.createElement("input");
    fileInput.type = "file";
    fileInput.accept = "application/zip";
    fileInput.addEventListener("input", () => {
      const file = fileInput.files?.item(0);
      if (file) setFile(() => file);
      else navigatePrevious();
    });
    fileInput.addEventListener("cancel", () => {
      navigatePrevious();
    });
    fileInput.click();
  }

  function onStep2Confirm() {
    const currentFile = file();
    if (currentFile) {
      notify.promise(API.importDatabase(currentFile), {
        pending: t("settings.database.restore.restoringDatabase"),
        success: t("settings.database.restore.restoredDatabase"),
        error: e => handleError(e, true)
      });

      // Navigate immediately instead of waiting for importation to finish.
      navigatePrevious();
    }
  }

  function onCancel() {
    navigatePrevious();
  }

  return (
    <>
      <Dialog class={StyleSettings.dialog}
              title={t("settings.database.restore.dialogTitle")}
              icon="settings_backup_restore"
              confirmText={t("settings.database.restore.dialogConfirm")}
              open={isStep1Visible()}
              onConfirm={onStep1Confirm}
              onCancel={onCancel}>
        <div class={StyleSettings.dialogContent}>
          {t("settings.database.restore.dialogContent")}
        </div>
      </Dialog>
      <Dialog class={StyleSettings.dialog}
              title={t("settings.database.restore.confirmDialogTitle")}
              icon="warning"
              confirmText={t("settings.database.restore.confirmDialogConfirm")}
              countdown={3}
              open={isStep2Visible()}
              onConfirm={onStep2Confirm}
              onCancel={onCancel}>
        <div class={StyleSettings.dialogContent}>
          {t("settings.database.restore.confirmDialogContent")}
        </div>
      </Dialog>
    </>
  );
}