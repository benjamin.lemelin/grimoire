import {createSignal, JSX} from "solid-js";
import {useNavigate} from "@solidjs/router";
import {MIN_PASSWORD_LEN} from "../config";
import {API, Role} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {Dialog} from "../dialog";
import {useErrorHandler} from "../error";
import {useCacheHash} from "../util/cache";

import Style from "./new-user-route.module.css";

export function SettingsNewUserRoute() {
  const [t] = useI18n();
  const handleError = useErrorHandler();
  const navigate = useNavigate();
  const notify = useNotify();
  const [, , invalidateCache] = useCacheHash();
  const [username, setUsername] = createSignal("");
  const isUsernameValid = () => !!username().trim();
  const [password, setPassword] = createSignal("");
  const isPasswordValid = () => password().length >= MIN_PASSWORD_LEN;
  const [passwordConfirmation, setPasswordConfirmation] = createSignal("");
  const isPasswordConfirmationValid = () => isPasswordValid() && password() === passwordConfirmation();
  const [role, setRole] = createSignal<Role>("user");
  const [validate, setValidate] = createSignal(false);

  const onUsernameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setUsername(event.currentTarget.value);
  };

  const onPasswordInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setPassword(event.currentTarget.value);
  };

  const onPasswordConfirmationInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setPasswordConfirmation(event.currentTarget.value);
  };

  const onRoleInput: JSX.EventHandler<HTMLSelectElement, InputEvent> = (event) => {
    setRole(event.currentTarget.value as Role);
  };

  function onConfirmed() {
    setValidate(true);
    if (!isUsernameValid()) return;
    if (!isPasswordValid()) return;
    if (!isPasswordConfirmationValid()) return;
    setValidate(false);

    notify.promise(API.addUser(username(), password(), role()), {
      pending: t("settings.user.new.creatingUser"),
      success: () => {
        invalidateCache();
        navigate("/settings/users");
        return t("settings.user.new.createdUser");
      },
      error: e => handleError(e, true),
    });
  }

  function onCancel() {
    navigate("/settings/users");
  }

  return (
    <Dialog title={t("settings.user.new.title")}
            confirmText={t("settings.user.new.confirm")}
            size="medium"
            open
            onConfirm={onConfirmed}
            onCancel={onCancel}>
      <form class={Style.form}>
        <div class={Style.username}>
          <label for="username">{t("settings.user.new.username")} :</label>
          <input id="username"
                 class="bordered"
                 classList={{"invalid": validate() && !isUsernameValid()}}
                 type="text"
                 value={username()}
                 onInput={onUsernameInput}
                 autofocus/>
          <div class={Style.error} classList={{[Style.visible]: validate() && !isUsernameValid()}}>
            {t("settings.user.new.usernameEmptyError")}
          </div>
        </div>
        <div>
          <label for="newPassword">{t("settings.user.new.password")} :</label>
          <input id="newPassword"
                 class="bordered"
                 classList={{"invalid": validate() && !isPasswordValid()}}
                 type="password"
                 value={password()}
                 onInput={onPasswordInput}/>
          <div class={Style.error} classList={{[Style.visible]: validate() && !isPasswordValid()}}>
            {t("settings.user.new.passwordLengthError", {length: MIN_PASSWORD_LEN})}
          </div>
        </div>
        <div>
          <label for="passwordConfirmation">{t("settings.user.new.passwordConfirmation")} :</label>
          <input id="passwordConfirmation"
                 class="bordered"
                 classList={{"invalid": validate() && !isPasswordConfirmationValid()}}
                 type="password"
                 value={passwordConfirmation()}
                 onInput={onPasswordConfirmationInput}/>
          <div class={Style.error} classList={{[Style.visible]: validate() && !isPasswordConfirmationValid()}}>
            {t("settings.user.new.passwordConfirmationError")}
          </div>
        </div>
        <div class={Style.role}>
          <label for="role">{t("settings.user.new.role")} :</label>
          <select id="role"
                  class="bordered"
                  value={role()}
                  onInput={onRoleInput}>
            <option value="admin">{t("role", "admin")}</option>
            <option value="user">{t("role", "user")}</option>
            <option value="guest">{t("role", "guest")}</option>
          </select>
          <p class="muted">
            {t("settings.user.new.roleExplanation")}
          </p>
        </div>
      </form>
    </Dialog>
  );
}