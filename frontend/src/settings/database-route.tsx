import {A, Outlet} from "@solidjs/router";
import {useI18n} from "../i18n";
import {Icon} from "../icon";

import Style from "./database-route.module.css";

export function SettingsDatabaseRoute() {
  const [t] = useI18n();

  return (
    <section class={Style.database}>
      <header class={Style.header}>
        <A class="button circle transparent desktop-hide" href="/settings">
          <Icon icon="arrow_back"/>
        </A>
        <h1>{t("settings.database.title")}</h1>
      </header>
      <ul class={Style.operations}>
        <li>
          <span>{t("settings.database.export.exportDatabase")}</span>
          <A class="button" href="/settings/database/download"><Icon icon="download"/></A>
        </li>
        <li>
          <span>{t("settings.database.restore.restoreDatabase")}</span>
          <A class="button" href="/settings/database/restore"><Icon icon="upload_file"/></A>
        </li>
        <li>
          <span>{t("settings.database.clear.clearDatabase")}</span>
          <A class="button" href="/settings/database/clear"><Icon icon="delete"/></A>
        </li>
      </ul>
      <Outlet/>
    </section>
  )
}