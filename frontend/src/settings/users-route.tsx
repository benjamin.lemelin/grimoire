import {createResource, Show, For, createEffect, on} from "solid-js";
import {A, Outlet} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {Icon} from "../icon";
import {CardListSkeleton, SpinnerFallback} from "../fallback";
import {Suspense} from "../suspense";
import {UserSummary} from "../user";
import {useCacheHash} from "../util/cache";
import {isDesktop} from "../util/styling";

import Style from "./users-route.module.css";

export function SettingsUsersRoute() {
  const [t] = useI18n();
  const [auth] = useAuth();
  const [cacheHash] = useCacheHash();
  const [users, {refetch}] = createResource(() => API.findAllUsers());

  // Re-fetch users when cache is dirty.
  createEffect(on(() => cacheHash(), () => refetch(), {defer: true}));

  return (
    <Show when={auth()?.user()} fallback={<SpinnerFallback/>} keyed>{currentUser =>
      <section class={Style.users}>
        <header class={Style.header}>
          <A class="button circle transparent desktop-hide" href="/settings">
            <Icon icon="arrow_back"/>
          </A>
          <h1>{t("settings.user.title")}</h1>
          {/* TODO : Create a special button for this case. The text disappears when on mobile. */}
          <A class="button" classList={{"circle": !isDesktop()}} href="/settings/users/new">
            <Icon icon="add"/> {isDesktop() && t("settings.user.add")}
          </A>
        </header>
        <Suspense resource={users}
                  loading={<CardListSkeleton class={Style.list} skeleton="user" horizontal/>}>{users =>
          <ul class={Style.list}>
            <For each={users}>{user =>
              <li class={`${Style.user} fade-in`}>
                <UserSummary user={user} self={currentUser.username() === user.username()}/>
                <A class="button circle" href={`/settings/users/${user.username()}`}>
                  <Icon icon="edit"></Icon>
                </A>
                <A class="button circle" href={`/settings/users/${user.username()}/delete`}>
                  <Icon icon="delete"></Icon>
                </A>
              </li>
            }</For>
          </ul>
        }</Suspense>
        <Outlet/>
      </section>
    }</Show>
  )
}