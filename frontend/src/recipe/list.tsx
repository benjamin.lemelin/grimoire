import {For} from "solid-js";
import {RecipeSummary} from "../api";
import {RecipeCard} from "./card";

import Style from "./list.module.css";

type Props = {
  class?: string,
  recipes: RecipeSummary[],
  horizontal?: boolean,
}

export function RecipeList(props: Props) {
  return (
    <ul class={`${Style.recipes} ${props.class ?? ""}`} classList={{[Style.horizontal]: props.horizontal}}>
      <For each={props.recipes}>{(recipe) =>
        <li>
          <RecipeCard recipe={recipe} horizontal={props.horizontal}/>
        </li>
      }</For>
    </ul>
  );
}