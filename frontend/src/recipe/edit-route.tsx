import {createMemo, createResource} from "solid-js";
import {useNavigate, useParams} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Suspense} from "../suspense";
import {RecipeEdit} from "./edit";
import {Dialog} from "../dialog";
import {SpinnerFallback} from "../fallback";
import {useCacheHash} from "../util/cache";

import Style from "./edit-route.module.css";

type Params = {
  id: string
}

export function RecipeEditRoute() {
  const params = useParams<Params>();
  const [t] = useI18n();
  const navigate = useNavigate();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const id = createMemo(() => parseInt(params.id));
  const [recipe, {mutate: setRecipe}] = createResource(() => id(), id => API.findRecipeById(id));
  const [, , invalidateCache] = useCacheHash();

  const onConfirm = () => {
    const currentRecipe = recipe();
    if (currentRecipe) {
      if (!currentRecipe.name().trim()) {
        notify.error(t("recipe.edit.recipeNameEmptyError"));
      } else {
        notify.promise(API.updateRecipe(currentRecipe), {
          pending: t("recipe.edit.recipeUpdating"),
          success: updatedRecipe => {
            invalidateCache();
            navigate(updatedRecipe.url());
            return t("recipe.edit.recipeUpdated");
          },
          error: e => handleError(e, true),
        });
      }
    }
  };

  const onCancel = () => {
    navigate("..");
  };

  return (
    <Dialog title={t("recipe.edit.title")}
            confirmText={t("recipe.edit.confirm")}
            size="large"
            alignButtons="end"
            open
            onConfirm={onConfirm}
            onCancel={onCancel}>
      <Suspense resource={recipe} loading={<SpinnerFallback class={Style.fallback}/>}>{recipe =>
        <RecipeEdit class={Style.recipeEdit} recipe={recipe} onRecipeInput={setRecipe}/>
      }</Suspense>
    </Dialog>
  );
}