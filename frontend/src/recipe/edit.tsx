import {createMemo, JSX, onMount} from "solid-js";
import {ModelFactory, Category, Difficulty, Duration, Recipe} from "../api";
import {useI18n} from "../i18n";
import {Icon} from "../icon";
import {ImageInput, TimeInput, ChipInput} from "../input";
import {ReorderableItem, ReorderableList, ReorderableHandle} from "../drag-and-drop";

import Style from "./edit.module.css";

type Props = {
  class?: string,
  recipe: Recipe,
  onRecipeInput?: (recipe: Recipe) => void
}

export function RecipeEdit(props: Props) {
  let nameInputElement: HTMLInputElement;
  let ingredientsElement: HTMLElement;
  let instructionsElement: HTMLElement;
  let [t] = useI18n();

  onMount(() => {
    nameInputElement.focus();
  });

  function focusNewIngredient() {
    setTimeout(() => {
      const nameElement = ingredientsElement.querySelector(`li:last-child input.${Style.name}`) as HTMLElement;
      if (nameElement) nameElement.focus();
    });
  }

  function focusNewInstruction() {
    setTimeout(() => {
      const nameElement = instructionsElement.querySelector(`li:last-child textarea`) as HTMLElement;
      if (nameElement) nameElement.focus();
    });
  }

  function notifyRecipeInput() {
    if (props.onRecipeInput) props.onRecipeInput(props.recipe);
  }

  const onNameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    props.recipe.setName(event.currentTarget.value);
    notifyRecipeInput();
  };

  const onImageInput = (image: File) => {
    props.recipe.setImage(() => image);
    notifyRecipeInput();
  };

  const onAuthorInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    props.recipe.setAuthor(event.currentTarget.value);
    notifyRecipeInput();
  };

  const onDescriptionInput: JSX.EventHandler<HTMLTextAreaElement, InputEvent> = (event) => {
    props.recipe.setDescription(event.currentTarget.value);
    notifyRecipeInput();
  };

  const onDurationChange = (duration: Duration) => {
    props.recipe.setDuration(duration);
    notifyRecipeInput();
  };

  const onDifficultyChange: JSX.EventHandler<HTMLSelectElement, InputEvent> = (event) => {
    props.recipe.setDifficulty(event.currentTarget.value as Difficulty);
    notifyRecipeInput();
  };

  const onYieldInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    props.recipe.setYield(event.currentTarget.value);
    notifyRecipeInput();
  };

  function onCategoriesAdd(category: Category) {
    props.recipe.addCategory(category);
    notifyRecipeInput();
  }

  function onCategoryRemove(index: number) {
    props.recipe.removeCategory(index);
    notifyRecipeInput();
  }

  function onIngredientNameInput(index: number, name: string) {
    props.recipe.ingredients()[index].setName(name);
    notifyRecipeInput();
  }

  function onIngredientQuantityInput(index: number, quantity: string) {
    props.recipe.ingredients()[index].setQuantity(quantity);
    notifyRecipeInput();
  }

  function onIngredientCommentInput(index: number, comment: string) {
    props.recipe.ingredients()[index].setComment(comment);
    notifyRecipeInput();
  }

  function onAddIngredientClick(event: MouseEvent) {
    event.preventDefault(); // Prevent form submit.
    props.recipe.addIngredient(ModelFactory.createIngredient());
    focusNewIngredient();
    notifyRecipeInput();
  }

  function onRemoveIngredientClick(event: MouseEvent, index: number) {
    event.preventDefault(); // Prevent form submit.
    props.recipe.removeIngredient(index);
    notifyRecipeInput();
  }

  function onIngredientMove(oldIndex: number, newIndex: number) {
    props.recipe.moveIngredient(oldIndex, newIndex);
    notifyRecipeInput();
  }

  function onInstructionTextInput(index: number, text: string) {
    props.recipe.instructions()[index].setText(text);
    notifyRecipeInput();
  }

  function onAddInstructionClick(event: MouseEvent) {
    event.preventDefault(); // Prevent form submit.
    props.recipe.addInstruction(ModelFactory.createInstruction());
    focusNewInstruction();
    notifyRecipeInput();
  }

  function onRemoveInstructionClick(event: MouseEvent, index: number) {
    event.preventDefault(); // Prevent form submit.
    props.recipe.removeInstruction(index);
    notifyRecipeInput();
  }

  function onInstructionMove(oldIndex: number, newIndex: number) {
    props.recipe.moveInstruction(oldIndex, newIndex);
    notifyRecipeInput();
  }

  return (
    <form class={`${Style.recipe} ${props.class ?? ""} fade-in`}>
      <section class={Style.recipeInfo} aria-label={t("recipe.edit.recipe.title")}>
        <input ref={el => nameInputElement = el}
               class={`${Style.recipeName} display-1`}
               type="text"
               value={props.recipe.name()}
               placeholder={t("recipe.edit.recipe.name")}
               onInput={onNameInput}/>
        <ImageInput class={Style.recipeImage}
                    value={props.recipe.image()}
                    alt={props.recipe.name()}
                    onInput={onImageInput}/>
        <input class={Style.recipeAuthor}
               type="text"
               value={props.recipe.author()}
               placeholder={t("recipe.edit.recipe.author")}
               onInput={onAuthorInput}/>
        <textarea class={Style.recipeDescription}
                  placeholder={t("recipe.edit.recipe.description")}
                  rows={4}
                  onInput={onDescriptionInput}>
        {props.recipe.description()}
      </textarea>
        <TimeInput class={Style.recipeDuration}
                   value={props.recipe.duration()}
                   onInput={onDurationChange}/>
        <select class={`${Style.recipeDifficulty} icon-egg`}
                value={props.recipe.difficulty()}
                onInput={onDifficultyChange}>
          <option value="easy">{t("difficulty", "easy")}</option>
          <option value="normal">{t("difficulty", "normal")}</option>
          <option value="hard">{t("difficulty", "hard")}</option>
        </select>
        <input class={`${Style.recipeYield} icon-soup`}
               type="text"
               value={props.recipe.yield()}
               placeholder={t("recipe.edit.recipe.yield")}
               onInput={onYieldInput}/>
        <ChipInput class={Style.recipeCategories}
                   value={props.recipe.categories()}
                   placeholder={t("recipe.edit.recipe.categories")}
                   onAdd={onCategoriesAdd}
                   onRemove={onCategoryRemove}/>
      </section>
      <section ref={el => ingredientsElement = el} class={Style.recipeIngredients}>
        <header>
          <h2>{t("recipe.edit.ingredient.title")}</h2>
        </header>
        <ReorderableList items={props.recipe.ingredients()} onMove={onIngredientMove}>{(ingredient, i) => {
          const isTitle = createMemo(() => ingredient.name().trim().startsWith("#"));

          return (
            <ReorderableItem class={`${Style.recipeIngredient} ${Style.recipeDraggable}`}
                             classList={{[Style.highlight]: isTitle()}}>
              <ReorderableHandle class={Style.handle}/>
              <input class={Style.name}
                     classList={{[Style.title]: isTitle()}}
                     type="text"
                     value={ingredient.name()}
                     placeholder={t("recipe.edit.ingredient.name")}
                     oninput={event => onIngredientNameInput(i(), event.currentTarget.value)}/>
              <input class={`${Style.quantity} icon-weight`} type="text"
                     value={ingredient.quantity()}
                     placeholder={t("recipe.edit.ingredient.quantity")}
                     oninput={event => onIngredientQuantityInput(i(), event.currentTarget.value)}/>
              <input class={`${Style.comment} icon-comment`}
                     type="text"
                     value={ingredient.comment()}
                     placeholder={t("recipe.edit.ingredient.comment")}
                     oninput={event => onIngredientCommentInput(i(), event.currentTarget.value)}/>
              <button class={`${Style.delete} circle transparent`}
                      onClick={event => onRemoveIngredientClick(event, i())}>
                <Icon class="mobile-hide" icon="remove"/>
                <Icon class="desktop-hide" icon="delete"/>
              </button>
            </ReorderableItem>
          );
        }}</ReorderableList>
        <footer>
          <button class="pill" onClick={onAddIngredientClick}>
            <Icon icon="add"/> {t("recipe.edit.ingredient.add")}
          </button>
        </footer>
      </section>
      <section ref={el => instructionsElement = el} class={Style.recipeInstructions}>
        <header>
          <h2>{t("recipe.edit.instruction.title")}</h2>
        </header>
        <ReorderableList items={props.recipe.instructions()} onMove={onInstructionMove}>{(instruction, i) => {
          const isTitle = createMemo(() => instruction.text().trim().startsWith("#"));

          return (
            <ReorderableItem class={`${Style.recipeInstruction} ${Style.recipeDraggable}`}
                             classList={{[Style.highlight]: isTitle()}}>
              <ReorderableHandle class={Style.handle}/>
              <textarea class={Style.text}
                        value={instruction.text()}
                        rows={7}
                        oninput={event => onInstructionTextInput(i(), event.currentTarget.value)}/>
              <button class={`${Style.delete} circle transparent`}
                      onClick={event => onRemoveInstructionClick(event, i())}>
                <Icon class="mobile-hide" icon="remove"/>
                <Icon class="desktop-hide" icon="delete"/>
              </button>
            </ReorderableItem>
          );
        }}</ReorderableList>
        <footer>
          <button class="pill" onClick={onAddInstructionClick}>
            <Icon icon="add"/> {t("recipe.edit.instruction.add")}
          </button>
        </footer>
      </section>
    </form>
  );
}