import {createSignal} from "solid-js";
import {useNavigate} from "@solidjs/router";
import {Recipe, API, ModelFactory} from "../api";
import {useI18n} from "../i18n";
import {useNotify} from "../notification";
import {useErrorHandler} from "../error";
import {Dialog} from "../dialog";
import {RecipeEdit} from "./edit";
import {toDialogExit} from "../util/url";
import {useCacheHash} from "../util/cache";

import Style from "./new-route.module.css";

export function RecipeNewRoute() {
  const [t] = useI18n();
  const navigate = useNavigate();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const [recipe, setRecipe] = createSignal<Recipe>(ModelFactory.createRecipe());
  const [, , invalidateCache] = useCacheHash();

  const onConfirm = () => {
    if (!recipe().name().trim()) {
      notify.error(t("recipe.new.recipeNameEmptyError"));
    } else {
      notify.promise(API.addRecipe(recipe()), {
        pending: t("recipe.new.recipeCreating"),
        success: newRecipe => {
          invalidateCache();
          navigate(newRecipe.url());
          return t("recipe.new.recipeCreated");
        },
        error: e => handleError(e, true),
      });
    }
  };

  const onCancel = () => {
    navigate(toDialogExit());
  };

  return (
    <Dialog title={t("recipe.new.title")}
            confirmText={t("recipe.new.confirm")}
            size="large"
            alignButtons="end"
            open
            onConfirm={onConfirm}
            onCancel={onCancel}>
      <RecipeEdit class={Style.recipeNew} recipe={recipe()} onRecipeInput={setRecipe}/>
    </Dialog>
  );
}