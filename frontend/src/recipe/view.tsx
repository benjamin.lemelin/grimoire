import {createMemo, createSignal, For, Match, Show, Switch} from "solid-js";
import {A} from "@solidjs/router";
import {Recipe} from "../api";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {Icon} from "../icon";
import {Dialog} from "../dialog";
import {ImageFallback} from "../image";
import {DurationTag} from "../duration";
import {DifficultyTag} from "../difficulty";
import {YieldTag} from "../yield";
import {CategoryTag} from "../category";
import {useCacheHash} from "../util/cache";

import Style from "./view.module.css";

type Props = {
  recipe: Recipe,
  onFavoriteToggleClick?: () => void,
  onPrintClick?: () => void,
  onDeleteClick?: () => void,
}

export function RecipeView(props: Props) {
  const [t] = useI18n();
  const [auth] = useAuth();
  const hasUserRole = createMemo(() => auth()?.user()?.hasRole("user"));
  const [isDeleteDialogVisible, setDeleteDialogVisible] = createSignal(false);
  const [, withCache] = useCacheHash();

  const onDeleteClick = () => {
    setDeleteDialogVisible(true);
  };

  const onDeleteConfirm = () => {
    if (props.onDeleteClick) props.onDeleteClick();
    setDeleteDialogVisible(false);
  };

  const onDeleteCancel = () => {
    setDeleteDialogVisible(false);
  }

  return (
    <>
      <article class={`${Style.recipe} fade-in`}>
        <section class={Style.recipeImage}>
          <ImageFallback src={withCache(props.recipe.imageUrl())} alt={props.recipe.name()}/>
        </section>
        <header class={Style.recipeHeader}>
          <h1 class={`${Style.recipeName} display-1`}>{props.recipe.name()}</h1>
          <Switch>
            <Match when={hasUserRole()} keyed>
              <button class={`${Style.recipeFavorite} circle transparent`} onClick={props.onFavoriteToggleClick}>
                <Icon classList={{"fill": props.recipe.isFavorite()}} icon="favorite"/>
              </button>
            </Match>
            <Match when={!hasUserRole()} keyed>
              <Icon class={Style.recipeFavorite} classList={{"fill": props.recipe.isFavorite()}}
                    icon="favorite"/>
            </Match>
          </Switch>
          <p class={`${Style.recipeAuthor} muted`}>
            {props.recipe.author()}
          </p>
          <p class={Style.recipeDescription}>
            {props.recipe.description()}
          </p>
          <ul class={Style.recipeStats}>
            <li>
              <DurationTag value={props.recipe.duration()}/>
            </li>
            <li>
              <DifficultyTag value={props.recipe.difficulty()}/>
            </li>
            <li>
              <YieldTag yield={props.recipe.yield()}/>
            </li>
            <For each={props.recipe.categories()}>{(category) =>
              <li>
                <CategoryTag category={category}/>
              </li>
            }</For>
          </ul>
        </header>
        <section class={Style.recipeIngredients}>
          <h2>{t("recipe.view.ingredient.title")}</h2>
          <ul class={Style.recipeChecklist}>
            <For each={props.recipe.ingredients()}>{(ingredient, i) => {
              const name = createMemo(() => ingredient.name().trim());
              const quantity = createMemo(() => ingredient.quantity().trim());
              const comment = createMemo(() => ingredient.comment().trim());

              const title = createMemo(() => name().substring(1));
              const isTitle = createMemo(() => name().startsWith("#"));
              const content = createMemo(() => t("recipe.view.ingredient.content", name(), quantity(), comment()));

              return (
                <li classList={{[Style.title]: isTitle()}}>
                  <Switch>
                    <Match when={isTitle()}>
                      <h3 class="muted">{title()}</h3>
                    </Match>
                    <Match when={!isTitle()}>
                      <input id={`ingredient-${i()}`} type="checkbox"/>
                      <label for={`ingredient-${i()}`}>{content()}</label>
                    </Match>
                  </Switch>
                </li>
              );
            }}</For>
          </ul>
        </section>
        <section class={Style.recipeInstructions}>
          <h2>{t("recipe.view.instruction.title")}</h2>
          <ol class={Style.recipeChecklist}>
            <For each={props.recipe.instructions()}>{(instruction, i) => {
              const text = createMemo(() => instruction.text().trim());
              const title = createMemo(() => text().substring(1));
              const isTitle = createMemo(() => text().startsWith("#"));
              const content = createMemo(() => text());

              return (
                <li classList={{[Style.title]: isTitle()}}>
                  <Switch>
                    <Match when={isTitle()}>
                      <h3 class="muted">{title()}</h3>
                    </Match>
                    <Match when={!isTitle()}>
                      <input id={`instruction-${i()}`} type="checkbox"/>
                      <label for={`instruction-${i()}`}>{content()}</label>
                    </Match>
                  </Switch>
                </li>
              );
            }}</For>
          </ol>
        </section>
        <section class={Style.recipeOptions}>
          <h2>{t("recipe.view.options.title")}</h2>
          <Show when={hasUserRole()} keyed>
            <button class="link" onClick={props.onFavoriteToggleClick}>
              <Icon classList={{"fill": props.recipe.isFavorite()}} icon="favorite"/>
              <span>{!props.recipe.isFavorite() ? t("recipe.view.options.addToFavorites") : t("recipe.view.options.removeFromFavorites")}</span>
            </button>
          </Show>
          <button class="link" onClick={props.onPrintClick}>
            <Icon icon="print"/>
            <span>{t("recipe.view.options.printRecipe")}</span>
          </button>
          <Show when={hasUserRole()} keyed>
            <A class="button link" href={`/recipes/${props.recipe.id()}/edit`}>
              <Icon icon="edit"/>
              <span>{t("recipe.view.options.editRecipe")}</span>
            </A>
            <button class="link" onClick={onDeleteClick}>
              <Icon icon="delete"/>
              <span>{t("recipe.view.options.deleteRecipe")}</span>
            </button>
          </Show>
        </section>
      </article>
      <Show when={isDeleteDialogVisible()}>
        <Dialog title={t("recipe.view.options.deleteDialogTitle")}
                icon="delete"
                confirmText={t("recipe.view.options.deleteDialogConfirm")}
                open
                onConfirm={onDeleteConfirm}
                onCancel={onDeleteCancel}>
          <div class={Style.dialogContent}>
            {t("recipe.view.options.deleteDialogText")}
          </div>
        </Dialog>
      </Show>
    </>
  );
}