import {createEffect, createMemo, createResource, on} from "solid-js";
import {Outlet, useNavigate, useParams} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {useErrorHandler} from "../error";
import {Suspense} from "../suspense";
import {useNotify} from "../notification";
import {SpinnerFallback} from "../fallback";
import {RecipeView} from "./view";
import {useCacheHash} from "../util/cache";

import Style from "./view-route.module.css";

type Params = {
  id: string
}

export function RecipeViewRoute() {
  const params = useParams<Params>();
  const [t] = useI18n();
  const navigate = useNavigate();
  const notify = useNotify();
  const handleError = useErrorHandler();
  const id = createMemo(() => parseInt(params.id));
  const [recipe, {refetch}] = createResource(() => id(), id => API.findRecipeById(id));
  const [cacheHash] = useCacheHash();

  // Re-fetch recipe when cache is dirty.
  createEffect(on(() => cacheHash(), () => refetch(), {defer: true}));

  const onFavoriteToggleClick = () => {
    const currentRecipe = recipe();
    if (currentRecipe) {
      // Make the change immediately. It will be reverted if any error occurs.
      const isFavorite = currentRecipe.setFavorite(isFavorite => !isFavorite);

      if (isFavorite) {
        notify.promise(API.setFavoriteRecipe(currentRecipe.id()), {
          pending: t("recipe.view.options.addingToFavorites"),
          success: t("recipe.view.options.addToFavorites"),
          error: e => {
            currentRecipe.setFavorite(false);
            return handleError(e, true);
          }
        });
      } else {
        notify.promise(API.unsetFavoriteRecipe(currentRecipe.id()), {
          pending: t("recipe.view.options.removingFromFavorites"),
          success: t("recipe.view.options.removedFromFavorites"),
          error: e => {
            currentRecipe.setFavorite(true);
            return handleError(e, true);
          }
        });
      }
    }
  }

  const onPrintClick = () => {
    window.print();
  }

  const onDeleteClick = () => {
    const currentRecipe = recipe();
    if (currentRecipe) {
      notify.promise(API.deleteRecipe(currentRecipe.id()), {
        pending: t("recipe.view.options.deletingRecipe"),
        success: () => {
          navigate("/");
          return t("recipe.view.options.deletedRecipe");
        },
        error: e => handleError(e, true),
      });
    }
  }

  return (
    <>
      <Suspense resource={recipe}
                loading={<SpinnerFallback class={Style.fallback}/>}>{recipe =>
        <RecipeView recipe={recipe}
                    onFavoriteToggleClick={onFavoriteToggleClick}
                    onPrintClick={onPrintClick}
                    onDeleteClick={onDeleteClick}/>
      }</Suspense>
      <Outlet/>
    </>
  );
}