import {A} from "@solidjs/router";
import {RecipeSummary} from "../api";
import {ImageFallback} from "../image";
import {DifficultyTag} from "../difficulty";
import {DurationTag} from "../duration";
import {useCacheHash} from "../util/cache";

import Style from "./card.module.css";

type Props = {
  recipe: RecipeSummary,
  horizontal?: boolean
}

export function RecipeCard(props: Props) {
  const [,withCache] = useCacheHash();

  return (
    <A class="card fade-in" href={props.recipe.url()} aria-label={props.recipe.name()}>
      <article class={Style.recipe} classList={{[Style.horizontal]: props.horizontal}}>
        <ImageFallback class={Style.image}
                     src={withCache(props.recipe.imageUrl())}
                     alt={props.recipe.name()}/>
        <h2 class={`${Style.name} ellipsis`}>
          {props.recipe.name()}
        </h2>
        <p class={`${Style.description} ellipsis`}>
          {props.recipe.description()}
        </p>
        <p class={`${Style.stats} muted`}>
          <DurationTag value={props.recipe.duration()}/>
          <DifficultyTag value={props.recipe.difficulty()}/>
        </p>
      </article>
    </A>
  );
}