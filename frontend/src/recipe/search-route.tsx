import {createMemo, createResource, Show, untrack} from "solid-js";
import {useNavigate, useSearchParams} from "@solidjs/router";
import {API} from "../api";
import {useI18n} from "../i18n";
import {Suspense} from "../suspense";
import {CardListSkeleton, EmptyStateFallback} from "../fallback";
import {SearchInput} from "../input";
import {RecipeList} from "./list";
import {useSearchQuery} from "../util/url";
import {useScrolled} from "../util/scroll";

import Style from "./search-route.module.css";

type Params = {
  q?: string
}

export function RecipeSearchRoute() {
  const [params] = useSearchParams<Params>();
  const [t] = useI18n();
  const navigate = useNavigate();
  const isHighlighted = useScrolled(window);
  const [searchQuery, setSearchQuery] = useSearchQuery();
  const currentQuery = createMemo(() => params.q?.trim() || untrack(searchQuery));
  const [recipes] = createResource(() => currentQuery() ? currentQuery() : null, keywords => API.findRecipesByKeywords(keywords));

  const onSearchSubmit = (event: SubmitEvent) => {
    event.preventDefault();

    if (searchQuery().trim())
      navigate(`/search?q=${searchQuery()}`);
  };

  return (
    <section class={Style.search} aria-label={t("search.title")}>
      <header class={Style.header} classList={{[Style.highlight]: isHighlighted()}}>
        <form action="/search" onSubmit={onSearchSubmit}>
          <SearchInput class={Style.input} name="q" value={searchQuery()} onInput={setSearchQuery}/>
        </form>
      </header>
      <Suspense resource={recipes}
                loading={<CardListSkeleton class={Style.recipes} skeleton="recipe" horizontal/>}
                fallback={<EmptyStateFallback icon="search">{t("search.empty")}</EmptyStateFallback>}>{recipes =>
        <Show when={recipes.length > 0}
              fallback={<EmptyStateFallback icon="search">{t("search.notFound", {query: currentQuery()})}</EmptyStateFallback>}>
          <RecipeList class={Style.recipes} recipes={recipes} horizontal/>
        </Show>
      }</Suspense>
    </section>
  );
}