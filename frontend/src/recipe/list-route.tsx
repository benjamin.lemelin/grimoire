import {createResource, Show} from "solid-js";
import {API} from "../api";
import {useI18n} from "../i18n";
import {Suspense} from "../suspense";
import {CardListSkeleton, EmptyStateFallback} from "../fallback";
import {RecipeList} from "./list";

import Style from "./list-route.module.css";

export function RecipeListRoute() {
  const [t] = useI18n();
  const [recipes] = createResource(() => API.findAllRecipes());

  return (
    <Suspense resource={recipes}
              loading={<CardListSkeleton class={Style.recipes} skeleton="recipe"/>}>{recipes =>
      <section class={Style.recipes} aria-label={t("recipe.list.title")} data-empty={recipes.length === 0}>
        <Show when={recipes.length > 0}
              fallback={<EmptyStateFallback>{t("recipe.list.empty")}</EmptyStateFallback>}>
          <RecipeList recipes={recipes}/>
        </Show>
      </section>
    }</Suspense>
  );
}