import {Show, createResource} from "solid-js";
import {API} from "../api";
import {useI18n} from "../i18n";
import {Suspense} from "../suspense";
import {ChipListSkeleton, CardListSkeleton, EmptyStateFallback} from "../fallback";
import {CategoryList} from "../category";
import {RecipeList} from "../recipe";
import {isDesktop} from "../util/styling";

import Style from "./style.module.css";

export function HomeRoute() {
  const [t] = useI18n();
  const [favorites] = createResource(() => API.findFavoritesRecipes());
  const [categories] = createResource(() => isDesktop(), () => API.findAllCategories());

  return (
    <>
      <Show when={isDesktop()} keyed>
        <Suspense resource={categories}
                  loading={<ChipListSkeleton class={Style.categories}/>}>{categories =>
          <Show when={categories.length > 0}>
            <section class={Style.categories} aria-label={t("home.categories")}>
              <CategoryList categories={categories} horizontal/>
            </section>
          </Show>
        }</Suspense>
      </Show>
      <Suspense resource={favorites}
                loading={<CardListSkeleton class={Style.recipes} skeleton="recipe" horizontal/>}>{favorites =>
        <section class={Style.recipes} aria-label={t("home.recipes")} data-empty={favorites.length === 0}>
          <Show when={favorites.length > 0}
                fallback={<EmptyStateFallback>{t("home.notFound")}</EmptyStateFallback>}>
            <RecipeList recipes={favorites} horizontal/>
          </Show>
        </section>
      }</Suspense>
    </>
  );
}