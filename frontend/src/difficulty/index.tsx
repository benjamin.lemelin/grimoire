import {Difficulty} from "../api";
import {useI18n} from "../i18n";
import {Icon} from "../icon";

import Style from "./style.module.css";

type Props = {
  value: Difficulty
}

export function DifficultyTag(props: Props) {
  const [t] = useI18n();

  return (
    <span class={Style.difficulty}>
      <Icon icon="egg"/>
      <span>{t("difficulty", props.value)}</span>
    </span>
  );
}