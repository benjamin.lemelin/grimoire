import {ClassList} from "../util/styling";

type Props = {
  class?: string,
  classList?: ClassList,
  icon: string
}

export function Icon(props: Props) {
  return (
    <span class={`icon ${props.class}`} classList={props.classList} aria-hidden="true">
      {props.icon}
    </span>
  )
}