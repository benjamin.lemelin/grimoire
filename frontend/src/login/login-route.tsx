import {createRenderEffect, createSignal, JSX, Show} from "solid-js";
import {useNavigate} from "@solidjs/router";
import {ApiError, API} from "../api";
import {useI18n} from "../i18n";
import {useAuth} from "../auth";
import {useErrorHandler} from "../error";
import {Spinner} from "../spinner";
import {Brand} from "../brand";

import Style from "./login-route.module.css";

export function LoginRoute() {
  const [t] = useI18n();
  const [, setAuth] = useAuth();
  const navigate = useNavigate();
  const handleError = useErrorHandler();
  const [username, setUsername] = createSignal("");
  const isUsernameValid = () => !!username().trim();
  const [password, setPassword] = createSignal("");
  const isPasswordValid = () => !!password();
  const [isWrongCredentials, setWrongCredentials] = createSignal(false);
  const [validate, setValidate] = createSignal(false);
  const [isSending, setSending] = createSignal(false);

  createRenderEffect(async () => {
    if (await API.isSetupRequired()) navigate("/setup");
  });

  const onUsernameInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setUsername(event.currentTarget.value);
  };

  const onPasswordInput: JSX.EventHandler<HTMLInputElement, InputEvent> = (event) => {
    setPassword(event.currentTarget.value);
  };

  async function onSubmit(event: SubmitEvent) {
    event.preventDefault();

    setValidate(true);
    if (!isUsernameValid()) return;
    if (!isPasswordValid()) return;
    setValidate(false);

    setSending(true);
    try {
      const user = await API.login(username(), password());
      setAuth(user);
    } catch (e) {
      if (e instanceof Error && e.message === ApiError.BadRequest) {
        setWrongCredentials(true);
      } else {
        handleError(e);
      }
    }
    setSending(false);
  }

  return (
    <section class={Style.login}>
      <header class={Style.header}>
        <Brand class={Style.brand}/>
        <h1>{t("login.title")}</h1>
        <p class="muted">{t("login.subtitle")}</p>
      </header>
      <form class={Style.form} onSubmit={onSubmit} novalidate>
        <label for="username">{t("login.username")} :</label>
        <input id="username"
               class="bordered"
               classList={{"invalid": validate() && !isUsernameValid()}}
               type="text"
               value={username()}
               onInput={onUsernameInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isUsernameValid()}}>
          {t("login.usernameEmptyError")}
        </div>
        <label for="password">{t("login.password")} :</label>
        <input id="password"
               class="bordered"
               classList={{"invalid": validate() && !isPasswordValid()}}
               type="password"
               value={password()}
               onInput={onPasswordInput}/>
        <div class={Style.error} classList={{[Style.visible]: validate() && !isPasswordValid()}}>
          {t("login.passwordEmptyError")}
        </div>
        <div class={`${Style.error} ${Style.credentials}`} classList={{[Style.visible]: isWrongCredentials()}}>
          {t("login.wrongCredentialsError")}
        </div>
        <button class="primary">
          <Show when={!isSending()} fallback={<Spinner small/>} keyed>
            {t("login.login")}
          </Show>
        </button>
      </form>
    </section>
  );
}