FROM debian:stable-slim

ARG backend_dir
ARG frontend_dir

COPY ./$backend_dir /grimoire/app
COPY ./$frontend_dir /grimoire/static

VOLUME /grimoire/data

EXPOSE 8242
EXPOSE 8243
WORKDIR /grimoire
ENTRYPOINT ["./app", \
            "--data-dir", "./data", \
            "--port", "8242", \
            "--secure-port", "8243"]